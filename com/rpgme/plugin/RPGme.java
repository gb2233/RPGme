package com.rpgme.plugin;

import com.rpgme.content.Launcher;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.blockmeta.BlockDataManager;
import com.rpgme.plugin.manager.CommandManager;
import com.rpgme.plugin.player.PlayerManager;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.TreasureBag;
import com.rpgme.plugin.util.config.*;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.logging.Level;

/**
 * Main class. This (indirectly) handles all modules of the plugin. Additionally it handles the configuration logic and has some quick access api methods.
 */
public class RPGme extends JavaPlugin {

    // core modules
    public static final int MODULE_PLAYER_MANAGER = Id.newId();
    public static final int MODULE_SKILL_MANAGER = Id.newId();
    public static final int MODULE_COMMAND_MANAGER = Id.newId();
    public static final int MODULE_BLOCK_META = Id.newId();
    public static final int MODULE_TREASURES = Id.newId();

    // additional default modules
    public static final int MODULE_PARTIES = Id.newId();
    public static final int MODULE_HIGHSCORES = Id.newId();
    public static final int MODULE_HEALTH_BARS = Id.newId();
    public static final int MODULE_CHAT_PLACEHOLDERS = Id.newId();
    public static final int MODULE_MOB_LEVELS = Id.newId();

    /** default Skill ids are defined in SkillType.java */

    /** default Ability ids are defined in their parents Skill class */

    // singleton
    private static RPGme instance;
    public static RPGme getInstance() {
        return instance;
    }

    // configuration
    private FileConfiguration fileConfig;
    private MessagesBundle messagesBundle;

    // module storage
    private IntMap<Module> moduleMap = new IntMap<>(12);

    @Override
    public void onEnable() {
        instance = this;
        LegacyConfig.prepare(this);
        // register core modules
        registerModule(MODULE_PLAYER_MANAGER, new PlayerManager(this));
        registerModule(MODULE_SKILL_MANAGER, new SkillManager(this));
        registerModule(MODULE_COMMAND_MANAGER, new CommandManager(this));
        registerModule(MODULE_BLOCK_META, new BlockDataManager(this));
        registerModule(MODULE_TREASURES, new TreasureBag(this));

        // helper class to launch the default content
        Launcher.registerDefaultContent(this);

        // finally read and apply the configuration
        /* note: every plugin that adds modules has to call this reloadConfig in the end to commit their modules */
        reloadConfig();
    }

    @Override
    public void onDisable() {
        for(Module module : moduleMap.values()) {
            try {
                module.onDisable();
            } catch (Exception e) {
                getLogger().log(Level.SEVERE, "Exception while disabling module " + module.getClass().getSimpleName(), e);
            }
        }
    }

    /**
     * Get one of the registered modules. All of the default module ids are declared in this class.
     * To overwrite the behaviour of a module, you can remove the existing module first, then register your own version with the same id.
     * If you do this, be sure that your new class extends the original, so that methods depending on it will not break.
     * @param id of the module
     * @param <T> the module instance
     * @return The module instances casted to the type you want. If a ClassCastException occurs this method returns null instead.
     */
    @SuppressWarnings("unchecked")
    public <T extends Module> T getModule(int id) {
        try {
            return (T) moduleMap.get(id);
        } catch (ClassCastException e) {
            return null;
        }
    }

    /**
     * <p>Register a module to the plugin. This should be used for global features, not bound to a specific Skill.
     * Skills themselves (which are also Modules) can be added here, though they are just forwarded to the SkillManager ({@link SkillManager#registerSkill(int, Skill)})
     * Abilities should be registered in the parent BaseSkill class.</p>
     * <p>Note that this will fail if there is already a module registered for a given id. If you want to replace a module, you have to disable it first with {@link #removeModule(int)}</p>
     * @param id The id to register this module to.
     * @param module the module instance to register.
     * @return false if id already taken or an exception occured during enable, true otherwise
     */
    public boolean registerModule(int id, Module module) {
        if(module instanceof Skill) {
            return getSkillManager().registerSkill(id, (Skill) module);
        }

        Module other = moduleMap.get(id);
        if(other != null) {
            getLogger().severe("Failed to register module "+module.getClass() + ". Id " + id + " already taken by " + other.getClass());
            return false;
        }

        try {
            module.onEnable();
        } catch(Exception e) {
            getLogger().log(Level.SEVERE, "Exception while enabling module " + module.getClass().getSimpleName(), e);
            return false;
        }
        if(module instanceof Listener) {
            Bukkit.getPluginManager().registerEvents((Listener) module, this);
        }

        moduleMap.put(id, module);
        return true;
    }

    /**
     * Removes and unregisters a Module for the given id.
     * @param id id the module was registered with
     * @return the removed module, or null if none found.
     */
    public Module removeModule(int id) {
        Module module = moduleMap.remove(id);
        if(module == null)
            return null;
        if(module instanceof Listener) {
            HandlerList.unregisterAll((Listener)module);
        }
        try {
            module.onDisable();
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Exception while disabling module " + module.getClass().getSimpleName(), e);
        }
        return module;
    }

    /** api / shortcut methods */

    public SkillManager getSkillManager() {
        SkillManager manager = getModule(MODULE_SKILL_MANAGER);
        return manager;
    }

    public PlayerManager getPlayerManager() {
        PlayerManager manager = getModule(MODULE_PLAYER_MANAGER);
        return manager;
    }

    public CommandManager getCommandManager() {
        CommandManager manager = getModule(MODULE_COMMAND_MANAGER);
        return manager;
    }

    public RPGPlayer getPlayer(Player player) {
        return getPlayerManager().get(player);
    }

    public int getLevel(Player player, int skill) {
        return getPlayer(player).getLevel(skill);
    }

    public float getExp(Player player, int skill) {
        return getPlayer(player).getExp(skill);
    }

    public Skill getSkill(String name) {
        return getSkillManager().getByName(name);
    }

    public Skill getSkill(int id) {
        return getSkillManager().getById(id);
    }

    public void addExp(Player player, int skill, float exp) {
        Skill instance = getSkillManager().getById(skill);
        if(instance != null) {
            instance.addExp(player, exp);
        }
    }

    /* configuration logic */

    @Override
    public FileConfiguration getConfig() {
        return fileConfig;
    }

    public MessagesBundle getMessages() {
        return messagesBundle;
    }


    /** Our custom reload method. It build the configurations according to registered modules, and then syncs it with the config.yml and messages.properties files. */
    @Override
    public void reloadConfig() {
        // here all modules define the configuration
        ConfigBuilder configBuilder = new ConfigBuilder();
        BundleBuilder bundleBuilder = new BundleBuilder();

        LegacyConfig.prepare(this);
        // define all the entire configuration files
        for(Module module : moduleMap.values()) {
            try {
                module.createConfig(configBuilder, bundleBuilder);
            } catch (Exception e) {
                getLogger().log(Level.SEVERE, "Exception while reloading module " + module.getClass().getSimpleName(), e);
            }
        }

        // This creates the file if not exist, reads the values, and updates the file if there are new values in our defined target config.
        // The settings configured in the file are now in the configBuilder
        File file = new File(getDataFolder(), "config.yml");
        configBuilder.combineWith(file);
        // use it as our config
        fileConfig = configBuilder.build();

        // now for the messages
        file = new File(getDataFolder(), "messages.properties");
        bundleBuilder.combineWith(file);
        messagesBundle = bundleBuilder.build();
        BundleSection messages = messagesBundle.getSection("");

        // now we have both configurations complete. Time to let the modules know so they can update their values in onLoad()
        for(Module module : moduleMap.values()) {
            try {
                module.onLoad(fileConfig, messages);
            } catch (Exception e) {
                getLogger().log(Level.SEVERE, "Exception while loading module " + module.getClass().getSimpleName(), e);
            }
        }
        LegacyConfig.destroy();
    }


    public File getUserDataFolder() {
        return new File(getDataFolder(), "data");
    }

    public static boolean DEBUG = true;
    public static void debug(String message) {
        if(DEBUG) {
            getInstance().getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "Debug; " + ChatColor.WHITE + message);
        }
    }
    public static void debug(String message, Object... args) {
        if(DEBUG) {
            getInstance().getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "Debug; " + ChatColor.WHITE + String.format(message, args));
        }
    }
}
