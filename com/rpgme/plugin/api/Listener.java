package com.rpgme.plugin.api;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;


public abstract class Listener<P extends Plugin> implements org.bukkit.event.Listener {

	public final P plugin;
	
	public Listener(P plugin) {
		this.plugin = plugin;
	}
	
	public void registerListeners() {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	public void unregisterListeners() {
		HandlerList.unregisterAll(this);
	}

	

}
