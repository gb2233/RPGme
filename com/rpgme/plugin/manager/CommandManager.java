package com.rpgme.plugin.manager;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.command.CoreCommand;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CommandManager implements Module {

	private final RPGme plugin;
	private final CommandMap commandMap;

	private String header, footer;
	private final List<CoreCommand> commands = new ArrayList<>(8);

	public CommandManager(RPGme plugin) {
		this.plugin = plugin;
		commandMap = initCommandMap();
	}

	@Override
	public void onEnable() {
		// generate header and footer for help text. Main color with the title center-top
		StringBuilder temp = new StringBuilder();
		String title = plugin.getSkillManager().colorLight() + plugin.getName() + " commands";
		// header
		int linerepeat = (52 - title.length() - 2) / 2;
		temp.append("&7&m").append(StringUtils.repeat('-', linerepeat))
				.append("[&r ").append(title).append(" &7&m]")
				.append(StringUtils.repeat('-', linerepeat));

		header = temp.toString();
		temp.setLength(0);
		// footer
		temp.append("&7&o  Add 'help' or '?' in any command to getById more information.\n&m")
				.append(StringUtils.repeat('-', 52));
		footer = temp.toString();
	}

	@Override
	public void onDisable() {
		commands.forEach(Module::onDisable);
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        messages.addHeader("Commands")
                .addValue("err_playernotfound", "&cSorry, player ''{0}'' is not online")
                .addValue("err_oncooldown", "")
                .addValue("err_nopermission", "&cYou do not have permission to do that.")
                .addValue("err_needhigherlevel", "&cYour {1} needs to be atleast level &f{0} &cto do that.")
                .addValue("err_alreadyskillenchanted", "&cEquipment can only boost one skill per piece.")
                .addValue("err_notunlocked", "&cYou do not meet the requirements to do that.\\nReason: {0}");
		commands.forEach((cmd) -> cmd.createConfig(config, messages));
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
        commands.forEach((cmd) -> cmd.onLoad(config, messages));
        commandMap.clearCommands();
        commandMap.registerAll(plugin.getName().toLowerCase(), new ArrayList<>(commands));
	}

	public void register(CoreCommand command) {
		command.onEnable();
		commands.add(command);
	}
	
	public void printCommandHelp(CoreCommand command, CommandSender sender) {
		StringBuilder result = new StringBuilder();
		final String HR2 = "\n&8----", HR1 = "&7&m" + StringUtils.repeat('-', 60);
		
		result.append(HR1).append("\n&bCommand &l/").append(command.getName());
		for(String alias : command.getAliases()) {
			result.append("&7 /").append(alias);
		}
		
		String description = command.getDescription();
		if(description != null) {
			result.append("\n&aDescription  &f&o").append(StringUtil.breakAndInsert(description + HR2, StringUtils.repeat(' ', 16) , false, " ", 72));
		}
		
		String usage = command.getUsage();
		if(usage != null) {
			result.append("\n&aUsage       &6").append(StringUtil.breakAndInsert(usage + HR2, StringUtils.repeat(' ', 16), false, " ", 72));
		}
		
		String example = command.getExample();
		if(example != null) {
			result.append("\n&aExample(s)  &e").append(StringUtil.breakAndInsert(example, StringUtils.repeat(' ', 16), false, " ", 72));
		}
		
		result.append('\n').append(HR1);
		sender.sendMessage(StringUtil.colorize(result.toString()));
		
	}

	public void printHelp(CommandSender sender) {		

		StringBuilder builder = new StringBuilder();
		// format entries and register to result
		for(CoreCommand command : commands) {
			if(!command.hasPermission(sender, false)) {
				continue;
			}

			String title = command.getName();
			builder.append('\n').append(plugin.getSkillManager().colorLight()).append(title).append(' ');
			for(String alias : command.getAliases()) {
				builder.append("&7 /").append(alias);
			}

			String desc = command.getDescription();
			if(desc != null && !desc.isEmpty()) {
				builder.append('\n').append(StringUtil.breakAndInsert(desc, StringUtils.repeat(' ', 8) + plugin.getSkillManager().colorLight(), true, " ", 75));
			}
		}
		// register footer and header
		builder.insert(0, header);
		builder.append('\n').append(footer);
		
		// send
		sender.sendMessage(StringUtil.colorize(builder.toString()));
	}

	private CommandMap initCommandMap() {
		try {
			final Field f = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			f.setAccessible(true);
			return (CommandMap) f.get(Bukkit.getServer());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public CommandMap getCommandMap() {
		return commandMap;
	}

}
