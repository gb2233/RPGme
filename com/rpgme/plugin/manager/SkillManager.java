package com.rpgme.plugin.manager;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.IntMap.Values;
import com.rpgme.plugin.util.TimeUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

/**
 *
 */
public class SkillManager implements Module {

    // static utility
    public static Skill findSkill(int id) {
        return RPGme.getInstance().getSkillManager().getById(id);
    }

    private final RPGme plugin;
    private final IntMap<Skill> skills = new IntMap<>();
    private FileConfiguration skillConfig;

    // configurables. cached for better performance
    private String colorPrimary, colorPrimaryDark, colorAccent;
    private int targetLevel;

    public SkillManager(RPGme plugin) {
        this.plugin = plugin;
    }

    @Override
    public void onEnable() {
        // Already read the current configuration, because we want to be able to know which modules are enabled.
        // If there is no current configuration, assume all modules to be enabled.
        File skillConfigFile = new File(plugin.getDataFolder(), "skills.yml");
        if(skillConfigFile.exists()) {
            skillConfig = YamlConfiguration.loadConfiguration(skillConfigFile);
        }
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        // settings for the main config

        messages.addHeader("Color Theme")
                .addValue("Set bukkit colors to theme various ui elements", "color_primary", "&e")
                .addValue("color_primaryDark", "&6").addValue("color_accent", "&b")

                .addHeader("Exp messages")
                .addValue("message when you level up. Placeholder 0 gets replaced with the skill, and 1 with the level", "exp_levelup", "&aYour {0} has increased to &2level &l{1}")
                .addValue("Format for the gains-style message", "exp_gain", "&6+{0} &eexp &f(&7{1}&f)")
                .addValue("Format for the stats-style message", "exp_stats", "&e{0} &7{1} &8| {2}")
                .addValue("Format for the bar-style message", "exp_bar", "&6[||||||||||||||||||||||||||||||&6] &8- &e{0}")
                .addValue("character(s) in the above string that getEntry colored", "exp_bar_char", "|")

                .addNewline().addHeader("UI")

                .addValue("cooldown message, with the placeholder as time", "ui_err_oncooldown", "&9On Cooldown for &b{0}")
                .addValue("ui_notification_title_format", "&6{0} &2: &9&l{1} &7- {2}")

                .addValue("skills_total", "Total:")
                .addValue("skills_combat", "Combat Level")
                .addValue("skills_hoverme", "Tip: Use your mouse")
                .addValue("ui_url", "&eClick for external information")
                .addValue("ui_nextlevel", "Until next:")
                .addValue("ui_currentlevel", "Level:")
                .addValue("ui_next", "&7&oNext: &l{0} &7&oat level &f&o{1}")
                .addValue("ui_stats", "&6Stats:")
                .addValue("ui_messages", "&aMessages:")
                .addValue("ui_clickme", "Click for skill info")
                .addValue("ui_settings_title", "&cRPGme Settings")
                .addValue("ui_settings", "Settings")
                .addValue("ui_settings_clickme", "Click to open")
                .addValue("ui_scoreboard_title", "&lRPGme")
                ;

        // skills don't getEntry to use the global config. instead we create a new one to pass to all children
        ConfigBuilder skillConfig = new ConfigBuilder();
        skillConfig.addLargeHeader("RPGme Skill configuration").addNewline();

        for(Skill skill : skills.values()) {
            String name = skill.getName();

            skillConfig.addHeader(name);
            skillConfig.beginSection(name);

            messages.addHeader(name);
            messages.setPrefix(name);

            try {
                skill.createConfig(skillConfig, messages);
            } catch (Exception e) {
                getPlugin().getLogger().log(Level.SEVERE, "Skill " + name + " failed to initialize.", e);
            }
            skillConfig.endSection();
            messages.addNewline();
        }
        messages.setPrefix(null);
        // and sync with a separate file
        File skillConfigFile = new File(plugin.getDataFolder(), "skills.yml");
        skillConfig.combineWith(skillConfigFile);

        this.skillConfig = skillConfig.build();
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        colorPrimary = messages.getMessage("color_primary");
        colorPrimaryDark = messages.getMessage("color_primaryDark");
        colorAccent = messages.getMessage("color_accent");

        targetLevel = config.getInt("Target Level");

        for(Skill skill : skills.values()) {

            String name = skill.getName();
            // load children from our separate config, with their unique section
            ConfigurationSection skillConfigSection = skillConfig.getConfigurationSection(name);
            BundleSection section = messages.getSection(name);

            try {
                skill.onLoad(skillConfigSection, section);
            } catch (Exception e) {
                getPlugin().getLogger().log(Level.SEVERE, "Skill " + name + " failed to initialize.", e);
            }
        }

        for(Skill skill : skills.values()) {
            if(!skill.isEnabled()) {
                removeSkill(skill.getId());
            }
        }
    }

    public RPGme getPlugin() {
        return plugin;
    }

    public int getTargetLevel() {
        return targetLevel;
    }

    public FileConfiguration getSkillConfig() {
        return skillConfig;
    }

    public String colorDark() {
        return colorPrimaryDark;
    }

    public String colorLight() {
        return colorPrimary;
    }

    public String colorAccent() {
        return colorAccent;
    }

    public Skill getByName(String name) {
        for(Skill skill : getSkills()) {

            if(skill.getName().equalsIgnoreCase(name)) {
                return skill;
            }
            if(skill.hasDisplayName() && skill.getDisplayName().equalsIgnoreCase(name)) {
                return skill;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <S extends Skill> S getById(int id) {
        try {
            return (S) skills.get(id);
        }catch (ClassCastException e) {
            return null;
        }
    }

    public int getSkillCount() {
        return skills.size;
    }

    public Values<Skill> getSkills() {
        return skills.values();
    }

    public Collection<Skill> getSkills(Player p) {
        RPGPlayer rp = plugin.getPlayer(p);
        if(rp == null) {
            return Collections.emptyList();
        }
        return getSkills(rp);
    }

    public Collection<Skill> getSkills(RPGPlayer rp) {
        List<Skill> list = new ArrayList<>(getSkillCount());
        for(Skill skill : getSkills()) {
            if(skill.isEnabled(rp)) {
                list.add(skill);
            }
        }
        return list;
    }

    public List<Integer> getKeys() {
        List<Integer> list = new ArrayList<>(skills.size);
        skills.entries().forEach((entry) -> list.add(entry.key));
        return list;
    }

    public boolean registerSkill(int id, Skill module) {
        Module other = skills.get(id);
        if(other != null) {
            plugin.getLogger().severe("Failed to register module "+module.getClass() + ". Id " + id + " already taken by " + other.getClass());
            return false;
        }

        boolean isModuleEnabled = skillConfig == null || skillConfig.getBoolean(module.getName() + ".enabled", true);
        if(!isModuleEnabled) {
            return false;
        }

        try {
            module.onEnable();
        } catch(Exception e) {
            plugin.getLogger().log(Level.SEVERE, "Exception while enabling module " + module.getClass(), e);
            return false;
        }

        Bukkit.getPluginManager().registerEvents(module, plugin);
        skills.put(id, module);
        return true;
    }

    public String buildCooldownMessage(long millis) {
        String time = TimeUtil.readableAsMinuteFormat((int) (millis/1000));
        return plugin.getMessages().getMessage("ui_err_oncooldown", time);
    }

    public Skill removeSkill(int skillId) {
        Skill skill = skills.remove(skillId);
        HandlerList.unregisterAll(skill);
        try {
            skill.onDisable();
        } catch (Exception e) {
            plugin.getLogger().log(Level.SEVERE, "Exception while disabling module " + skill.getClass(), e);
        }
        return skill;
    }

}
