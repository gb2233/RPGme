package com.rpgme.plugin.command;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.player.PartyModule;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.Symbol;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class PartyCommand extends CoreCommand {

	private PartyModule module;

	public PartyCommand(RPGme plugin, PartyModule module) {
		super(plugin, "rpgparty", "rpgme.command.parties");
		this.module = module;

		setAliases("p", "pchat", "party");
		setConsoleAllowed(false);
		setDescription("A simple form of parties. All RPGme exp gained will be shared with nearby party members. Additionally you can chat with your party in private.");
		setExample("/p register/join <player>\n/p leave\n/p <message to party members>");
		setUsage("/party register/join/leave <player>\n/party <anything else to send a party chat message>.");
	}

	@Override
	public void execute(CommandSender sender, String label, List<String> args) {
		if(args.isEmpty()) {
			plugin.getCommandManager().printCommandHelp(this, sender);
			return;
		}

		Player p = (Player) sender;
		RPGPlayer player = plugin.getPlayer(p);
		if(player == null) {
			error(sender, "Unable to load your RPGme profile.");
			return;
		}

		String keyword = args.get(0);

		// list members
		if(keyword.equals("list")) {
			sendPartyMemberList(player);
		}

		// handle invites
		else if(keyword.equals("accept")) {

			module.onAcceptCommand(player);
			
		} else if(keyword.equals("decline")) {

			module.onDeclineCommand(player);
		}

		// invite others
		else if(args.size() == 2 && (keyword.equalsIgnoreCase("register") || keyword.equalsIgnoreCase("join"))) {

			Player other = plugin.getServer().getPlayer(args.get(1));
			RPGPlayer otherPlayer = other != null ? plugin.getPlayer(other) : null;
			if(other == null || otherPlayer == null) {
				sender.sendMessage(plugin.getMessages().getMessage("err_playernotfound", args.get(1)));
				return;
			}
			module.onInviteCommand(player, otherPlayer);

			// leave party
		} else if(args.size() == 1 && args.get(0).equalsIgnoreCase("leave")) {

			module.onLeaveCommand(player);

		} else {

			// else do party chat
			StringBuilder sb = new StringBuilder();
			for(String w : args) {
				sb.append(w).append(' ');
			}
			module.doPartyChat(player, sb.toString());
		}

	}

	private void sendPartyMemberList(RPGPlayer player) {
		String title = "&2Party Members\n";
		StringBuilder sb = new StringBuilder();
		sb.append("&a").append(Symbol.FRAME_TOP_PIECE).append(title);

		if(player.getPartyMembers() == null) {
			sb.append("&a").append(Symbol.FRAME_LINE).append("&7You are not in a party.\n");
		} else {
			for(RPGPlayer member : player.getPartyMembers()) {
				String color = member == player ? "&e" : "&a";
				sb.append("&a").append(Symbol.FRAME_LINE).append(member.getPlayer().getName()).append('\n');
			}
		}
		sb.append("&a").append(Symbol.FRAME_END);
		player.getPlayer().sendMessage(StringUtil.colorize(sb.toString()));
	}

	@Override
	public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
		return getOnlinePlayers();
	}

}
