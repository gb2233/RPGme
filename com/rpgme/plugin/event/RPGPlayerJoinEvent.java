package com.rpgme.plugin.event;

import com.rpgme.plugin.player.RPGPlayer;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called when an player has finished initializing.
 * For use instead of PlayerJoinEvent
 * @author Robin
 */
public class RPGPlayerJoinEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final RPGPlayer player;
	
	public RPGPlayerJoinEvent(RPGPlayer player) {
		this.player = player;
	}
	
	public RPGPlayer getRPGPlayer() {
		return player;
	}
	
	public Player getPlayer() {
		return player.getPlayer();
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

}
