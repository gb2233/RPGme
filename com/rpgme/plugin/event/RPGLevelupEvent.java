package com.rpgme.plugin.event;

import com.rpgme.plugin.api.Skill;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RPGLevelupEvent extends PlayerEvent {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final Skill skill;
	private final int tolevel;

	public RPGLevelupEvent(Player p, Skill skill, int tolevel) {
		super(p);
		this.skill = skill;
		this.tolevel = tolevel;
	}

	public Skill getSkill() {
		return skill;
	}

	public int getSkillId() {
		return skill.getId();
	}

	public int getToLevel() {
		return tolevel;
	}
	
	public int getFromLevel() {
		return tolevel-1;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}


}
