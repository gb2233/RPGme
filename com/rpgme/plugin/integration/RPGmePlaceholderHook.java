package com.rpgme.plugin.integration;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;

import org.bukkit.entity.Player;

import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderHook;

public class RPGmePlaceholderHook extends PlaceholderHook {
	
	protected static void register() {
		if(PlaceholderAPI.registerPlaceholderHook("RPGme", new RPGmePlaceholderHook(), false))
			RPGme.getInstance().getLogger().info("PlaceholderAPI placeholders hooked.");
		else
			RPGme.getInstance().getLogger().severe("Failed to register PlaceholderAPI hooks.");
	}

	@Override
	public String onPlaceholderRequest(Player arg0, String arg1) {
		// getModule our player holding all information
		RPGPlayer player = RPGme.getInstance().getPlayer(arg0);
		if(player == null)
			return null;

		int value = -1;

		// first check if the id is a skill
		Skill skill = RPGme.getInstance().getSkillManager().getByName(arg1);
		if(skill != null) {
			value = player.getLevel(skill);
		} else {
			// else check additional tags
			switch(arg1) {
			case "total": value = player.getSkillSet().getTotalLevel(); break;
			case "combat": value = player.getSkillSet().getCombatLevel(); break;
			case "average": value = player.getSkillSet().getAverageLevel(); break;
			}
		}

		return value > 0 ? String.valueOf(value) : null;
	}


}
