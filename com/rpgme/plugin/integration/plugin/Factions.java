package com.rpgme.plugin.integration.plugin;

import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPerm;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.factions.entity.MPlayerColl;
import com.massivecraft.massivecore.ps.PS;
import com.rpgme.plugin.integration.PlayerRelation;
import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import com.rpgme.plugin.integration.PluginIntegration.TeamPlugin;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Factions implements BlockProtectionPlugin, TeamPlugin {
	
	@Override
	public void enable(Plugin plugin) {
	
	}

	@Override
	public PlayerRelation getRelation(Player one, Player two) {
		MPlayer Pone = MPlayer.get(PS.valueOf(one));
		MPlayer Ptwo = MPlayer.get(PS.valueOf(two));

		return valueOf(Pone.getRelationTo(Ptwo));
	}

	private PlayerRelation valueOf(Rel rel) {
		switch(rel) {
		case ALLY: return PlayerRelation.TEAM;
		case ENEMY: return PlayerRelation.ENEMIES;
		default: return PlayerRelation.NEUTRAL;
		}
	}

	@Override
	public boolean canChange(Player p, Block block) {
		MPlayer factionsPlayer = MPlayerColl.get().get(p);
		Faction blockFaction = BoardColl.get().getFactionAt(PS.valueOf(block));

		return blockFaction == null || MPerm.getPermBuild().has(factionsPlayer, blockFaction, false);
	}

	@Override
	public boolean isInClaim(Block block) {
		Faction blockFaction = BoardColl.get().getFactionAt(PS.valueOf(block));
		return blockFaction != null;
	}

	@Override
	public String getPluginName() {
		return "Factions";
	}



}
