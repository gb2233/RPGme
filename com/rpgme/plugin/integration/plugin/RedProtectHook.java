package com.rpgme.plugin.integration.plugin;

import com.rpgme.plugin.integration.PlayerRelation;
import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import com.rpgme.plugin.integration.PluginIntegration.TeamPlugin;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.Set;

import br.net.fabiozumbi12.RedProtect.RedProtect;
import br.net.fabiozumbi12.RedProtect.Region;

import static com.rpgme.plugin.integration.PlayerRelation.NEUTRAL;
import static com.rpgme.plugin.integration.PlayerRelation.TEAM;

public class RedProtectHook implements TeamPlugin, BlockProtectionPlugin {

	@Override
	public void enable(Plugin plugin) {
	}

	@Override
	public String getPluginName() {
		return "RedProtect";
	}

	@Override
	public boolean canChange(Player p, Block block) {
		 Region region = RedProtect.rm.getTopRegion(block.getLocation());
		 return region == null || region.canBuild(p);
	}

	@Override
	public boolean isInClaim(Block block) {
		return RedProtect.rm.getTopRegion(block.getLocation()) != null;
	}

	@Override
	public PlayerRelation getRelation(Player one, Player two) {
		Set<Region> onesRegions = RedProtect.rm.getRegions(one.getUniqueId().toString());
		Set<Region> twosRegions = RedProtect.rm.getRegions(one.getUniqueId().toString());
		
		if(onesRegions == null || onesRegions.isEmpty() || twosRegions == null || twosRegions.isEmpty()) {
			return NEUTRAL;
		}
		
		for(Region region : onesRegions) {
			if(twosRegions.contains(region)) 
				return TEAM;
		}
		
		return NEUTRAL;
	}
	
	

}
