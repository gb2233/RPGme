package com.rpgme.plugin.integration.plugin;

import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.ryanhamshire.GriefPrevention.Claim;

public class GriefPrevention implements BlockProtectionPlugin {
	
	@Override
	public void enable(Plugin plugin) {
	
	}
	
	@Override
	public boolean canChange(Player p, Block block) {
		Claim claim = me.ryanhamshire.GriefPrevention.GriefPrevention.instance.dataStore
				.getClaimAt(block.getLocation(), false, null);
		return claim == null || claim.allowBreak(p, block.getType()) == null;
	}
	
	@Override
	public boolean isInClaim(Block block) {
		Claim claim = me.ryanhamshire.GriefPrevention.GriefPrevention.instance.dataStore
				.getClaimAt(block.getLocation(), false, null);
		return claim != null;
	}

	@Override
	public String getPluginName() {
		return "GriefPrevention";
	}

}
