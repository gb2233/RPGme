package com.rpgme.plugin.integration;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.integration.plugin.Factions;
import com.rpgme.plugin.integration.plugin.FactionsUUID;
import com.rpgme.plugin.integration.plugin.GriefPrevention;
import com.rpgme.plugin.integration.plugin.KingdomsHook;
import com.rpgme.plugin.integration.plugin.RedProtectHook;
import com.rpgme.plugin.integration.plugin.Residence;
import com.rpgme.plugin.integration.plugin.Towny;
import com.rpgme.plugin.integration.plugin.WorldGuard;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class PluginIntegration {

	private static final PluginIntegration i = new PluginIntegration();

	public static PluginIntegration getInstance() {
		return i;
	}

	private WGCustomFlags wgFlags;
	private Set<BlockProtectionPlugin> protectionPlugins;
	private TeamPlugin teamPlugin;

	private PluginIntegration() {
		PluginManager pm = Bukkit.getPluginManager();

		register("Towny", pm);
		register("GriefPrevention", pm);
		register("Kingdoms", pm);
		register("RedProtect", pm);
		register("Residence", pm);
		
		register("PlaceholderAPI", pm);
		if(register("WorldGuard", pm))
			register("WGCustomFlags", pm);
		
		Plugin factions = pm.getPlugin("Factions");
		if(factions != null) {
			String className = factions.getClass().getSimpleName();
			if(className.equals("Factions")) {
				register("Factions", pm);
			}else if(className.equals("P")) {
				register("FactionsUUID", pm);
			}
		}

		logResults();	
	}

	private boolean register(String name, PluginManager pm) {
		Plugin plugin = pm.getPlugin(name);
		if(plugin == null) return false;

		PluginHook hook = null;

		if(name.equals("Factions")) {
			// factions vs factionsUUID
			
		}

		switch(name) {
		case "Factions": hook = new Factions(); break;
		case "FactionsUUID": hook = new FactionsUUID(); break;
		case "Towny": hook = new Towny(); break;
		case "Residence": hook = new Residence(); break;
		case "GriefPrevention": hook = new GriefPrevention(); break;
		case "Kingdoms": hook = new KingdomsHook(); break;
		case "WorldGuard": hook = new WorldGuard(); break;
		case "WGCustomFlags": hook = new WGCustomFlags(); break;
		case "RedProtect": hook = new RedProtectHook(); break;
		case "PlaceholderAPI": RPGmePlaceholderHook.register(); return true;
		default: return false;
		}

		hook.enable(plugin);

		if(hook instanceof BlockProtectionPlugin) {
			if(protectionPlugins == null)
				protectionPlugins = new HashSet<>();

			protectionPlugins.add((BlockProtectionPlugin) hook);
		}
		if(hook instanceof TeamPlugin) {
			if(teamPlugin == null) {
				teamPlugin = (TeamPlugin) hook;
			}
			else {
				RPGme.getInstance().getLogger().severe("Intefering Team plugins detected "
						+ "("+teamPlugin.getPluginName() + " and "+ hook.getPluginName()
						+ ")! Using only "+teamPlugin.getPluginName() + " to define Player relations.");
			}
		}
		return true;
	}

	private void logResults() {
		Logger log = RPGme.getInstance().getLogger();

		if(protectionPlugins != null) {
			StringBuilder builder = new StringBuilder();
			for(BlockProtectionPlugin pl : protectionPlugins) {
				builder.append(pl.getPluginName() + ", ");
			}
			builder.setLength(builder.length()-2);

			builder.insert(0, "Found block protection manager(s): [");
			builder.append("]");

			log.info(builder.toString());
		}

		if(teamPlugin != null) {
			log.info("Found teams manager: "+teamPlugin.getPluginName());
		}


	}

	/*
	 * WorldGuard custom flags
	 */
	public double getExpMultiplier(Player player, Location location) {
		return wgFlags == null ? 1.0 : wgFlags.getExpValueAt(player, location);
	}

	public boolean isPluginEnabled(Player player, Location location) {
		return wgFlags == null || wgFlags.isPluginEnabledAt(player, location);
	}

	/*
	 * Public methods that consult hooked plugins
	 */
	public boolean canChange(Player p, Block block) {
		if(protectionPlugins == null || p == null || block == null)
			return true;

		for(BlockProtectionPlugin plugin : protectionPlugins) {

			try {
				if(!plugin.canChange(p, block))
					return false;
			} catch(Throwable e) {
				RPGme.getInstance().getLogger().severe("Error while consulting block protection manager "+plugin.getPluginName());
			}

		}

		return true;
	}

	public boolean isInClaim(Block block) {
		if(protectionPlugins == null || block == null)
			return false;

		for(BlockProtectionPlugin plugin : protectionPlugins) {

			try {
				if(plugin.isInClaim(block))
					return true;
			} catch(Throwable e) {
				RPGme.getInstance().getLogger().severe("Error while consulting block protection manager "+plugin.getPluginName());
			}
		}

		return false;
	}

	public PlayerRelation getRelation(Player one, Player two) {
		if(one == two)
			return PlayerRelation.SELF;

		if(teamPlugin == null || one == null || two == null)
			return PlayerRelation.NEUTRAL;

		try {
			return teamPlugin.getRelation(one, two);
		} catch(Throwable e) {
			RPGme.getInstance().getLogger().severe("Error while consulting teams manager "+teamPlugin.getPluginName());
			return PlayerRelation.NEUTRAL;
		}
	}

	public interface PluginHook {

		void enable(Plugin plugin);
		String getPluginName();

	}

	/*
	 * Interfaces used in this package
	 */
	public interface BlockProtectionPlugin extends PluginHook {

		boolean canChange(Player p, Block block);
		boolean isInClaim(Block block);

	}

	public interface TeamPlugin extends PluginHook {

		PlayerRelation getRelation(Player one, Player two);

	}

}
