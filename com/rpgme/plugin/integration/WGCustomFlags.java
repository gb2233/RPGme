package com.rpgme.plugin.integration;

import com.mewin.WGCustomFlags.WGCustomFlagsPlugin;
import com.mewin.util.Util;
import com.rpgme.plugin.integration.PluginIntegration.PluginHook;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.DoubleFlag;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class WGCustomFlags implements PluginHook {
	
	public static final DoubleFlag EXP_FLAG = new DoubleFlag("exp-gain");
	public static final BooleanFlag ENABLED_FLAG = new BooleanFlag("rpgme-enabled");
	
	private WGCustomFlagsPlugin plugin;
		
	@Override
	public void enable(Plugin plugin) {
		this.plugin = (WGCustomFlagsPlugin) plugin;
		this.plugin.addCustomFlag(EXP_FLAG);
		this.plugin.addCustomFlag(ENABLED_FLAG);
	}
	
	public double getExpValueAt(Player player, Location loc) {
		Double value = Util.getFlagValue(WGCustomFlagsPlugin.wgPlugin, loc, EXP_FLAG, player);
		return value == null ? 1.0 : value.doubleValue();
	}
	
	public boolean isPluginEnabledAt(Player player, Location loc) {
		Boolean value = Util.getFlagValue(WGCustomFlagsPlugin.wgPlugin, loc, ENABLED_FLAG, player);
		return value == null || value.booleanValue();
	}

	@Override
	public String getPluginName() {
		return plugin.getName();
	}

}
