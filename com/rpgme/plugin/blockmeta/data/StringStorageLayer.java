package com.rpgme.plugin.blockmeta.data;

import com.rpgme.plugin.blockmeta.BlockDataManager;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemoryConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class StringStorageLayer implements StorageLayer<String> {
	
	private final String layerName;
	protected Map<String, String> data;
		
	public StringStorageLayer(String layerName) {
		this.layerName = layerName;
		data = new HashMap<>();
	}
	
	private String getKey(Block block) {
		return BlockDataManager.toString(block);
	}
	
	
	@Override
	public String getName() {
		return layerName;
	}
	

	@Override
	public void setData(Block block, String value) {
		if(value != null)
			data.put(getKey(block), value);
		else
			data.remove(getKey(block));
	}
	
	@Override
	public String getData(Block block) {
		return data.get(getKey(block));
	}

	
	@Override
	public void readFromFile(MemoryConfiguration file) {
		ConfigurationSection section = file.getConfigurationSection(getName());
		if(section == null) return;
		
		for(Entry<String, Object> entry : section.getValues(false).entrySet()) {
			data.put(entry.getKey(), entry.getValue().toString());
		}
	}
	
	@Override
	public void writeToFile(MemoryConfiguration file) {
		file.createSection(getName(), data);
	}
	
	@Override
	public StringStorageLayer clone() {
		return new StringStorageLayer(getName());
	}
	
	@Override
	public int size() {
		return data.size();
	}
	
	
}
