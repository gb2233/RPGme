package com.rpgme.plugin.blockmeta.data;

import com.rpgme.plugin.blockmeta.BlockDataManager;
import org.bukkit.block.Block;
import org.bukkit.configuration.MemoryConfiguration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BooleanStorageLayer implements StorageLayer<Boolean> {
	
	private final String name;
	private final Set<String> data = new HashSet<>();
	
	public BooleanStorageLayer(String layerName) {
		this.name = layerName;
	}
	
	private String getKey(Block block) {
		return BlockDataManager.toString(block);
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setData(Block block, Boolean value) {
		if(value != null && value.booleanValue()) {
			data.add(getKey(block));
		} else {
			data.remove(getKey(block));
		}
	}

	@Override
	public Boolean getData(Block block) {
		return data.contains(getKey(block));
	}
	
	@Override
	public void readFromFile(MemoryConfiguration file) {
		List<String> list = file.getStringList(getName());
		data.addAll(list);
	}
	
	@Override
	public void writeToFile(MemoryConfiguration file) {
		List<String> list = new ArrayList<String>(data);
		file.set(getName(), list);
	}
	
	@Override
	public BooleanStorageLayer clone() {
		return new BooleanStorageLayer(getName());
	}
	
	@Override
	public int size() {
		return data.size();
	}

}
