package com.rpgme.plugin.util.config;

/**
 *
 */
public class BundleSection extends MessagesBundle {

    private final MessagesBundle bundle;

    private String path;

    public BundleSection(MessagesBundle bundle, String path) {
        this.bundle = bundle;
        setPath(path);
    }

    public final MessagesBundle getRoot() {
        return bundle;
    }

    public String getPath() {
        return path;
    }

    public BundleSection setPath(String path) {
        this.path = path == null  ? "" : path;
        if(!this.path.isEmpty() && !this.path.endsWith("_")) {
            this.path += "_";
        }
        return this;
    }

    @Override
    public String getMessage(String key) {
        key = path + key;
        return bundle.getMessage(key);
    }

    @Override
    public String getMessage(String key, Object... args) {
        key = path + key;
        return bundle.getMessage(key, args);
    }

    @Override
    public void put(String key, String value) {
        key = path + key;
        bundle.put(key, value);
    }

    @Override
    public BundleSection getSection(String name) {
        if(name == null || name.isEmpty()) {
            return new BundleSection(bundle, getPath());
        }
        return new BundleSection(bundle, path + name);
    }
}
