package com.rpgme.plugin.util.config;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.bukkit.plugin.Plugin;

import java.io.IOException;
import java.util.Properties;

/**
 * A utility class to help port v1 code to v2
 * Created by Robin on 02/07/2016.
 */
public class LegacyConfig {

    private static FileConfiguration config;
    private static Properties messages;

    public static void prepare(Plugin plugin) {
        try {
            config = YamlConfiguration.loadConfiguration(new InputStreamReader(plugin.getResource("legacy_config.yml")));
            messages = new Properties();
            messages.load(new InputStreamReader(plugin.getResource("legacy_messages.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void injectMessage(BundleBuilder builder, String... keys) {
        for(String key : keys) {
            injectMessage(builder, key, key);
        }
    }

    public static void injectMessage(BundleBuilder builder, String oldKey, String newKey) {
        String value = messages.getProperty(oldKey);
        if(value == null || value.isEmpty()) {
            System.err.println("Error injecting message '" + oldKey + "' as '"+newKey + "'. No such value in legacy properties");
        } else {
            builder.addValue(newKey, value);
        }
    }

    public static void injectNotification(BundleBuilder builder, Class<?> clazz, Object id) {
        String oldKey = "notification_"+clazz.getSimpleName().toLowerCase() + id;
        String key = "notification"+id;
        injectMessage(builder, oldKey, key);
    }

    public static String getNotification(BundleSection bundle, Class<?> clazz, int id, Object... formatArgs) {
        return getNotification(bundle, clazz, String.valueOf(id), formatArgs);
    }

    public static String getNotification(BundleSection bundle, Class<?> clazz, String id, Object... formatArgs) {
        String key = "notification"+id;
        return formatArgs.length > 0 ? bundle.getMessage(key, formatArgs) : bundle.getMessage(key);
    }

    public static void destroy() {
        config = null;
        messages = null;
    }

}
