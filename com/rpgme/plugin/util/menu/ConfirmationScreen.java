package com.rpgme.plugin.util.menu;

import com.rpgme.content.util.GUI;
import com.rpgme.content.util.ItemUtil;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class ConfirmationScreen extends GUI<JavaPlugin> {
	
	private final String action;
	private final int confirmslot, denyslot;
	
	public ConfirmationScreen(JavaPlugin plugin, Player player, String action) {
		super(plugin, player, "Are you sure?", 3);
		
		confirmslot = 2 + 9;
		denyslot = 6 + 9;
		this.action = action;
		
		addItems();
	}

	@Override
	public void addItems() {
		addItem(confirmslot, ItemUtil.create(Material.STAINED_CLAY, (short) 5, "&aConfirm", action));
		addItem(denyslot, ItemUtil.create(Material.STAINED_CLAY, (short)14, "&4Deny", "No thank you."));
	}

	@Override
	public void doAction(int slot) {
		if(slot == confirmslot)
			onConfirm();
		else if(slot == denyslot)
			onDeny();
		closeGUI();
	}
	
	public abstract void onConfirm();
	
	public abstract void onDeny();
	
	
	
	

}
