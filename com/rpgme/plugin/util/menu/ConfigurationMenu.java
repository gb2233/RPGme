package com.rpgme.plugin.util.menu;

import com.google.common.collect.Maps;
import com.rpgme.content.util.GUI;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.menu.Settings.ExpGainSetting;
import com.rpgme.plugin.util.menu.Settings.ScoreboardSetting;
import com.rpgme.plugin.util.menu.Settings.Setting;
import com.rpgme.plugin.util.menu.Settings.StaminaSetting;

import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Map.Entry;

public class ConfigurationMenu extends GUI<RPGme> {

	private static Map<Integer, Class<? extends Setting>> settingClasses = Maps.newHashMap();
	private static int rows = 1;

	public static void registerSetting(int slot, Class<? extends Setting> clazz) {
		settingClasses.put(slot, clazz);
	}
	public static void setSize(int rows) {
		ConfigurationMenu.rows = rows;
	}

	static {
		registerSetting(1, ExpGainSetting.class);
		registerSetting(8, StaminaSetting.class);
		if(RPGme.getInstance().getPlayerManager().isScoreboardEnabled())
			registerSetting(3, ScoreboardSetting.class);
	}

	private final RPGPlayer rpPlayer;
	private final Map<Integer, Setting> settings = Maps.newHashMap();

	public ConfigurationMenu(RPGme plugin, Player p) {
		super(plugin, p, plugin.getMessages().getMessage("ui_settings_title"), rows);
		rpPlayer = plugin.getPlayer(player);
		addItems();
	}


	@Override
	public void addItems() {

		for(Entry<Integer, Class<? extends Setting>> e : settingClasses.entrySet()) {

			Setting set = null;
			try {
				set = e.getValue().getConstructor(RPGPlayer.class).newInstance(rpPlayer);
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e1) {
				plugin.getLogger().severe("Error instantiating setting "+e.getValue() + '(' + e1.getMessage() + ')');
				continue;
			}

			settings.put(e.getKey(), set);

			addItem(e.getKey(), set.getAsItem());

		}

	}

	@Override
	public void doAction(int slot) {

		Setting setting = settings.get(Integer.valueOf(slot));
		if(setting != null)
			addItem(slot, setting.onToggle());


	}


}
