package com.rpgme.plugin.util.menu;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.util.GUI;
import com.rpgme.content.util.ItemUtil;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.Symbol;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ExpAddMenu extends GUI<RPGme> {
	
	private final int amount, minlvl;
	private Map<Skill, Integer> map;
    private List<Skill> list;
	
	public ExpAddMenu(RPGme plugin, Player p, int exp, int minlevel) {
		super(plugin, p, "Exp Tomb", 2);
		this.amount = exp;
		this.minlvl = minlevel;
		
		addItems();
	}

	@Override
	public void addItems() {
		map = new HashMap<>(plugin.getSkillManager().getSkillCount());
        list = new ArrayList<>(plugin.getSkillManager().getSkillCount());
		int slot = 1;
		
		for(Skill skill : plugin.getSkillManager().getSkills()) {
			if(skill.isEnabled(player)) {
				
				map.put(skill, slot);
				addItem(slot++, getItemRepresentation(skill));
				if(slot == 8)
					slot = 10;
			}
		}
		
	}

	@Override
	public void doAction(int slot) {
        Skill skill = null;
        for(Entry<Skill, Integer> e : map.entrySet()) {
            if(e.getValue() == slot) {
                skill = e.getKey();
            }
        }
        if(skill == null)
            return;

		if(!canUse(skill)) {
			player.sendMessage(plugin.getMessages().getMessage("err_needhigherlevel", minlvl, skill.getDisplayName()));
			GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
		}
		else {
			
			this.closeGUI();

            final Skill finalSkill = skill;

			new ConfirmationScreen(plugin, player, "&fIncrease skill &e"+skill.getDisplayName()+"?") {

				@Override
				public void onConfirm() {
					ItemStack newitem = player.getInventory().getItemInMainHand();
					newitem.setAmount(newitem.getAmount()-1);
					
					player.getInventory().setItemInMainHand(newitem.getAmount() > 0 ? newitem : null);
					
					GameSound.play(Sound.BLOCK_NOTE_HARP, player, 2f, 1.5f);
					((RPGme)plugin).getPlayer(player).addTrueExp(finalSkill, amount);
				}

				@Override
				public void onDeny() {
					GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
				}
			}.openGUI();
			
		}
		
	}
	
	private boolean canUse(Skill skill) {
		return skill.isEnabled(player) && plugin.getLevel(player, skill.getId()) >= minlvl;
	}

	
	private ItemStack getItemRepresentation(Skill skill) {
		String name = "&a" + skill.getDisplayName();
		String lore;
		
		RPGPlayer p = plugin.getPlayer(player);
		int level = p.getLevel(skill);
		
		if(level >= minlvl) {
			
			lore =  "&a" + Symbol.YES + "&e +"+amount+" exp \n &7"+level;
			
			int newlvl = ExpTables.getLevelAt(p.getExp(skill)+amount);
			if(newlvl > level) {
				lore += " &2" + Symbol.ARROW_RIGHT2 + " " + newlvl;
			}
		}
		
		else {
			lore = "&c" + Symbol.NO + "&7 not level "+minlvl;
		}

		Material mat = skill.getItemRepresentation();
		if(mat == null) mat = Material.WORKBENCH;
		return ItemUtil.addItemFlags(ItemUtil.create(mat, name, lore), ItemFlag.HIDE_ATTRIBUTES);
	}
	
	
	

}
