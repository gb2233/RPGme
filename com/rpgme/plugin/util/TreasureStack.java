package com.rpgme.plugin.util;

import com.rpgme.content.util.RPGItems;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.manager.SkillManager;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionType;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TreasureStack implements ConfigurationSerializable  {

	final ItemStack item;
	final int rarity;

	public TreasureStack(ItemStack item, int rarity) {
		this.item = item;
		this.rarity = Math.min(100, Math.max(1, rarity));
	}

	public int getWeight(int forLevel) {
		forLevel = Math.min(125, forLevel);
		int baseChance = 100 - Math.min(80, rarity);
		double rarityFactor;
		
		if(forLevel <= rarity) {
			int dif = rarity - forLevel;
			rarityFactor = dif > 80 ? 0.01 : Math.max(0.2, 1 - (dif/40.0));
		} else {
			int dif = forLevel - rarity;
			rarityFactor = Math.max(0.1, 1 - (dif/70.0));
		}
		
		int result = (int) Math.round(baseChance * rarityFactor);		
		return result;
	}

	public ItemStack getItemStack() {
		return item;
	}
	
	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<String, Object>(6);
		ItemStack item = getItemStack();
		map.put("type", item.getType().name());
		map.put("rarity", rarity);
		
		if(item.getAmount() != 1)
			map.put("amount", item.getAmount());
		if(item.getDurability() != 0)
			map.put("data", item.getDurability());
		
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public static TreasureStack deserialize(Map<String, Object> map) {
		
		Integer rarity = (Integer) map.get("rarity");
		if(rarity == null)
			rarity = 10;
		
		final String typeinput = (String) map.get("type");
		if(typeinput == null)
			return null;
		
		if(typeinput.equalsIgnoreCase("musicdisc")) {
			return new TreasureStack(null, rarity) {
				final Set<Material> musicDiscs = EnumSet.of(Material.RECORD_10, Material.RECORD_11, Material.RECORD_12, Material.RECORD_3, 
						Material.RECORD_4, Material.RECORD_5, Material.RECORD_6, Material.RECORD_7, Material.RECORD_8, Material.RECORD_9, 
						Material.GOLD_RECORD, Material.GREEN_RECORD);
				@Override
				public ItemStack getItemStack() {
					return new ItemStack(musicDiscs.toArray(new Material[musicDiscs.size()])[CoreUtil.random.nextInt(musicDiscs.size())]);
				}
			};
		}
		
		else if(typeinput.equalsIgnoreCase("cloudpotion")) {
			return new TreasureStack(null, rarity) {
				@Override
				public ItemStack getItemStack() {
					PotionType[] types = PotionType.values();
					PotionType type = null;
					while((type = types[CoreUtil.random.nextInt(types.length)]) == PotionType.WATER) { }
					return new ItemStack(Material.DIRT);//fixme uncomment: PotionCloud.createCloudPotion(type);
				}
			};
		}
		
		else if(typeinput.startsWith("skillenchantment")) {
			
			if(!RPGme.getInstance().getConfig().getBoolean("Skill Enchants.enabled"))
				return null;
			
			final int boost;
			try {
				boost = Integer.parseInt(typeinput.substring(16));
			} catch(NumberFormatException | IndexOutOfBoundsException exc) {
				RPGme.getInstance().getLogger().severe("Error reading skill enchantment book in treasure.yml ("+typeinput+") format: 'skillenchantment<boost>'");
				return null;
			}
			return new TreasureStack(null, rarity) {
				@Override
				public ItemStack getItemStack() {
					SkillManager manager = RPGme.getInstance().getSkillManager();
					List<Integer> skills = manager.getKeys();
					int id = skills.get(CoreUtil.random.nextInt(skills.size()));
					Skill randomSkill = manager.getById(id);
					return new ItemStack(Material.DIRT);//fixme uncomment: return new SkillEnchantment(randomSkill, boost).getEnchantedBook();
				}
			}; 
		}
		
		ItemStack type = deserializeType(typeinput);
		if(type == null)
			return null;
		
		Integer amount = (Integer) map.get("amount");
		type.setAmount(amount == null ? 1 : amount);
		
		Integer data = (Integer) map.get("data");
		type.setDurability(data == null ? 0 : (short) data.intValue());
		
		ItemMeta meta = type.getItemMeta();
		String name = (String) map.get("name");
		if(name != null)
			meta.setDisplayName(StringUtil.colorize("&f" + name));
		
		Object lore = map.get("lore");
		if(lore instanceof String)
			meta.setLore(Arrays.asList(StringUtil.colorize((String)lore).split("\n")));
		else if(lore instanceof List)
			meta.setLore(StringUtil.colorize((List<String>)lore));
		
		type.setItemMeta(meta);
		
		return new TreasureStack(type, rarity);
	}
	
	private static ItemStack deserializeType(String string) {
		
		if(string.startsWith("exptomb")) {
			String[] split = string.split(",");
			try {
				int exp = Integer.parseInt(split[0].substring(7));
				int level = split.length > 1 ? Integer.parseInt(split[1]) : 1;
				return RPGItems.createExpTomb(exp, level);
			} catch(NumberFormatException | IndexOutOfBoundsException exc) {
				RPGme.getInstance().getLogger().severe("Error reading exptomb in treasure.yml ("+string+") format: 'exptomb<exp>,<minlevel>'");
				return null;
			}
		}
		else if(string.equalsIgnoreCase("skillgem")) {
			return RPGItems.createSkillGem();
		}
		
		
		Material mat = Material.matchMaterial(string);
		if(mat == null) {
			RPGme.getInstance().getLogger().severe("Error reading treasure stack: unknown type 'type: "+string+"'");
			return null;
		}
		
		return new ItemStack(mat);
	}
	
	


}