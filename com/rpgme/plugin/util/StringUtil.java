package com.rpgme.plugin.util;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class StringUtil {

	private static Locale l = Locale.ENGLISH;

	public static String CamelCase(String input) {
		return CamelCase(input, " ");
	}

	public static String CamelCase(String input, String split) {
		StringBuffer buff = new StringBuffer();
		input = input.toLowerCase(l).trim();

		for(String word : input.split(split)) {

			buff.append( word.substring(0, 1).toUpperCase(l) + word.substring(1) + split);

		}

		return buff.toString().substring(0, buff.length()-split.length());
	}

	public static String matchEnum(String input) {
		return ChatColor.stripColor(input.replace(".", "_").replace(' ', '_').toUpperCase(l));
	}

	public static String reverseEnum(String constant) {
		return CamelCase(constant.replace('_', ' '));
	}

	public static List<String> splitToLength(String prefix, String input, String split, int length) {
		List<String> list = new ArrayList<String>();
		StringBuilder buff = new StringBuilder();

		for(String line : input.split("\n")) {

			for(String s : line.split(split)) {

				if(buff.length() + s.length() + 1 > length + colorCodeChars(s)) {
					list.add( prefix + buff.toString());
					buff.delete(0, buff.length());
				}
				buff.append(s + split);
			}
			if(buff.length() > 0) {
				list.add( prefix + buff.toString()); 
				buff.delete(0, buff.length()); }
		}

		return list;
	}

	public static String breakAndInsert(String text, String prefix, boolean insertFirst, String split, int length) {
		StringBuilder result = new StringBuilder(insertFirst ? prefix : "");

		int index;

		String[] lines = text.split("\n");
		for(int i = 0; i < lines.length; i++) {

			if(i > 0) {
				result.append('\n').append(prefix);
			}
			index = prefix.length();
			for(String word : lines[i].split(split)) {

				if((index += word.length() + split.length()) > length) {
					result.append('\n').append(prefix);
					index = prefix.length();
				}
				result.append(word).append(split);
			}
		}
		
		return result.toString();

	}

	private static int colorCodeChars(String s) {
		return (s.split("&").length - 1) * 2;
	}
	
	public static String colorizeForBungee(String string) {
		return BaseComponent.toPlainText(TextComponent.fromLegacyText(string));
	}

	public static String colorize(String string) {
		if(string == null) return null;
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	public static List<String> colorize(List<String> input) {
		for(int i = 0; i < input.size(); i++) {
			input.set(i, colorize(input.get(i)));
		}
		return input;
	}

	public static String[] colorize(String[] input) {
		for(int i = 0; i < input.length; i++) {
			input[i] = colorize(input[i]);
		}
		return input;
	}

	public static String trimToSize(String s, int maxlength) {
		if(s.length() > maxlength)
			s = s.substring(0, maxlength);
		return s;
	}

	public static String readableDecimal(double d) {
		if((long)d == d) {
			return String.format("%d", (long)d);
		}
		String res = String.format(Locale.ENGLISH, "%.2f", d);
		return res.endsWith("0") ? res.substring(0, res.length()-1) : res;
	}


}
