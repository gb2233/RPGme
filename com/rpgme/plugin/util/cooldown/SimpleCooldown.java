package com.rpgme.plugin.util.cooldown;

import com.google.common.collect.Maps;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * A utility timer class that can store cooldown timings for abilities or other needs.
 * This variant is to waitsa specified amount of milliseconds.
 */
public class SimpleCooldown implements Cooldown {
	
	protected final Map<UUID, Long> map = Maps.newHashMap();
	protected final long duration;
	
	public SimpleCooldown(Plugin plugin) {
		this(plugin, -1);
	}
	
	public SimpleCooldown(Plugin plugin, long duration) {
		CooldownCleaner.register(this, plugin);
		this.duration = duration;
	}

	@Override
	public boolean isOnCooldown(Player p) {
		Long time = map.getOrDefault(p.getUniqueId(), 0L);
        if(time < System.currentTimeMillis()) {
            map.remove(p.getUniqueId());
            return false;
        }
        return true;
		//return time != null && time > System.currentTimeMillis();
	}

	@Override
	public void add(Player p) {
		if(duration < 0)
			throw new UnsupportedOperationException("No default duration has been set. Either supply a duration in the constructior or use register(Player, long).");
		map.put(p.getUniqueId(), System.currentTimeMillis() + duration);
	}
	
	@Override
	public void add(Player p, long duration) {
		map.put(p.getUniqueId(), System.currentTimeMillis() + duration);
	}

	@Override
	public boolean remove(Player p) {
		return map.remove(p.getUniqueId()) != null;
	}

	@Override
	public long getMillisRemaining(Player p) {
		return getMillisRemaining(map.get(p.getUniqueId()));
	}
	
	private long getMillisRemaining(Long since) {
		if(since == null)
			return 0;
		return since - System.currentTimeMillis();
	}

	@Override
	public void cleanUp() {

		synchronized(map) {
			Iterator<Entry<UUID, Long>> it = map.entrySet().iterator();
			while(it.hasNext()) {
				
				if(getMillisRemaining(it.next().getValue()) < 1)
					it.remove();
			}
		}

	}

}
