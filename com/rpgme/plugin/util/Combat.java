package com.rpgme.plugin.util;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;

import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Tameable;

public class Combat {
	
	private static final Scaler
			healthExpScale = new Scaler(25, 1, 250, 5),
			distanceScale = new Scaler(0, 2, 45*45, 5);
	
	private static final double pvpMultiplier, spawnerMultiplier;
	
	static {
		pvpMultiplier = RPGme.getInstance().getConfig().getDouble("PvP exp", 0.25);
		spawnerMultiplier = RPGme.getInstance().getConfig().getDouble("Mobspawner exp", 0.2);
	}
	
	public static int getDamageAttribute(Material mat) {
		switch(mat) {

		case WOOD_SWORD: return 4;
		case STONE_SWORD: return 5;
		case IRON_SWORD: return 6;
		case GOLD_SWORD: return 4;
		case DIAMOND_SWORD: return 7;
		default: return 0;
		}

	}
	
	public static float getArcheryDamageExp(LivingEntity e, double damage, double distanceSquared) {
		
		return (float) (getAttackDamageExp(e, damage) * distanceScale.scale(distanceSquared) ); 
		
	}
	
	
	public static float getAttackDamageExp(LivingEntity e, double damage) {
		
		float total = getKillExp(e);
		
		return (float) (damage / e.getMaxHealth() * total);
	}
	
	public static float getKillExp(LivingEntity e) {
		
		double base = ExpTables.getExpRewardForKilling(e);
		
		return (float) (base * getExpFactor(e));
	}
	
	public static double getExpFactor(LivingEntity e) {
		
		if(e instanceof Tameable && ((Tameable)e).getOwner() != null)
			return 0;
		
		double factor = healthExpScale.scale(e.getMaxHealth());
		
		// multiply for pvp
		if(e.getType() == EntityType.PLAYER) {
			factor *= pvpMultiplier;
		}
		// multiply for mobspawner
		else if(e.hasMetadata("MonsterSpawner")) {
			factor *= spawnerMultiplier;
		}
		return factor;
	}

}
