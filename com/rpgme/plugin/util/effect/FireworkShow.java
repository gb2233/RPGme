package com.rpgme.plugin.util.effect;

import com.rpgme.plugin.util.CoreUtil;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class FireworkShow {
	
	private final JavaPlugin plugin;
	private final Iterator<Location> iterator;
	private final int amount;
	
	public FireworkShow(JavaPlugin plugin, Location around, int amount) {
		this.plugin = plugin;
		this.amount = amount*2;
		List<Location> set = CoreUtil.circle(around, 6, amount);
		set.addAll(set);
		iterator = set.iterator();
	}
	
	public void start() {
		new BukkitRunnable() {
			int count = 0;
			public void run() {
				
				if(count++ == amount)
					this.cancel();
				else {

					spawnFireworks( iterator.next());					
				}
				
			}
		}.runTaskTimer(plugin, 0l, 10l);
	}
	
	public static Firework spawnFireworks(Location loc) {
		return spawnFireworks(loc, CoreUtil.random.nextInt(2)+1);
	}
	
	public static Firework spawnFireworks(Location loc, int power) {
		Firework f = loc.getWorld().spawn(loc, Firework.class);
		FireworkMeta fm = f.getFireworkMeta();
		
		fm.addEffect(FireworkEffect.builder()
				.trail(true).with(shapes[ThreadLocalRandom.current().nextInt(shapes.length)]
				).withColor(randomColor()).withFade(randomColor()).build());
		
		fm.setPower(power);
		f.setFireworkMeta(fm);
		
		return f;
	}
	
	private static final Type[] shapes = new Type[] { Type.BALL, Type.BALL_LARGE, Type.STAR };
	private static Color randomColor() {
		Random random = ThreadLocalRandom.current();
		return Color.fromRGB(random.nextInt(255), random.nextInt(255), random.nextInt(255));
	}

}
