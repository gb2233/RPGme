package com.rpgme.plugin.util.effect;

import com.google.common.collect.Sets;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.util.CoreUtil;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Set;
public class LevelupEffect extends BukkitRunnable {

	private static final Set<LivingEntity> currentTasks = Sets.newHashSet();
	private static final Vector up = new Vector(0,1,0);

	final LivingEntity target;
	final List<Location> list;
	int count = 20;

	public LevelupEffect(LivingEntity e) {
		this.target = e;
		list = CoreUtil.circle(e.getEyeLocation().add(0, 0.4, 0), 1f, 10);
		list.addAll(list);		
	}

	public void start() {
		if(currentTasks.contains(target)) {
			return;
		}
		currentTasks.add(target);
		runTaskTimerAsynchronously(RPGme.getInstance(), 0l, 3l);
	}

	@Override
	public void run() {

		Location loc = list.get(--count);

		ParticleEffect.FLAME.display(up, 0.08f, loc, 16);
		ParticleEffect.VILLAGER_HAPPY.display(up, 0.1f, loc.add(up), 16);

		if(count == 0) {
			cancel();
		}
	}

}