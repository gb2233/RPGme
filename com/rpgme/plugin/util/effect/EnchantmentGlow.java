package com.rpgme.plugin.util.effect;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;

@SuppressWarnings("deprecation")
public class EnchantmentGlow extends EnchantmentWrapper {
	
	private static Enchantment glow;

	static
	{
		try
		{
			Field field = Enchantment.class.getDeclaredField("acceptingNew");
			field.setAccessible(true);
			field.set(null, Boolean.valueOf(true));
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		glow = new EnchantmentGlow(255);
		if(getById(255) == null)
			registerEnchantment(glow);
	}

	private EnchantmentGlow(int id) {
		super(id);
	}

	public String getName()
	{
		return "Glowy";
	}

	public boolean canEnchantItem(ItemStack item)
	{
		return true;
	}

	public boolean conflictsWith(Enchantment other)
	{
		return false;
	}

	public EnchantmentTarget getItemTarget()
	{
		return null;
	}

	public int getMaxLevel()
	{
		return 1;
	}

	public int getStartLevel()
	{
		return 1;
	}

	public static Enchantment getGlowEnchantment()
	{
		return glow;
	}

	public static ItemStack addGlow(ItemStack itemStack)
	{
		itemStack.addUnsafeEnchantment(getGlowEnchantment(), 1);
		return itemStack;
	}

	public static ItemStack removeGlow(ItemStack itemStack)
	{
		itemStack.removeEnchantment(getGlowEnchantment());
		return itemStack;
	}
}

