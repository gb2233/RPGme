package com.rpgme.plugin.util;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;

public class Symbol {

	private static final String piece = "ᚋᚌᚍᚎᚏ ᚏᚎᚍᚌᚋ";

	public static final String
			lineTop 	= "§7§m-----------"+piece+"---------",
			lineBottom = "§7§m---------"+piece+"-----------",
			configButton = "§7[§8X§7]";

	public static final String LINE = " &7&m----------------------------------------",
			FRAME_TOP_PIECE = " ╔  ",
			FRAME_END_PIECE = " ╚  ",
			FRAME_TOP = " ╔" + StringUtils.repeat('═', 25) + "╗",
			FRAME_LINE = " ║  ",
			FRAME_END = " ╚" + StringUtils.repeat('═', 25) + "╝";
	
	/*
	 * Ⓐ Ⓑ Ⓒ Ⓓ Ⓔ Ⓕ Ⓖ Ⓗ Ⓘ Ⓙ Ⓚ Ⓛ Ⓜ Ⓝ Ⓞ Ⓟ Ⓠ Ⓡ Ⓢ Ⓣ Ⓤ Ⓥ Ⓦ Ⓧ Ⓨ Ⓩ
	 */
	
	public static final char[] numbers = {'➀', '➁', '➂', '➃', '➄', '➅', '➆', '➇', '➈', '➉'  };
		
	public static final char
	YES = '✔',
	NO = '✖',
	HEART = '❤',
	STAR_EMPTY = '✧',
	STAR_FULL = '✦',
	ARROW_RIGHT = '➛',
	ARROW_RIGHT2 = '➡',
	INFINITY = '∞',
	
	UNLOCK = '♜',
	UPGRADE = '♛',
	NOTIFICATION = '\u2691',
	
	COMBAT_SYMBOL = '⚔';
	
	public static final String BTN_YES = ChatColor.GREEN + "[ " + YES + "]  ";
	public static final String BTN_NO = ChatColor.RED + "[ " + NO + "]";

	public static final String DISPLAY_COMBAT = ChatColor.DARK_RED.toString() + COMBAT_SYMBOL + ChatColor.WHITE;

}
