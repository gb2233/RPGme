package com.rpgme.plugin.util;

import org.apache.commons.lang.time.FastDateFormat;

import java.util.Calendar;

public class TimeUtil {
	
	private final static FastDateFormat minuteFormat = FastDateFormat.getInstance("mm:ss");
		
	private static int currentSeconds() {
		Calendar c = Calendar.getInstance();
		return (c.get(Calendar.HOUR_OF_DAY)+ c.get(Calendar.MINUTE)*60 + c.get(Calendar.SECOND));
	}
	
	public static long milisToMidnight() {
		Calendar c = Calendar.getInstance();
		int seconds = 86400 - c.get(Calendar.HOUR_OF_DAY)*3600 - c.get(Calendar.MINUTE)*60 - c.get(Calendar.SECOND);
		return seconds * 1000;
	}
	
	public static String readableTime(long milis) {
		return FastDateFormat.getTimeInstance(FastDateFormat.MEDIUM).format(milis);
	}
	
	public static int timeUntil(String time) {
		int sec = secondsInDay(time);
		int cur = currentSeconds();
		if(cur > sec)
			sec += 3600*24;
		
		return sec - cur;
	}
	
	public static int secondsInDay(String time) {
		String[] arr = time.split(":");
		return Integer.valueOf(arr[0])*3600 + Integer.valueOf(arr[1])*60;
	}
	
	public static String readableTimeUntil(int sec) {
		if(sec > 120)
			return (sec/3600) + "h " + ((sec % 3600)/60) + "m";
		else
			return (sec/60) + "m " + (sec % 60)+"s";
	}
	
	public static String readableAsMinuteFormat(int sec) {
		int min = (sec/60);
		return (min > 0 ? min + "m" : "") + sec%60 + "s";
	}
	
	public static String readableTime(String time) {
		String[] arr = time.split(":");
		int hour = Integer.valueOf(arr[0]);
		while(hour >= 24) hour -= 24;
		int min = Integer.valueOf(arr[1]);
		while(min >= 60) min -= 60;
		
		boolean pm = hour >= 12;
		if(pm) hour -= 12;
		
		String m = String.valueOf(min);
		
		return String.valueOf(hour) + ":" + (m.length() == 1 ? "0"+m : m) + " " + (pm ? "PM" : "AM");
		
	}
	
	public static String readableCurrentTime() {
		Calendar c = Calendar.getInstance();
		String time = (c.get(Calendar.HOUR_OF_DAY)) + ":" + c.get(Calendar.MINUTE);
		return readableTime(time);
	}
	
	public static String minutesSince(long timestamp) {
		return minuteFormat.format(System.currentTimeMillis() - timestamp);
	}
	
	public static boolean isFinished(long timestamp, int duration) {
		return System.currentTimeMillis() > (timestamp + (duration * 60 * 1000) );
	}
	
//	private final static FastDateFormat minuteFormat;// 05:00
//	private final static FastDateFormat ampmFormat; // 0:20 AM
//	private final static FastDateFormat hoursAndMinutesFormat; // 2h 15m
//	
//	private final static FastDateFormat simpleHourFormat;
//	
//	static {
//		TimeZone zone = getTimeZoneByOffset(Config.timeZone);
//		minuteFormat = FastDateFormat.getInstance("mm:ss", zone);
//		ampmFormat = FastDateFormat.getInstance("K:mm a");
//		hoursAndMinutesFormat = FastDateFormat.getInstance("H'h' m'm'");
//		simpleHourFormat = FastDateFormat.getInstance("HH:mm:ss");
//	}
//	
//	private static final long millisInHour = 3600000;
//	

//	
//	public static long timeUntil(String input) {
//		long time = readTimeInput(input);
//		long now = currentTimeOfDay();
//			
//		if(now > time)
//			time += 24*millisInHour;
//		
//		return time - now;
//	}
//	
//	public static String readableTimeUntil(long time) {
//		return hoursAndMinutesFormat.format(time);
//	}
//	public static String readableTime(String input) {
//		return ampmFormat.format(readTimeInput(input));
//	}
//	public static String readableCurrentTime() {
//		return ampmFormat.format(System.currentTimeMillis());
//	}
//	
//	private static long readTimeInput(String input) {
//		String[] arr = input.split(":");
//		return toMillis(Integer.parseInt(arr[0])-1, Integer.parseInt(arr[1]));
//	}
//	private static long currentTimeOfDay() {
//		String[] arr = simpleHourFormat.format(System.currentTimeMillis()).split(":");
//		return toMillis(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));
//	}
//	
//	private static long toMillis(int hours, int minutes) {
//		return toMillis(hours, minutes, 0);
//	}
//	private static long toMillis(int hours, int minutes, int seconds) {
//		return (hours*3600 + minutes*60 + seconds) * 1000;
//	}
//	
//	public static long millisToTicks(long time) {
//		return time / 50;
//	}

}
