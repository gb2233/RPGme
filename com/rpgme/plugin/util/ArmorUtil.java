package com.rpgme.plugin.util;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ArmorUtil {
	
	public static boolean wearingArmorInAllSlots(Player p) {
		for(ItemStack i : p.getEquipment().getArmorContents()) {
			if(i.getType() == Material.AIR)
				return false;
		}
		return true;
	}
	
	public static boolean hasEnchantedArmor(Player p) {
		for(ItemStack i : p.getEquipment().getArmorContents()) {
			if(!i.getEnchantments().isEmpty())
				return true;
		}
		return false;
	}
	
	public static boolean wearingArmorSet(Player p) {
		return fullArmorSet(p) != null;
	}
	
	public static String fullArmorSet(Player p) {
		
		String type = null;
		
		for(ItemStack i : p.getEquipment().getArmorContents()) {
			
			if(i == null || i.getType() == Material.AIR)
				return null;
			
			String name = i.getType().name();
			
			if(type == null)
				type = name.substring(0, name.indexOf('_'));
			else {
				// allows diamond boots to go with gold set
				if(type.equalsIgnoreCase("DIAMOND") && name.equalsIgnoreCase("GOLD_LEGGINGS")) {
					type = "GOLD";
				}
				
				if(!name.startsWith(type))
					return null;
				
			}
			
		}
		
		return type;
	}
	
	public static boolean isArmor(ItemStack item) {
		if(item == null)
			return false;
		
		String name = item.getType().name();
		return name.endsWith("_CHESTPLATE") || name.endsWith("_LEGGINGS") || name.endsWith("_BOOTS") || name.endsWith("_HELMET");
	}

	public static boolean isLight(ItemStack item) {
		if(item == null || item.getType() == Material.AIR)
			return false;
		String name = item.getType().name();
		return name.startsWith("LEATHER") || name.startsWith("CHAINMAIL");
	}

	public static boolean isHeavy(ItemStack item) {
		if(item == null || item.getType() == Material.AIR)
			return false;
		String name = item.getType().name();
		return name.startsWith("IRON") || name.startsWith("GOLD") || name.startsWith("DIAMOND");
	}

}
