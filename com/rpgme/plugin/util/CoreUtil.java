package com.rpgme.plugin.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import java.util.*;

public class CoreUtil {

	public static final Random random = new Random();

    public static List<Vector> outwardsVectors(int amount) {
		List<Vector> set = new ArrayList<Vector>();
		for(int i = 0; i < amount; i++) {

			double a = 2*Math.PI / amount * i;
			double x = Math.cos(a);
			double z = Math.sin(a);

			set.add(new Vector(x,0,z));
		}
		return set;
	}

	public static Set<Vector> sphereVectors(int yAngles, int circleAngles) {
		Set<Vector> set = new HashSet<Vector>();
		double angleDif = Math.PI / yAngles;

		for(int i = 0; i < yAngles; i++) {
			double angle = angleDif * i;

			for(int j = 0; j < circleAngles; j++) {

				double a = 2*Math.PI / circleAngles * j;
				double x = Math.cos(a);
				double y = Math.cos(angle);
				double z = Math.sin(a);

				Vector vector = new Vector(x,y,z);
				set.add(vector);
			}
		}
		return set;
	}

	public static List<Location> circle(Location center, double radius, int amount) {
		List<Location> list = new ArrayList<Location>();

		for(int i = 0; i < amount; i++) {

			double angle = 2*Math.PI / amount * i;
			double x = Math.cos(angle) * radius;
			double z = Math.sin(angle) * radius;

			list.add(center.clone().add(x, 0, z));
		}
		return list;
	}

	public static boolean isLeftClick(org.bukkit.event.block.Action a) {
		return a == org.bukkit.event.block.Action.LEFT_CLICK_AIR || a == org.bukkit.event.block.Action.LEFT_CLICK_BLOCK;
	}

	public static boolean isRightClick(org.bukkit.event.block.Action a) {
		return a == org.bukkit.event.block.Action.RIGHT_CLICK_AIR || a == org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;
	}

	/**
	 * Get the nearest face on the given block relative to the given location.
	 *
	 * @param block
	 * @param loc
	 * @return
	 */
	public static BlockFace getNearestFace(Block block, Location loc) {
		Location bLoc = block.getLocation().add(0.5, 0.5, 0.5);
		Vector v = loc.toVector().subtract(bLoc.toVector());
		float min = Float.MAX_VALUE;
		BlockFace wantedFace = null;
		for (BlockFace face : new BlockFace[] {
				BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST, BlockFace.DOWN, BlockFace.UP }) {
			Vector v1 = new Vector(face.getModX(), face.getModY(), face.getModZ());
			float angle = v.angle(v1);
			if (angle < min) {
				min = angle;
				wantedFace = face;
			}
		}
		return wantedFace;
	}

	public static Vector addNaturalOffset(Vector vec, double factor) {
		double x = (random.nextDouble() - .5) * 2 * factor;
		double y = (random.nextDouble() - .5) * 2 * factor;
		double z = (random.nextDouble() - .5) * 2 * factor;
		return vec.clone().add(new Vector(x,y,z));
	}
	
	public static Vector rotate(Vector vec, double degrees) {
		double radians = Math.toRadians(degrees);
		double rx = (vec.getX() * Math.cos(radians)) - (vec.getZ() * Math.sin(radians));
	    double rz = (vec.getX() * Math.sin(radians)) + (vec.getZ() * Math.cos(radians));
	    return new Vector(rx, vec.getY(), rz);
	}

	public static boolean hasClickAction(Material type) {
		// if a block of this type is clicked, no projectile will be launched instead, if holding a snowball/bow/etc in hand

		if (type == null || !type.isBlock()) return false;
		switch (type) {
		case DISPENSER:
		case NOTE_BLOCK:
		case BED_BLOCK:
		case WORKBENCH:
		case FURNACE:
		case BURNING_FURNACE:
		case WOODEN_DOOR:
		case LEVER:
		case IRON_DOOR_BLOCK:
		case STONE_BUTTON:
		case JUKEBOX:
		case CAKE_BLOCK:
		case DIODE_BLOCK_OFF:
		case DIODE_BLOCK_ON:
		case TRAP_DOOR:
		case FENCE_GATE:
		case ENCHANTMENT_TABLE:
		case BREWING_STAND:
		case DRAGON_EGG:
		case ENDER_CHEST:
		case COMMAND:
		case BEACON:
		case WOOD_BUTTON:
		case ANVIL:
		case TRAPPED_CHEST:
		case CHEST:
		case REDSTONE_COMPARATOR_OFF:
		case REDSTONE_COMPARATOR_ON:
		case HOPPER:
		case DROPPER:

			return true;
		default:
			return false;

		}
	}




}
