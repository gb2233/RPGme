package com.rpgme.plugin.player;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.command.PartyCommand;
import com.rpgme.plugin.event.SkillExpGainEvent;
import com.rpgme.plugin.util.Symbol;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.cooldown.Cleanable;
import com.rpgme.plugin.util.cooldown.CooldownCleaner;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class PartyModule implements ListenerModule {

    public static final int INVITE_TIMEOUT = 90000; // 1m30s
    private static final String BUTTON_ACCEPT = "  [ " + Symbol.YES + " ]  ";
    private static final String BUTTON_DECLINE = "[" + Symbol.NO + "]";

    private static final List<RPGPlayer> EMPTY_LIST = Collections.emptyList();

    private InviteMap pendingInvites = new InviteMap();

    private final RPGme plugin;

    public PartyModule(RPGme plugin) {
        this.plugin = plugin;
    }

    public int getPartyShareDistance() {
        return (int) Math.ceil(Math.pow(plugin.getConfig().getInt("Parties.share exp range", 0), 2));
    }


    @Override
    public void onEnable() {
        plugin.getCommandManager().register(new PartyCommand(plugin, this));
        CooldownCleaner.register(pendingInvites, plugin);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        LegacyConfig.injectMessage(messages, "party_chatformat", "party_no_invites",
                "party_invite", "party_invite_sent",
                "party_joined", "party_decline",
                "party_left", "party_empty",
                "party_leavefirst", "party_noparty");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {

    }

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
        RPGPlayer player = plugin.getPlayer(event.getPlayer());
        if(player != null) {
            onLeaveCommand(player);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onKick(PlayerKickEvent event) {
        onLogout(new PlayerQuitEvent(event.getPlayer(), ""));
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onExpGain(SkillExpGainEvent event) {
        RPGPlayer player = event.getRPGPlayer();
        List<RPGPlayer> partyMembers = player.getPartyMembers();
        if(partyMembers.size() < 2) {
            return;
        }
        float amount = event.getExp();
        int partyShareDistance = getPartyShareDistance();
        if(partyShareDistance >= 0) {

            List<RPGPlayer> shares = new ArrayList<>(partyMembers.size());
            Location location = player.getPlayer().getLocation();
            for(RPGPlayer member : partyMembers) {
                if(!location.getWorld().getUID().equals(member.getPlayer().getWorld().getUID()))
                    continue;

                if(partyShareDistance == 0 || member.getPlayer().getLocation().distanceSquared(location) < partyShareDistance * partyShareDistance)
                    shares.add(member);
            }

            amount /= shares.size();
            // set the event to 1 share
            event.setExp(amount);

            // register exp to all other party members
            for(RPGPlayer member : shares) {
                if(member != player) {
                    member.addTrueExp(event.getSkill(), amount);
                }
            }

        }
    }

    /**
     * Call to send a message to the players chat channel
     */
    public void doPartyChat(RPGPlayer player, String message) {
        if(!player.isInParty()) {
            player.getPlayer().sendMessage(plugin.getMessages().getMessage("party_noparty"));
        } else {

            String chatLine = plugin.getMessages().getMessage("party_chatformat", player.getPlayer().getName(), message);
            for(RPGPlayer member : player.getPartyMembers()) {
                member.getPlayer().sendMessage(chatLine);
            }

        }
    }

    /**
     * Sets these two players to be party members from now on. This does not send invites first to do so.
     * This will fail if both are in a party already, else it will be made or joined.
     * @return success
     */
    public boolean setAsPartyMembers(RPGPlayer one, RPGPlayer two) {
        // if both are in a party, one has to leave first.
        if(one.isInParty() && two.isInParty()) {
            one.getPlayer().sendMessage(plugin.getMessages().getMessage("party_leavefirst"));
            two.getPlayer().sendMessage(plugin.getMessages().getMessage("party_leavefirst"));
            return false;
        }

        // else if both are alone create a new party
        if(!one.isInParty() && !two.isInParty()) {

            List<RPGPlayer> set = new ArrayList<>(4);
            one.setPartyMembers(set);
            two.setPartyMembers(set);

            set.add(one);
            set.add(two);

            one.getPlayer().sendMessage(plugin.getMessages().getMessage("party_joined", two.getPlayer().getName()));
            two.getPlayer().sendMessage(plugin.getMessages().getMessage("party_joined", one.getPlayer().getName()));

        }

        // else we merge them together
        else {

            List<RPGPlayer> list;
            RPGPlayer toAdd;
            if(one.isInParty()) {
                list = one.getPartyMembers();
                two.setPartyMembers(list);
                toAdd = two;
            } else {
                list = two.getPartyMembers();
                one.setPartyMembers(list);
                toAdd = one;
            }

            for(RPGPlayer member : one.getPartyMembers()) {
                member.getPlayer().sendMessage(plugin.getMessages().getMessage("party_joined", toAdd.getPlayer().getName()));
                toAdd.getPlayer().sendMessage(plugin.getMessages().getMessage("party_joined", member.getPlayer().getName()));
            }

            list.add(toAdd);
        }
        return true;
    }

    public void onAcceptCommand(RPGPlayer player) {
        // check if there is invite to accept
        RPGPlayer two = pendingInvites.onAcceptCommand(player);
        if(two == null) {
            // no?
            player.getPlayer().sendMessage(plugin.getMessages().getMessage("party_no_invites"));
        } else {
            // yes!
            setAsPartyMembers(player, two);
        }
    }

    public void onDeclineCommand(RPGPlayer player) {
        pendingInvites.onDeclineCommand(player);
        player.getPlayer().sendMessage(plugin.getMessages().getMessage("party_decline"));
    }

    public void onInviteCommand(RPGPlayer player, RPGPlayer target) {
        // first check if target has already invited player
        Iterator<PartyInvite> it = pendingInvites.iterator();
        for(PartyInvite invite = it.next(); it.hasNext(); ) {
            if(invite.getInviter().equals(target) && invite.getInvited().equals(player)) {
                setAsPartyMembers(player, target);
                it.remove();
                return;
            }
        }

        // else create and register invite
        pendingInvites.add(new PartyInvite(player, target));

        // invite message
        ComponentBuilder builder = new ComponentBuilder(plugin.getMessages().getMessage("party_invite", player.getPlayer().getName()));
        builder.append(BUTTON_ACCEPT).color(ChatColor.GREEN).bold(true).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rpgparty acceptinvite"));
        builder.append(BUTTON_DECLINE).color(ChatColor.RED).bold(false).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rpgparty declineinvite"));
        // send
        target.getPlayer().spigot().sendMessage(builder.create());
        player.getPlayer().sendMessage(plugin.getMessages().getMessage("party_invite_sent"));
    }

    /**
     * Mars that this players wants to leave his party
     */
    public void onLeaveCommand(RPGPlayer player) {
        if(player.getPartyMembers().isEmpty()) {
            player.getPlayer().sendMessage(plugin.getMessages().getMessage("party_noparty"));

        } else {
            // notify
            player.getPlayer().sendMessage(plugin.getMessages().getMessage("party_empty"));
            // remove
            player.getPartyMembers().remove(player);
            player.setPartyMembers(EMPTY_LIST);

            // notify others
            for(RPGPlayer member : player.getPartyMembers()) {
                member.getPlayer().sendMessage(plugin.getMessages().getMessage("party_left", player.getPlayer().getName()));
            }
            // if there is only one member left, notify him and destroy set
            if(player.getPartyMembers().size() < 2) {
                RPGPlayer other = player.getPartyMembers().iterator().next();
                other.getPlayer().sendMessage(plugin.getMessages().getMessage("party_empty"));
                other.setPartyMembers(EMPTY_LIST);
            }
        }
    }


    public static class PartyInvite {

        final RPGPlayer invited, inviter;
        final long timeout;

        public PartyInvite(RPGPlayer inviter, RPGPlayer invited) {
            super();
            this.invited = invited;
            this.inviter = inviter;
            this.timeout = System.currentTimeMillis() + INVITE_TIMEOUT;
        }

        public RPGPlayer getInvited() {
            return invited;
        }
        public RPGPlayer getInviter() {
            return inviter;
        }
        public long getTimeout() {
            return timeout;
        }


    }

    private static class InviteMap extends ArrayList<PartyInvite> implements Cleanable {

        /**
         * Find first instance that has player as invited and removes and retuns the inviter
         */
        public RPGPlayer onAcceptCommand(RPGPlayer player) {
            Iterator<PartyInvite> it = iterator();
            for(PartyInvite invite = it.next(); it.hasNext(); ) {
                if(invite.getInvited().equals(player)) {
                    it.remove();
                    return invite.getInviter();
                }
            }
            return null;
        }

        /**
         * Remove all instances that have player as invited
         */
        public void onDeclineCommand(RPGPlayer player) {
            Iterator<PartyInvite> it = iterator();
            for(PartyInvite invite = it.next(); it.hasNext(); ) {
                if(invite.getInvited().equals(player)) {
                    invite.getInviter().getPlayer().sendMessage(RPGme.getInstance().getMessages().getMessage("party_decline"));
                    it.remove();
                }
            }
        }

        /** removes expired invites */
        @Override
        public synchronized void cleanUp() {
            Iterator<PartyInvite> iterator = iterator();
            long currentTime = System.currentTimeMillis();
            while(iterator.hasNext()) {
                PartyInvite entry = iterator.next();
                if(currentTime > entry.getTimeout())
                    iterator.remove();
            }
        }




    }

}
