package com.rpgme.plugin.player;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.api.RPGPlayerProfile;
import com.rpgme.plugin.player.PlayerIO.FileLoader;
import com.rpgme.plugin.player.PlayerIO.FileSaver;
import com.rpgme.plugin.player.PlayerIO.SQLLoader;
import com.rpgme.plugin.player.PlayerIO.SQLSaver;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class PlayerManager implements ListenerModule {

    public static final int MODE_SQL = 1;
    public static final int MODE_FILE = 0;

    private final Map<UUID, RPGPlayer> playerMap;
    private final RPGme plugin;

    private int mode = MODE_FILE;

    private boolean scoreboardEnabled;
    private double requirementMultiplier, gainMultiplier, pvpMultiplier, spawnerMultiplier;
    private boolean survivalOnly;

    public PlayerManager(RPGme plugin) {
        this.plugin = plugin;
        playerMap = new HashMap<>(8);
    }

    @Override
    public void onEnable() {
        for(Player player : plugin.getServer().getOnlinePlayers()) {
            loadPlayer(player.getUniqueId());
        }
    }

    @Override
    public void onDisable() {
        playerMap.values().forEach(this::saveProfile);
        if(autoSaveTaskID != 0) {
            getPlugin().getServer().getScheduler().cancelTask(autoSaveTaskID);
        }
    }

    @Override
    public void createConfig(ConfigBuilder configBuilder, BundleBuilder bundleBuilder) {
        configBuilder
                .addLargeHeader("RPGme Plugin configuration")
                .addValue("Time in minutes between saving player data. Set to 0 to disable. This only has an effect when your server crashes to reduce the data roll-back",
                    "Auto save", 45).addNewline()
                .addValue("Option to disable scoreboards server-wide in case it is not compatible with other plugins\nNote that if this is true, players can still disable them for themselves",
                    "Scoreboard enabled", true).addNewline()

                .addLargeHeader("Exp Settings")
                .addValue("The exp required to gain levels. Changing this may cause players levels to change, as the requirments are now different.",
                        "Exp Required Multiplier", 1.0).addNewline()
                .addValue("A global multiplier for all exp received from all skills.",
                        "Exp Gain Multiplier", 1.0).addNewline()
                .addValue("Only receive exp while in survival mode",
                        "Survival Only", true).addNewline()
                .addValue("The following two settings apply to combat skills only\nexp multiplier for actions against other players",
                        "PvP exp", 0.25).addNewline()
                .addValue("exp multiplier for actions against entities spawned from spawners",
                        "Mobspawner exp", 0.2).addNewline()
                .addValue("A level at which skills should be 'completed'. Setting this higher should make perkd and abilities take longer to unlock or improve.\n" +
                        "This can be used together with 'Exp Gain Multiplier' to adjust the level scale of the whole pluign.",
                        "Target Level", 100).addNewline();

        configBuilder.addLargeHeader("Modules");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        requirementMultiplier = config.getDouble("Exp Required Multiplier");
        gainMultiplier = config.getDouble("Exp Gain Multiplier");
        pvpMultiplier = config.getDouble("PvP exp");
        spawnerMultiplier = config.getDouble("Mobspawner exp");
        survivalOnly = config.getBoolean("Survival Only");

        scoreboardEnabled = config.getBoolean("Scoreboard enabled");
        startAutoSaveTask(config.getInt("Auto save"));
    }

    public RPGme getPlugin() {
        return plugin;
    }

    /**
     * @return either MODE_SQL or MODE_FILE
     */
    public int getStorageMode() {
        return mode;
    }

    public boolean isScoreboardEnabled() {
        return scoreboardEnabled;
    }

    /**
     * Gets the RPGPlayer associated with a specified player.
     * @param player the player
     * @return the RPGPlayer, or null if none found
     */
    public RPGPlayer get(Player player) {
        return player == null ? null : playerMap.get(player.getUniqueId());
    }

    public boolean isLoaded(Player player) { return player != null && playerMap.containsKey(player.getUniqueId()); }

    public int getLevel(Player player, int skill) {
        return get(player).getLevel(skill);
    }

    public float getExp(Player player, int skill) {
        return get(player).getExp(skill);
    }

    /** configuration getter */
    public double getRequirementMultiplier() {
        return requirementMultiplier;
    }
    /** configuration getter */
    public double getGainMultiplier() {
        return gainMultiplier;
    }
    /** configuration getter */
    public double getPvpMultiplier() {
        return pvpMultiplier;
    }
    /** configuration getter */
    public double getSpawnerMultiplier() {
        return spawnerMultiplier;
    }
    /** configuration getter */
    public boolean isSurvivalOnly() {
        return survivalOnly;
    }

    @EventHandler
    public void onLogin(PlayerJoinEvent event) {
        loadPlayer(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        remove(event.getPlayer());
    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        remove(event.getPlayer());
    }

    protected void remove(Player player) {
        RPGPlayer rp = playerMap.remove(player.getUniqueId());
        if(rp != null) {
            saveProfileAsync(rp);
        }
    }

    protected void loadPlayer(UUID id) {
        if(mode == MODE_SQL) {
            new SQLLoader(this, id).runTaskAsynchronously(plugin);
        } else {
            new FileLoader(this, id).runTaskAsynchronously(plugin);
        }
    }

    protected void saveProfileAsync(RPGPlayerProfile profile) {
        if(mode == MODE_SQL) {
            new SQLSaver(this, profile).runTaskAsynchronously(plugin);
        } else {
            new FileSaver(this, profile).runTaskAsynchronously(plugin);
        }
    }

    protected void saveProfile(RPGPlayerProfile profile) {
        if(mode == MODE_SQL) {
            new SQLSaver(this, profile).run();
        } else {
            new FileSaver(this, profile).run();
        }
    }

    public File getDataFile(UUID id) {
        return new File(plugin.getUserDataFolder(), id + ".dat");
    }

    protected void onLoadComplete(UUID id, RPGPlayer result) {
        playerMap.put(id, result);
    }

    private int autoSaveTaskID;

    protected void startAutoSaveTask(int mins) {
        if(autoSaveTaskID != 0) {
            getPlugin().getServer().getScheduler().cancelTask(autoSaveTaskID);
        }
        long dur = mins * 60 * 20;
        autoSaveTaskID = new BukkitRunnable() {
            @Override
            public void run() {
                playerMap.values().forEach(PlayerManager.this::saveProfileAsync);
            }
        }.runTaskTimer(getPlugin(), dur, dur).getTaskId();
    }



}
