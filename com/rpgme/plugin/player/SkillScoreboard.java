package com.rpgme.plugin.player;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.Symbol;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;

public class SkillScoreboard {
	
	private final Scoreboard scoreboard;
	private final RPGPlayer p;

	public SkillScoreboard(RPGPlayer p) {
		this.p = p;
		scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
	}

	public void build() {
		
		for(String s : scoreboard.getEntries()) {
			scoreboard.resetScores(s);
		}
		for(Team t : scoreboard.getTeams()) {
			t.unregister();
		}
		
		Objective obj = scoreboard.getObjective(DisplaySlot.SIDEBAR);
		if(obj == null) {
			obj = scoreboard.registerNewObjective("RPGmestats", "levels");
			
			boolean showCombat = RPGme.getInstance().getConfig().getBoolean("Show Combat Level", true);
			
			String display = (showCombat ? Symbol.DISPLAY_COMBAT + p.getSkillSet().getCombatLevel() : "")
					+ " " + p.getPlugin().getMessages().getMessage("ui_scoreboard_title");
			obj.setDisplayName(display);
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		} 
		
		// sort in decending order

		List<Skill> list = Lists.newArrayList((Iterable<Skill>)RPGme.getInstance().getSkillManager().getSkills());
		list.sort((x, y) -> Integer.compare(p.getLevel(x.getId()), p.getLevel(y.getId())) * -1);
		List<String> lines = new ArrayList<>(list.size());
		
		for(Skill skill : list) {
			
			int lvl = p.getLevel(skill);
			
			String entry = p.getPlugin().getSkillManager().colorLight() + lvl + ChatColor.GRAY + " - "
					+ p.getPlugin().getSkillManager().colorLight() + skill.getDisplayName();
			
			if(entry.length() <= 16) {
				lines.add(entry);

			} else {
				Team team = scoreboard.registerNewTeam(skill.getName());
				team.setSuffix(StringUtil.trimToSize(entry.substring(16), 16));
				entry = entry.substring(0, 16);
				team.addEntry(entry);
				lines.add(entry);
			}
		}
		
				
		int index = lines.size()+1;
		for(String s : lines) {
			obj.getScore(s).setScore(index--);
		}
		
		Player player = p.getPlayer();
		if(player != null) {
			player.setScoreboard(scoreboard);
		}

	}
	
	private int taskID = 0;
	
	public void cancelScheduledRemoval() {
		Bukkit.getServer().getScheduler().cancelTask(taskID);
	}
	
	public void scheduleRemoval(int afterSeconds) {		
		taskID =
		new BukkitRunnable() {
			public void run() {
				
				Player player = p.getPlayer();
				if(player != null) {
					scoreboard.getObjective(DisplaySlot.SIDEBAR).unregister();
					player.setScoreboard(Bukkit.getServer().getScoreboardManager().getMainScoreboard());
				}				
				
			}
		}.runTaskLater(p.getPlugin(), afterSeconds*20).getTaskId();
	}

}
