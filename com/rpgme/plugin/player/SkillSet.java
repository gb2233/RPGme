package com.rpgme.plugin.player;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.util.IntMap;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.LinkedHashMap;
import java.util.Map;


public class SkillSet implements ConfigurationSerializable {

	
	protected final IntMap<ExpEntry> skillMap;
	private int total;
	
	private RPGPlayer player;

	protected SkillSet() {
		SkillManager manager = RPGme.getInstance().getModule(RPGme.MODULE_SKILL_MANAGER);
		this.skillMap = new IntMap<>(manager.getSkillCount());
	}

	protected void setOwner(RPGPlayer player) {
		this.player = player;
	}

	public int getTotalLevel() {
        if(total == 0) {
            recalculateTotalLevel();
        }
		return total;
	}
	
	public void recalculateTotalLevel() {
		total = 0;
		for(ExpEntry entry : skillMap.values()) {
			total += entry.getLevel();
		}
	}
	
	public int getCombatLevel() {
		return ( getLevel(SkillType.ATTACK) +
				getLevel(SkillType.DEFENCE) +
				getLevel(SkillType.STAMINA) )
				* 12 / 10  + (getLevel(SkillType.ARCHERY) +
				getLevel(SkillType.SUMMONING) / 2) / 4;
	}
		
	public int getAverageLevel() {
		return (int) Math.round((double)getTotalLevel() / skillMap.size);
	}


	public float getExp(int skill) {
		ExpEntry value = skillMap.get(skill);
		if(value == null)
			return 0;
		else return value.getExp();
	}

	public void setExp(int skill, float amount) {
		ExpEntry value = skillMap.get(skill);
		if(value == null) {
			value = new ExpEntry();
			skillMap.put(skill, value);
		}
		
		value.setExp(amount);
		if(player != null) {
			while(shouldLevelUp(skill)) {
				levelUp(skill);
			}
		}
	}

	public void addExp(int skill, float amount) {
		setExp(skill, getExp(skill)+amount);
	}

	public int getLevel(int skill) {
		ExpEntry value = skillMap.get(skill);
		if(value == null)
			return 0;
		
		return value.getLevel();
	}

	private boolean shouldLevelUp(int skill) {
		return ExpTables.xpForLevel(getLevel(skill)+1) <= (int) getExp(skill);
	}

	private void levelUp(int skill) {
		total++;
		player.onLevelup(skill);
	}
	
	public void recalculateLevel(int skill) {
		ExpEntry exp = skillMap.get(skill);
		exp.setLevel(ExpTables.getLevelAt((int) exp.getExp()));
		recalculateTotalLevel();
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		SkillManager manager = RPGme.getInstance().getSkillManager();
		for(IntMap.Entry<ExpEntry> e : skillMap.entries()) {
			if(e.value.getExp() > 1f) {
				String name = manager.getById(e.key).getName();
				map.put(name, (int) e.value.getExp());
			}
		}
		return map;
	}
	
	public static class ExpEntry {
		
		private float exp;
		private int level;
		
		protected ExpEntry(float exp, int level) {
			this.exp = exp;
			this.level = level;
		}
		protected ExpEntry() {
			this(0f, 1);
		}
		
		public float getExp() {
			return exp;
		}
		public void setExp(float exp) {
			this.exp = exp;
		}
		
		public int getLevel() {
			return level;
		}
		public void setLevel(int level) {
			this.level = level;
		}
		
	}

	

}
