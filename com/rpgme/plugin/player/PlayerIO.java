package com.rpgme.plugin.player;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.RPGPlayerProfile;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.event.RPGPlayerJoinEvent;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.config.YamlFile;

import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class PlayerIO {

    /* ////////////////////////// Loaders ///////////////////////////// */
    private abstract static class Loader extends BukkitRunnable {

        protected final PlayerManager manager;
        protected final UUID id;

        public Loader(PlayerManager manager, UUID id) {
            this.manager = manager;
            this.id = id;
        }

        protected void finish(final RPGPlayer player) {
            player.getSkillSet().recalculateTotalLevel();

            new BukkitRunnable() {
                public void run() {
                    manager.onLoadComplete(player.getUniqueId(), player);
                    // call event (sync)
                    manager.getPlugin().getServer().getPluginManager().callEvent(new RPGPlayerJoinEvent(player));
                    // set scoreboard
                    player.updateScoreboard();
                }
            }.runTask(manager.getPlugin());
        }
    }

    public static class SQLLoader extends Loader {

        public SQLLoader(PlayerManager plugin, UUID id) {
            super(plugin, id);
        }

        @Override
        public void run() {

        }
    }

    public static class FileLoader extends Loader {

        public FileLoader(PlayerManager plugin, UUID id) {
            super(plugin, id);
        }

        @Override
        public void run() {
            YamlFile file =  new YamlFile(manager.getDataFile(id));
            file.read();

            // initialize map and objects
            SkillSet skillSet = new SkillSet();
            RPGPlayer player = new RPGPlayer(manager, id, skillSet);
            skillSet.setOwner(player);

            // read stats and settings
            Map<String, Object> map = file.data.getValues(true);
            SkillManager skillManager = manager.getPlugin().getModule(RPGme.MODULE_SKILL_MANAGER);

            // check file entries as key_value pairs
            for(Map.Entry<String, Object> e : map.entrySet()) {
                Skill skill = skillManager.getByName(e.getKey());
                if (skill != null) {
                    if (skill.isEnabled()) {
                        // register as skill if available and enabled
                        Integer exp = read(e.getValue());
                        if(exp == null) {
                            // register as setting if not a value
                            player.setSetting(e.getKey(), e.getValue().toString());
                        } else {
                            SkillSet.ExpEntry entry = new SkillSet.ExpEntry(exp, ExpTables.getLevelAt(exp));
                            skillSet.skillMap.put(skill.getId(), entry);
                        }
                    }
                } else {
                    // register as setting if not a skill
                    player.setSetting(e.getKey(), e.getValue().toString());
                }
            }

            // register empty missing skills
            for(Skill skill : skillManager.getSkills()) {
                if(!skillSet.skillMap.containsKey(skill.getId()))
                    skillSet.skillMap.put(skill.getId(), new SkillSet.ExpEntry(0f,1));
            }
            // report back
            finish(player);
        }

        private Integer read(Object obj) {
            if(obj == null)
                return null;
            try {
                return Integer.valueOf(obj.toString());
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    /* ////////////////////////// Savers ///////////////////////////// */

    public abstract static class Saver extends BukkitRunnable {

        protected final PlayerManager manager;
        protected final SkillManager skillManager;
        protected final RPGPlayerProfile[] player;

        public Saver(PlayerManager manager, RPGPlayerProfile... player) {
            this.manager = manager;
            skillManager = manager.getPlugin().getSkillManager();
            this.player = player;
        }
    }

    public static class SQLSaver extends Saver {
        public SQLSaver(PlayerManager manager, RPGPlayerProfile... player) {
            super(manager, player);
        }

        @Override
        public void run() {

        }
    }

    public static class FileSaver extends Saver {

        public FileSaver(PlayerManager manager, RPGPlayerProfile... player) {
            super(manager, player);
        }

        @Override
        public void run() {
            // the map of files to store
            final Map<UUID, Map<String, Object>> serialized = new HashMap<UUID, Map<String, Object>>(player.length);

            for(RPGPlayerProfile profile : player) {
                // inner map representing one profile
                Map<String, Object> serializedProfile = new HashMap<>(20);
                // register all skills
                SkillSet skillSet = profile.getSkillSet();
                synchronized (skillSet) {
                    for (IntMap.Entry<SkillSet.ExpEntry> entry : skillSet.skillMap.entries()) {
                        int exp = (int) entry.value.getExp();
                        if (exp > 1) {
                            serializedProfile.put(skillManager.getById(entry.key).getName(), exp);
                        }
                    }
                }
                // register settings
                Map<String, String> settings = profile.getSettings();
                synchronized (settings) {
                    serializedProfile.putAll(settings);
                }

                serialized.put(profile.getUniqueId(), serializedProfile);
            }

            // write the files to disk
            for(Map.Entry<UUID, Map<String, Object>> e : serialized.entrySet()) {

                UUID id = e.getKey();
                YamlFile file = new YamlFile(manager.getDataFile(id));

                file.clear();
                for(Map.Entry<String, Object> map : e.getValue().entrySet()) {
                    file.data.set(map.getKey(), map.getValue());
                }
                file.write();
            }
        }
    }

}
