package com.rpgme.content;

import com.rpgme.content.module.ChatFormatter;
import com.rpgme.content.module.Healthbars;
import com.rpgme.content.module.HighscoreModule;
import com.rpgme.content.module.moblevel.MonsterLevels;
import com.rpgme.content.skill.alchemy.Alchemy;
import com.rpgme.content.skill.archery.Archery;
import com.rpgme.content.skill.attack.Attack;
import com.rpgme.content.skill.defence.Defence;
import com.rpgme.content.skill.enchanting.Enchanting;
import com.rpgme.content.skill.farming.Farming;
import com.rpgme.content.skill.fishing.Fishing;
import com.rpgme.content.skill.forging.Forging;
import com.rpgme.content.skill.landscaping.Landscaping;
import com.rpgme.content.skill.magic.Magic;
import com.rpgme.content.skill.mining.Mining;
import com.rpgme.content.skill.stamina.Stamina;
import com.rpgme.content.skill.taming.Taming;
import com.rpgme.content.skill.woodcutting.Woodcutting;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.command.RPGmeCommand;
import com.rpgme.plugin.command.SkillDisplayCommand;
import com.rpgme.plugin.command.SkillOverviewCommand;
import com.rpgme.plugin.manager.CommandManager;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.player.PartyModule;

import static com.rpgme.content.skill.SkillType.*;
import static com.rpgme.plugin.RPGme.*;

/**
 *
 */
public class Launcher {

    public static void registerDefaultContent(RPGme plugin) {
        // skills
        SkillManager skillManager = plugin.getSkillManager();

        skillManager.registerSkill(ALCHEMY, new Alchemy());
        skillManager.registerSkill(ARCHERY, new Archery());
        skillManager.registerSkill(ATTACK, new Attack());
        skillManager.registerSkill(DEFENCE, new Defence());
        skillManager.registerSkill(ENCHANTING, new Enchanting());
        skillManager.registerSkill(FARMING, new Farming());
        skillManager.registerSkill(FISHING, new Fishing());
        skillManager.registerSkill(FORGING, new Forging());
        skillManager.registerSkill(LANDSCAPING, new Landscaping());
        skillManager.registerSkill(MAGIC, new Magic());
        skillManager.registerSkill(MINING, new Mining());
        skillManager.registerSkill(STAMINA, new Stamina());
        skillManager.registerSkill(TAMING, new Taming());
        skillManager.registerSkill(WOODCUTTING, new Woodcutting());

        // extra modules
        plugin.registerModule(MODULE_PARTIES, new PartyModule(plugin));
        plugin.registerModule(MODULE_HIGHSCORES, new HighscoreModule(plugin));
        plugin.registerModule(MODULE_HEALTH_BARS, new Healthbars(plugin));
        plugin.registerModule(MODULE_MOB_LEVELS, new MonsterLevels(plugin));
        plugin.registerModule(MODULE_CHAT_PLACEHOLDERS, new ChatFormatter());

        // commands
        CommandManager commandManager = plugin.getCommandManager();
        commandManager.register(new RPGmeCommand(plugin));
        commandManager.register(new SkillDisplayCommand(plugin));
        commandManager.register(new SkillOverviewCommand(plugin));
    }

}
