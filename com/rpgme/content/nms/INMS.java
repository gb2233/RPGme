package com.rpgme.content.nms;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class INMS {
	
	public interface INMSUtil {
		
		Player getNearestPlayer(Location loc, double range);
		
		Player getNearestPlayer(World world, double x, double y, double z, double range);
		
		void setAbsorptionHearts(LivingEntity p, float extra);
		
		float getAbsorptionHearts(LivingEntity p);

		void setInvisible(Entity entity, boolean value);
		
		void setMovementSpeed(Entity entity, double value);
		
		boolean isFromMobspawner(Entity entity);
		
		boolean canPickup(Arrow arrow);
		void setCanPickup(Arrow arrow, boolean value);
		
	}
	
	public interface IPacketSender {
		
		void doArmSwing(Player p);
		
		void sendToActionbar(Player player, String message);
		
		void sendTitleLenght(Player player, int fadein, int length, int fadeout);
		void sendTitle(Player player, String title);
		void sendSubtitle(Player player, String title);
		
		void setWorldborderEffect(Player p, int value);
		
	}
	
	public interface IItemTag {
		
		ItemStack addSimpleTag(ItemStack item, String key);
		
		ItemStack addStringTag(ItemStack item, String key, String value);
		
		ItemStack addIntegerTag(ItemStack item, String key, int value);
		
		ItemStack addIntegerArrayTag(ItemStack item, String key, int[] value);
		
		ItemStack addFloatTag(ItemStack item, String key, float value);
		
		boolean hasTag(ItemStack item, String key);
		
		String getString(ItemStack item, String key);
		
		int getInteger(ItemStack item, String key);
		
		int[] getIntegerArray(ItemStack item, String key);
		
		float getFloat(ItemStack item, String key);
		
		ItemStack remove(ItemStack item, String key);
		
	}
	
	public interface INBTStorageFile {

		boolean hasKey(String key);

		void remove(String key);

		boolean getBoolean(String key);

		double getDouble(String key);

		float getFloat(String key);

		int getInt(String key);

		int[] getIntArray(String key);

		String getString(String key);

		void setBoolean(String key, boolean value);

		void setDouble(String key, double value);

		void setFloat(String key, float value);

		void setInt(String key, int value);

		void setIntArray(String key, int[] value);

		void setString(String key, String value);

		void setStringList(String key, Collection<String> value);

		List<String> getStringList(String key);

		Set<String> getKeys();

		void clear();

		void read();

		void save();

	}

}
