package com.rpgme.content.nms.v1_9_R1;

import com.rpgme.content.nms.INMS.IPacketSender;

import net.minecraft.server.v1_9_R1.IChatBaseComponent;
import net.minecraft.server.v1_9_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_9_R1.Packet;
import net.minecraft.server.v1_9_R1.PacketPlayOutAnimation;
import net.minecraft.server.v1_9_R1.PacketPlayOutChat;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_9_R1.PacketPlayOutWorldBorder;
import net.minecraft.server.v1_9_R1.PacketPlayOutWorldBorder.EnumWorldBorderAction;
import net.minecraft.server.v1_9_R1.WorldBorder;

import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PacketSender implements IPacketSender {

	@Override
	public void doArmSwing(Player p) {
		Packet armSwing = new PacketPlayOutAnimation( ((CraftPlayer)p).getHandle(), 0);
		for(Player other : p.getWorld().getPlayers()) {
			((CraftPlayer)other).getHandle().playerConnection.sendPacket(armSwing);
		}
	}

	public void sendToActionbar(Player player, String message) {
		IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(ppoc);
	}
	
	public void sendTitleLenght(Player player, int fadein, int length, int fadeout) {
		PacketPlayOutTitle packet = new PacketPlayOutTitle(fadein, length, fadeout);
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
	}
	
	public void sendTitle(Player player, String title) {
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + title + "\"}");
		PacketPlayOutTitle packet = new PacketPlayOutTitle(EnumTitleAction.TITLE, cbc);
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
		
	}
	
	public void sendSubtitle(Player player, String title) {
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + title + "\"}");
		PacketPlayOutTitle packet = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, cbc);
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);	
	}
	
	public void setWorldborderEffect(Player p, int percent) {
		PacketPlayOutWorldBorder ppowb;
		WorldBorder fakeborder = new WorldBorder();
		
		if(percent > 99) {
			fakeborder.setCenter(p.getLocation().getX()+10000, 0);
			fakeborder.setSize(1);
			fakeborder.setWarningDistance(0);
		} else if(percent > 0) {

			fakeborder.setSize(20000);
			fakeborder.setWarningDistance(10000);
			fakeborder.setCenter(p.getLocation().getX()+(percent*100), p.getLocation().getZ());

		} else {
			// TODO: set actual border
			
		}
		ppowb = new PacketPlayOutWorldBorder(fakeborder, EnumWorldBorderAction.INITIALIZE);
		
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppowb);
	}

}
