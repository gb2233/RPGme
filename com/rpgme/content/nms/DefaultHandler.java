package com.rpgme.content.nms;

import com.rpgme.content.nms.INMS.INMSUtil;
import com.rpgme.content.nms.INMS.IPacketSender;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class DefaultHandler implements INMSUtil, IPacketSender {

	@Override
	public void doArmSwing(Player p) {
				
	}

	@Override
	public void sendToActionbar(Player player, String message) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
	}

	@Override
	public void sendTitleLenght(Player player, int fadein, int length, int fadeout) {
		
	}

	@Override
	public void sendTitle(Player player, String title) {

	}

	@Override
	public void sendSubtitle(Player player, String title) {
		
	}

	@Override
	public void setWorldborderEffect(Player p, int value) {
		
	}

	@Override
	public Player getNearestPlayer(Location loc, double range) {
		double distance = -1;
		Player player = null;
		
		for(Player p : loc.getWorld().getPlayers()) {
			
			double dis = loc.distanceSquared(p.getEyeLocation());
			if(distance < 0 || distance > dis) {
				player = p;
				distance = dis;
			}
		}
		return player;
	}

	@Override
	public Player getNearestPlayer(World world, double x, double y, double z,
			double range) {
		return getNearestPlayer(new Location(world, x, y, z), 0);
	}

	@Override
	public void setAbsorptionHearts(LivingEntity p, float extra) {
		
	}

	@Override
	public float getAbsorptionHearts(LivingEntity p) {
		return 0f;
	}

	@Override
	public void setInvisible(Entity entity, boolean value) {
		
	}

	@Override
	public void setMovementSpeed(Entity entity, double value) {
		
	}

	@Override
	public boolean isFromMobspawner(Entity entity) {
		return false;
	}

	@Override
	public boolean canPickup(Arrow arrow) {
		return false;
	}

	@Override
	public void setCanPickup(Arrow arrow, boolean value) {

	}
	
	

}
