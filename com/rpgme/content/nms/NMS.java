package com.rpgme.content.nms;

import com.rpgme.content.nms.INMS.INMSUtil;
import com.rpgme.content.nms.INMS.IPacketSender;
import com.rpgme.plugin.util.nbtlib.NBTFactory;


public class NMS {

    public static final INMSUtil util;

    public static final IPacketSender packets;

    private static final boolean hooked;

    static {
        String version = NBTFactory.VERSION;

        switch(version) {

//		case "v1_8_R3": {
//			util = new com.rpgme.content.nms.v1_8_R3.NMSUtil();
//			packets = new com.rpgme.content.nms.v1_8_R3.PacketSender();
//			hooked = true;
//			break;
//		}
//
            case "v1_9_R1": {
                util = new com.rpgme.content.nms.v1_9_R1.NMSUtil();
                packets = new com.rpgme.content.nms.v1_9_R1.PacketSender();
                hooked = true;
                break;
            }

            case "v1_9_R2": {
                util = new com.rpgme.content.nms.v1_9_R2.NMSUtil();
                packets = new com.rpgme.content.nms.v1_9_R2.PacketSender();
                hooked = true;
                break;
            }

            case "v1_10_R1": {
                util = new com.rpgme.content.nms.v1_10_R1.NMSUtil();
                packets = new com.rpgme.content.nms.v1_10_R1.PacketSender();
                hooked = true;
                break;
            }

            default: {
                DefaultHandler defaultHandler = new DefaultHandler();
                hooked = false;
                util = defaultHandler;
                packets = defaultHandler;
                System.out.println("[RPGme] Spigot NMS version "+version + " NOT supported. Some functions may be disabled or not working.");
                break;
            }
        }

    }

    public static boolean isHooked() {
        return hooked;
    }


}
