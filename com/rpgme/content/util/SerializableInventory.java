package com.rpgme.content.util;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SerializableInventory implements ConfigurationSerializable {

	private final ItemStack[] inv;

	public SerializableInventory(ItemStack[] content) {
		inv = content;
	}

	public ItemStack[] getContent() {
		return inv;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new HashMap<String, Object>();

		if(inv != null) {
			for(int i = 0; i < inv.length; i++) {
				ItemStack item = inv[i];
				if(item != null)
					map.put(String.valueOf(i), item);
			}
		}
		return map;
	}

	public static SerializableInventory deserialize(Map<String, Object> map) {
		ItemStack[] items = new ItemStack[54];

		for(Entry<String, Object> e : map.entrySet()) {
			try {
				int slot = Integer.parseInt(e.getKey());
				items[slot] = (ItemStack) e.getValue();
			} catch(NumberFormatException exc) { continue; }
		}

		return new SerializableInventory(items);
	}

}
