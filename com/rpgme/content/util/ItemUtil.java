package com.rpgme.content.util;

import com.google.common.collect.Sets;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.NBTConstants;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import com.rpgme.plugin.util.nbtlib.Tag;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ItemUtil {

	public static ItemStack create(Material mat, @Nullable String name, @Nullable String lore) {
		return create(mat, null, name, lore);
	}

	public static ItemStack create(Material mat, @Nullable Short data, @Nullable String name, @Nullable String lore) {
		return create(null, mat, data, name, lore);
	}

	public static ItemStack create(Tag tag, Material mat, @Nullable Short data, @Nullable String name, @Nullable String lore) {
		ItemStack item = new ItemStack(mat, 1, (data == null ? (short) 0 : data));

		if(tag != null) {
			CompoundTag compound = tag.getTypeId() == NBTConstants.TYPE_COMPOUND ? (CompoundTag) tag : new CompoundTag().putTag(tag.getName(), tag);
			item = NBTFactory.setCompoundTag(item, compound);
		}
		ItemMeta meta = item.getItemMeta();

		if(name != null)
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		if(lore != null)
			meta.setLore(StringUtil.colorize(StringUtil.splitToLength(" &7", lore, " ", 35)));

		if(data != null) {
			item.setDurability(data);
		}
		item.setItemMeta(meta);
		return item;

	}

	public static ItemStack setName(ItemStack item, String name) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name != null ? ChatColor.translateAlternateColorCodes('&', name) : null);
		item.setItemMeta(meta);
		return item;
	}

	public static boolean take(Inventory inv, ItemStack mat, int amount) {
		Set<Integer> toRemove = Sets.newHashSet();

		ItemStack[] content = inv.getContents();
		for(int i = 0; i < content.length; i++) {
			ItemStack item = content[i];

			if(mat.isSimilar(item)) {

				item.setAmount(item.getAmount() - amount);
				if(item.getAmount() <= 0) {
					toRemove.add(i);
					amount = -item.getAmount();
				} else {
					amount = 0;
					break;
				}

			}
		}

		for(Integer i : toRemove) {
			inv.setItem(i, null);
		}

		return amount == 0;
	}

	public static boolean take(Inventory inv, Material mat, int amount) {
		Set<Integer> toRemove = Sets.newHashSet();

		for(Entry<Integer, ? extends ItemStack> e : inv.all(mat).entrySet()) {
			e.getValue().setAmount(e.getValue().getAmount() - amount);

			if(e.getValue().getAmount() <= 0) {
				toRemove.add(e.getKey());
				amount = -e.getValue().getAmount();
			} else {
				amount = 0;
				break;
			}

		}

		for(Integer i : toRemove) {
			inv.setItem(i, null);
		}

		return amount == 0;
	}

	public static void give(Player p, ItemStack... item) {
		Map<Integer, ItemStack> left = p.getInventory().addItem(item);
		p.updateInventory();
		if(left != null && !left.isEmpty()) {
			for(ItemStack l : left.values()) 
				p.getWorld().dropItemNaturally(p.getLocation(), l);
		}
	}

	public static ItemStack addItemFlags(ItemStack item, ItemFlag... flags) {
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(flags);
		item.setItemMeta(meta);
		return item;
	}

	public static ItemStack removeItemFlags(ItemStack item, ItemFlag... flags) {
		ItemMeta meta = item.getItemMeta();
		meta.removeItemFlags(flags);
		item.setItemMeta(meta);
		return item;
	}

	public static boolean isType(ItemStack item, Material... types) {
		if(item == null)
			return false;
		for(Material m : types)
			if(item.getType() == m)
				return true;
		return false;
	}

	public static ItemStack addToLore(ItemStack item, String... entries) {
		ItemMeta meta = item.getItemMeta();
		List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<String>();

		for(String s : StringUtil.colorize(entries)) {
			lore.add(s);
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}

	public static ItemStack addToLore(ItemStack item, List<String> entries) {
		return addToLore(item, entries.toArray(new String[entries.size()]));
	}

	public static void removeLastLore(ItemStack item, int rows) {
		ItemMeta meta = item.getItemMeta();
		if(!meta.hasLore())
			return;

		List<String> lore = meta.getLore();
		if(lore.size() <= rows)
			lore.clear();
		else {
			for(int i = 0; i < rows; i++) {
				lore.remove(lore.size()-1);
			}
		}
		meta.setLore(lore);
		item.setItemMeta(meta);

	}

    public static ItemStack[] getArmorContent(Material boots, Material legs, Material chest, Material helm) {
        return new ItemStack[] { new ItemStack(boots), new ItemStack(legs), new ItemStack(chest), new ItemStack(helm)};
    }

    public static ItemStack setMaxStackSize(ItemStack is, int amount){
        try {
            Class<?> CraftItemStack = Class.forName(NBTFactory.CRAFT_PATH + ".inventory.CraftItemStack");
            Object nmsItem = CraftItemStack.getMethod("asNMSCopy", ItemStack.class).invoke(is);
            //net.minecraft.server.v1_8_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(is);
            nmsItem.getClass().getMethod("getItem").invoke(nmsItem).getClass().getMethod("c", int.class).invoke(nmsItem, amount);
            //nmsIS.getItem().c(amount);
            return (ItemStack) CraftItemStack.getMethod("asBukkitCopy", nmsItem.getClass()).invoke(null, nmsItem);
            //return CraftItemStack.asBukkitCopy(nmsIS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return is;
    }


}
