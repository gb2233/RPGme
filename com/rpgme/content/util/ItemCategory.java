package com.rpgme.content.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemCategory {
	
	public static ItemCategory matchValue(String string) {
		if(string == null)
			return null;
		
		switch(string) {
		case "weapon": return WEAPON;
		case "tool": return TOOL;
		case "bow": return BOW;
		case "armour":
		case "armor": return ARMOR;
		case "rod": return ROD;
		default: return null;
		}
	}
	
	public static final ItemCategory
	WEAPON = new ItemCategory("Weapons", "SWORD", "_AXE"),
	
	BOW = new ItemCategory("Bows", "BOW"),
	
	ARMOR = new ItemCategory("Armor", "BOOTS", "LEGGINGS", "CHESTPLATE", "HELMET"),
	
	TOOL = new ItemCategory("Tools", "AXE", "SPADE", "SHEARS", "HOE"),
	
	ROD = new ItemCategory("Fishing Rods", "FISHING_ROD");
	

	public static final ItemCategory ALL = new CombinedCategory("All", WEAPON, BOW, ARMOR, TOOL, ROD);
	
	private final String name;
	private final String[] suffixes;
	
	private ItemCategory(String name, String... suffixes) {
		this.name = name;
		this.suffixes = suffixes;
	}
	
	public boolean isItem(ItemStack item) {
		if(item == null)
			return false;
		return isMaterial(item.getType());
	}
	


	public boolean isMaterial(Material material) {
		String type = material.name();
		for(String suffix : suffixes) {
			if(type.endsWith(suffix)) 
				return true;
			
		}
		return false;
	}
	
	@Override
	public String toString() {
		return name;
	}

	
	public static class CombinedCategory extends ItemCategory {
		
		private final ItemCategory[] categories;
		
		public CombinedCategory(String name, ItemCategory... categories) {
			super(name);
			this.categories = categories;
		}

		@Override
		public boolean isItem(ItemStack item) {
			for(ItemCategory cat : categories) {
				if(cat.isItem(item))
					return true;
			}
			return false;
		}

        @Override
        public boolean isMaterial(Material material) {
            for(ItemCategory cat : categories) {
                if(cat.isMaterial(material))
                    return true;
            }
            return false;
        }

        public static CombinedCategory of(ItemCategory... categories) {
			return new CombinedCategory("Multiple", categories);
		}
	}


}
