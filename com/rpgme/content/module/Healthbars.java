package com.rpgme.content.module;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.Symbol;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

public class Healthbars implements ListenerModule {

	private static final String KEY = "OriginalName";
	
	private final StringBuilder builder = new StringBuilder();
	private String type;
	private boolean showLevel;

	private final RPGme plugin;

	public Healthbars(RPGme plugin) {
		this.plugin = plugin;
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		config.beginSection("Healthbars")
				.addValue("enabled", true)
				.addValue("The type of healthbar to use. Choose one of three: [hearts, bar, stats]", "type", "hearts")
				.endSection();
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		type = config.getString("Healthbars.type");
		showLevel = config.getBoolean("Mob Spawning.assign levels", true) && config.getBoolean("Mob Spawning.show level", false);
	}

	@Override
	public void onDisable() {
		for(World world : plugin.getServer().getWorlds()) {
			for(Chunk chunk : world.getLoadedChunks()) {
				onUnload(chunk);
			}
		}
	}

	@EventHandler (ignoreCancelled = true, priority = EventPriority.MONITOR)
	public void onDamage(EntityDamageEvent e) {
		if(e.getEntityType() != EntityType.PLAYER && e.getEntity() instanceof LivingEntity) {
			LivingEntity mob = (LivingEntity)e.getEntity();
			double newhealth = mob.getHealth() - e.getFinalDamage();
			setDisplayName(mob, newhealth);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDeath(PlayerDeathEvent e) {
		String deathMessage = e.getDeathMessage();
		if(deathMessage == null || deathMessage.isEmpty())
			return;
		
		EntityDamageEvent lastEvent = e.getEntity().getLastDamageCause();
		if(!(lastEvent instanceof EntityDamageByEntityEvent))
			return;

		Entity lastDamager = ((EntityDamageByEntityEvent)lastEvent).getDamager();
		String currentName = lastDamager.getCustomName();
		if(currentName == null)
			return;
		
		for(MetadataValue value : lastDamager.getMetadata(KEY)) {
			if(value.getOwningPlugin().equals(plugin)) {
				
				String origionalName = (String) value.value();
				if(origionalName == null)
					origionalName = StringUtil.reverseEnum(lastDamager.getType().name());
				
				e.setDeathMessage(deathMessage.replace(currentName, origionalName));
			}
		}


	}

	@EventHandler
	public void onUnload(ChunkUnloadEvent e) {
		onUnload(e.getChunk());
	}

	private void onUnload(Chunk chunk) {
		for(Entity entity : chunk.getEntities()) {
			if(entity instanceof LivingEntity)
				resetDisplayName((LivingEntity)entity);
		}
	}

	public void resetDisplayName(LivingEntity e) {
		boolean success = false;
		for(MetadataValue value : e.getMetadata(KEY)) {
			if(value.getOwningPlugin().equals(plugin)) {

				String origional = (String) value.value();
				e.setCustomName(origional);
				success = true;
				break;
			}
		}
		if(success) {
			e.removeMetadata(KEY, plugin);
		}
	}

	public void setDisplayName(LivingEntity e, double health) {
		switch(type) {
		case "hearts": buildHearts(e, health); break;
		case "stats": buildStats(e, health); break;
		case "bar": buildBar(e, health); break;
		}
		if(showLevel) {
			buildLevel(e);
		}
		if(builder.length() > 0) {

			if(!e.hasMetadata(KEY)) { // if metadatavalue can handle null values, this should work properly without hasCustomName()
				e.setMetadata(KEY, new FixedMetadataValue(plugin, e.getCustomName()));
			}
			e.setCustomName(builder.toString());
			builder.setLength(0);
		}
	}



	private void buildHearts(LivingEntity e, double health) {

		int length = (int) Math.min(e.getMaxHealth()/2, 10);

		int hearts = health <= 0.0 ? 0 : (int) Math.ceil(health / e.getMaxHealth() * length);

		builder.append(ChatColor.DARK_RED).append(StringUtils.repeat(Symbol.HEART, hearts))
		.append(ChatColor.GRAY).append(StringUtils.repeat(Symbol.HEART, length-hearts));
	}

	private void buildStats(LivingEntity e, double health) {

		builder.append(getColor(health)).append((int)Math.ceil(health)).append(ChatColor.GRAY)
		.append(" / ").append((int)Math.ceil(e.getMaxHealth()));

	}

	private void buildBar(LivingEntity e, double health) {

		int length = (int) Math.min(e.getMaxHealth(), 20);
		int green = health <= 0.0 ? 0 : (int) Math.ceil(health / e.getMaxHealth() * length);

		builder.append(getColor(health)).append(StringUtils.repeat('|', green)).append(ChatColor.DARK_GRAY)
		.append(StringUtils.repeat('|', length - green));

	}
	
	public static ChatColor getColor(double health) {
		return health < 4 ? ChatColor.RED : health < 12 ? ChatColor.YELLOW : ChatColor.GREEN;
	}

	private void buildLevel(LivingEntity e) {
		int level = 0;
		for(MetadataValue value : e.getMetadata("CombatLevel")) {
			if(value.getOwningPlugin().equals(plugin)) {
				level = value.asInt();
				break;
			}
		}

		if(level > 0) {
			builder.append(' ').append(Symbol.DISPLAY_COMBAT).append(level);
		}

	}

}
