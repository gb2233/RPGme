package com.rpgme.content.module.moblevel;

import com.rpgme.content.module.moblevel.Outfits.EntityOutfit;
import com.rpgme.content.nms.NMS;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.EntityTypes;
import com.rpgme.plugin.util.math.MathUtils;
import com.rpgme.plugin.util.math.Scaler;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;


public class MonsterLevels implements ListenerModule {

	private final RPGme plugin;
	private Outfits equipment;

	private Scaler movementSpeed, healthScale;
	private static final String META_COMBAT_LEVEL = "CombatLevel";
    private double difficulty;

	public MonsterLevels(RPGme plugin) {
		this.plugin = plugin;
	}

	@Override
	public void onEnable() {
		equipment = new Outfits();
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        config.beginSection("Module that assigns combat levels to most types of mobs. The level is based on the combat levels of the nearest player. " +
                "Higher level mobs have more health and do more damage.", "Mob levels")
                .addValue("enabled", true)
                .addValue("A slider in the range of 1.0 to 10.0. This affect the assigned levels and buffs.", "difficulty", 6.0);
        // todo: add difficulty 1.0 - 10.0
		equipment.createConfig(config, messages);

        config.endSection();
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
        difficulty = MathUtils.clamp(config.getDouble("difficulty"), 1.0, 11.0);
		equipment.onLoad(config, messages);
        int targetLevel = (int) (plugin.getSkillManager().getTargetLevel() * 2.5);

        movementSpeed = new Scaler(1, 0.17, targetLevel, 0.33);
        healthScale = new Scaler(1, 0.6, targetLevel, difficulty);
	}

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onSpawn(CreatureSpawnEvent e) {
		if(e.getSpawnReason() != SpawnReason.NATURAL || !(shouldAlter(e.getEntity())))
			return;

		Player closest = NMS.util.getNearestPlayer(e.getLocation(), 100);
		if(closest == null)
			return;

		RPGPlayer rp = plugin.getPlayer(closest);
		if(rp == null)
			return;

		int playerLevel = rp.getSkillSet().getCombatLevel();
		int mobLevel = (int) Math.round(MathUtils.clamp((CoreUtil.random.nextDouble() * 2 - 1 + difficulty/8) * difficulty  + playerLevel, 0, plugin.getSkillManager().getTargetLevel() * 2.5));

		LivingEntity entity = e.getEntity();
		entity.setMetadata(META_COMBAT_LEVEL, new FixedMetadataValue(plugin, mobLevel));
		
		EntityOutfit outfit = equipment.getArmoredOutfit(mobLevel);

		outfit.setHealth(entity.getMaxHealth() * healthScale.scale(mobLevel));

		outfit.setSpeed(movementSpeed.scale(mobLevel));

		outfit.equip(entity);
		if(e.getEntityType() == EntityType.SKELETON && ((Skeleton)entity).getSkeletonType() == SkeletonType.NORMAL) {
			outfit.withWeapon(entity, new ItemStack(Material.BOW)).withOffhand(entity, null);
		}		

	}


	private boolean shouldAlter(Entity e) {
		return EntityTypes.isHostile(e) || EntityTypes.isNetherMob(e);
	}






}
