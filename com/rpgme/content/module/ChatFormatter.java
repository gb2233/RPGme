package com.rpgme.content.module;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.player.RPGPlayer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatFormatter implements ListenerModule {

	private final String[] tags = {"{TOTAL_LEVEL}", "{COMBAT_LEVEL}", "{AVERAGE_LEVEL}"},
			regex = {"\\{TOTAL_LEVEL\\}", "\\{COMBAT_LEVEL\\}", "\\{AVERAGE_LEVEL\\}"};
	

	private String replacementForTag(RPGPlayer player, int index) {
		switch(index) {
		case 0: return String.valueOf(player.getSkillSet().getTotalLevel());
		case 1: return String.valueOf(player.getSkillSet().getCombatLevel());
		case 2: return String.valueOf(player.getSkillSet().getAverageLevel());
		
		default: return "";		
		}
	}

	@EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onChat(AsyncPlayerChatEvent e) {
		RPGPlayer player = null;
		String format = e.getFormat();
		
		for(int i = 0; i < tags.length; i++) {
			if(format.contains(tags[i])) {
				if(player == null) {
					player = RPGme.getInstance().getPlayer(e.getPlayer());
				}
				format = format.replaceFirst(regex[i], replacementForTag(player, i));
			}
		}
		e.setFormat(format);
	}

}
