package com.rpgme.content.event;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

public class AnvilCraftEvent extends InventoryInteractEvent {

	private static final HandlerList handlers = new HandlerList();

	private final InventoryView view;
	private final Player player;
	private AnvilRecipe recipe = null;

	public AnvilCraftEvent(InventoryView view, Player player) {
		super(view);
		this.view = view;
		this.player = player;
	}
	
	public AnvilCraftEvent(InventoryView view, Player player, AnvilRecipe recipe) {
		super(view);
		this.view = view;
		this.player = player;
		this.recipe = recipe;
	}

	public ItemStack getCraftResult() {
		return view.getItem(2);
	}
	public void setCraftResult(ItemStack item) {
		view.setItem(2, item);
	}
	
	public ItemStack getCraftMaterial() {
		return view.getItem(1);
	}
	public void setCraftMaterial(ItemStack item) {
		view.setItem(1, item);
	}
	
	public ItemStack getCraftOrigional() {
		return view.getItem(0);
	}
	public void setCraftOrigional(ItemStack item) {
		view.setItem(0, item);
	}

	public boolean isRepair() {
		return getCraftOrigional() != null && getCraftMaterial() != null && getCraftMaterial().getType() != Material.ENCHANTED_BOOK;
	}
	public boolean isEnchant() {
		return getCraftOrigional() != null && getCraftMaterial() != null && getCraftMaterial().getType() == Material.ENCHANTED_BOOK;
	}

	public Player getPlayer() {
		return player;
	}
	
	public AnvilRecipe getRecipe() {
		if(recipe == null) {
			recipe = AnvilRecipeListener.findRecipe(view);
		}
		return recipe;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

//	private static boolean registered = false;
//
//	public static void registerEvent(RPGme plugin) {
//		if(!registered) {
//			new AnvilCraftEventListener(plugin).registerListeners();
//			registered = true;
//		}
//	}



}
