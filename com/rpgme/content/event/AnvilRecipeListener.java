package com.rpgme.content.event;

import com.rpgme.content.util.ItemUtil;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Listener;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import static org.bukkit.event.inventory.InventoryAction.*;

public class AnvilRecipeListener extends Listener<RPGme> {

	private static AnvilRecipeListener instance = null;

	private static void initialize() {
		if(instance == null) {
			instance = new AnvilRecipeListener(RPGme.getInstance());
			instance.registerListeners();
		}
	}

	public static void registerRecipe(AnvilRecipe recipe) {
        initialize();
        if(recipe != null) {
            instance.recipes.add(recipe);
        }
	}
	public static AnvilRecipe findRecipe(InventoryView view) {
		ItemStack[] currentIngredients = {view.getItem(0), view.getItem(1) };

		for(AnvilRecipe recipe : instance.recipes) {
			if(recipe.matches(currentIngredients)) {
				return recipe;
			}
		}
		return null;
	}
	

	private final List<AnvilRecipe> recipes = new ArrayList<>();

	private final Set<InventoryAction> checkActions = EnumSet.of(MOVE_TO_OTHER_INVENTORY, PLACE_ALL, PLACE_ONE, PLACE_SOME, SWAP_WITH_CURSOR);

	private AnvilRecipeListener(RPGme plugin) {
		super(plugin);
	}
		
	
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent e) {		
		if(e.getView().getType() != InventoryType.ANVIL) {
			return;
		}

		final InventoryView view = e.getView();

		if(e.getSlot() == 2 && e.getClickedInventory() == e.getView().getTopInventory()) {

			ItemStack result = e.getCurrentItem();
			if(result == null || result.getType() == Material.AIR || !e.isLeftClick())
				return;
			
			AnvilRecipe recipe = findRecipe(e.getView());

			AnvilCraftEvent event = new AnvilCraftEvent(view, (Player) e.getWhoClicked(), recipe);
			Bukkit.getPluginManager().callEvent(event);

            if(recipe == null || event.isCancelled())
				return;

            // take the result
			if(e.isShiftClick()) {
				ItemUtil.give((Player) e.getWhoClicked(), result);
				view.setItem(2, null);

			} else {
				ItemStack cursor = view.getCursor();
				view.setCursor(result);
				view.setItem(2, cursor);
			}

			// take ingredients
			recipe.removeIngredients(view);
			return;
		}

		if(!checkActions.contains(e.getAction()))
			return;

		new BukkitRunnable() {
			public void run() {

				ItemStack[] currentIngredients = {view.getItem(0), view.getItem(1) };

				for(AnvilRecipe recipe : recipes) {
					if(recipe.matches(currentIngredients)) {
						view.getTopInventory().setItem(2, recipe.getResult(view));
						break;
					}
				}
			}
		}.runTask(plugin);

	}


}


