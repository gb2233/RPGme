package com.rpgme.content.skill.woodcutting;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.rpgme.plugin.blockmeta.PlayerPlacedListener;
import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.MessageUtil;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.VarEnergyPerSecCooldown;
import com.rpgme.plugin.util.math.Scaler;
import nl.flamecore.util.Maths;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class Timber extends Ability<Woodcutting> {

	private static final Vector[] checkArea;
	static {
		List<Vector> list = new ArrayList<Vector>();
		for(int x = -1; x <= 1; x++) {
			for(int y = 1; y >= 0; y--) {
				for(int z = -1; z <= 1; z++) {
					list.add(new Vector(x,y,z));
				}
			}
		}
		list.remove(new Vector());
		checkArea = list.toArray(new Vector[list.size()]);
	}

	private final Cooldown cooldown = new VarEnergyPerSecCooldown(plugin, 40, 100);
	private Scaler cooldownSpeed;

	private int unlock, blockLimit;

	public Timber(Woodcutting skill) {
		super(skill, "Timber", Woodcutting.ABILITY_TIMBER);


	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 15)
                .addValue("Maximum amount of blocks per tree, including leaves", "max size", 250);
        LegacyConfig.injectNotification(messages, getClass(), "");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlock = getConfig().getInt("unlocked");
        blockLimit = getConfig().getInt("max size");
        cooldownSpeed = new Scaler(unlock, 1, 100, 8);

        addNotification(unlock, Notification.ICON_UNLOCK, getName(), LegacyConfig.getNotification(messages, getClass(), ""));
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlock) {
			list.add("Timber Max cooldown:" + StringUtil.readableDecimal(40 / cooldownSpeed.scale(forlevel))+"s");
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onAbility(PlayerInteractEvent e) {

		if(e.getAction() == Action.RIGHT_CLICK_BLOCK && 
				e.hasItem() && isAxe(e.getItem().getType()) && Woodcutting.isWood((e.getClickedBlock().getType())) &&
				!PluginIntegration.getInstance().isInClaim(e.getClickedBlock())) {

			Player p = e.getPlayer();

			if(!isEnabled(p))
				return;

			int level = getLevel(p);

			if(level >= unlock) {

				if(cooldown.isOnCooldown(p)) {
					sendOnCooldownMessage(p, cooldown, true);
					return;
				}

				if(!PluginIntegration.getInstance().canChange(p, e.getClickedBlock())) {
					p.sendMessage(getPlugin().getMessages().getMessage("err_nopermission"));
					GameSound.play(Sound.ENTITY_VILLAGER_NO, p);
					return;
				}

				long energypersec = (long) cooldownSpeed.scale(level)*1000;
				cooldown.add(p, energypersec);

				dropTree(e.getClickedBlock(), e.getPlayer());

			}

		}
	}

	@SuppressWarnings("deprecation")
	private void dropTree(Block start, Player player) {

		Set<Block> toDrop = new HashSet<Block>();
		Set<Block> toCheck = new HashSet<Block>();

		toCheck.add(start);

		boolean done = false;

		while(!done) {

			Set<Block> toAdd = new HashSet<Block>();

			for(Block b : toCheck) {

				if(toDrop.contains(b))
					continue;

				if(!PluginIntegration.getInstance().canChange(player, b))
					continue;

				for(Vector dir : checkArea) {

					Block other = b.getLocation().add(dir).getBlock();
					if((Woodcutting.isWood(other.getType()) && !PlayerPlacedListener.getInstance().isPlayerPlaced(other))
							|| Woodcutting.isBush(other.getType())) {
						toAdd.add(other);
					}
				}

				toDrop.add(b);

			}

			toCheck.addAll(toAdd);

			if(toDrop.size() >= blockLimit || toDrop.containsAll(toCheck))
				done = true;

		}

		//		skill.setSleeping(true);
		Vector dir = Vector.getRandom().setY(0.2);
		int woodblocks = 0;

		for(Block b : toDrop) {

			Material mat = b.getType();
			byte data = b.getData();

			if(Woodcutting.isBush(mat)) {

				//	LeavesDecayEvent event = new LeavesDecayEvent(b);
				//	manager.getServer().getPluginManager().callEvent(event);

				b.setType(Material.AIR, false);

				if(b.getRelative(BlockFace.UP).getType() == Material.SNOW) {
					b.getRelative(BlockFace.UP).setType(Material.AIR);
				}

				if(CoreUtil.random.nextDouble() < 0.06) {

					int height = b.getY() - start.getY();
					double speed = Maths.clamp((height+4)*0.08, 0.05, 1.0);
					b.getWorld().spawnFallingBlock(b.getLocation(), mat, data).setVelocity(dir.clone().multiply(speed));

				}


			} else {

				//	BlockBreakEvent event = new BlockBreakEvent( b, player);
				//	manager.getServer().getPluginManager().callEvent(event);
				woodblocks++;
				GameSound.play(Sound.BLOCK_WOOD_BREAK, b.getLocation());

				b.setType(Material.AIR);
				//displayParticles(b, player);

				int height = b.getY() - start.getY();
				double speed = Math.min(height * 0.02 + 0.1, 0.4);
				b.getWorld().spawnFallingBlock(b.getLocation(), mat, data).setVelocity(dir.clone().multiply(speed));

			}

		}

		float xp = woodblocks * 2.5f;
		addExp(player, xp);

	}

	private boolean isAxe(Material mat) {
		return mat.name().endsWith("_AXE");
	}


}
