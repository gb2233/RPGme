package com.rpgme.content.skill.defence;

import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.ArmorUtil;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.effect.ParticleEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;


public class GroundSlam extends Ability<Defence> {

	private int unlocked;

	public GroundSlam(Defence defence) {
		super(defence, "Ground Slam", Defence.ABILITY_GROUND_SLAM);

	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {


	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 50);
		LegacyConfig.injectNotification(messages, getClass(), "");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		unlocked = getConfig().getInt("unlocked");
		addNotification(unlocked, Notification.ICON_UNLOCK, getName(),
				LegacyConfig.getNotification(messages, getClass(), ""));

	}

	final Map<Player, Integer> chargeMap = new WeakHashMap<Player, Integer>();

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onFall(PlayerMoveEvent e) {

		Player p = e.getPlayer();

		if(p.isSneaking() && !p.isFlying() && !p.getLocation().getBlock().isLiquid() && isEnabled(p) && !((Entity)p).isOnGround() && getLevel(p) >= unlocked) {

			Integer charge = chargeMap.get(p);
			if(charge == null)
				charge = Integer.valueOf(0);

			doParticleEffect(p.getLocation(), ++charge - 5);
			chargeMap.put(p, charge);
		} else {
			chargeMap.remove(p);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	public void onLand(EntityDamageEvent e) {
		if(e.getCause() == DamageCause.FALL && e.getEntityType() == EntityType.PLAYER) {

			Integer charge = chargeMap.remove(e.getEntity());
			if(charge != null) {

				Player p = (Player) e.getEntity();
				
				if(!PluginIntegration.getInstance().canChange(p, p.getLocation().getBlock()))
					return;
				
				int level = getLevel(p);

				// 5 - 10 damage based on level
				double damage = (5 + (6.0/70*(level-25)) )
						* getArmorValue(p) * 1.2 // x1.2 armor bonus
						* Math.min((charge) / 10.0, 2); // per 10 charges

				double radius = Math.min(charge / 3, 4.0);

				doSlamEffect(p, damage, radius);
				
				// prevent death if died within 3 hearts
				if(e.getDamage() >= p.getHealth() && p.getHealth() - e.getDamage() > -6 ) {
					e.setDamage(p.getHealth() - 2);
				}
				
				// sound
				float volume = Math.max(0.5f, Math.min(2f, charge / 20f));
				GameSound.play(Sound.ENTITY_GENERIC_EXPLODE, p.getLocation(), volume, 1f);
			}
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onHit(EntityChangeBlockEvent e) {
		if(e.getEntity().hasMetadata("groundSlam")) {
			e.setCancelled(true);
			FallingBlock block = (FallingBlock) e.getEntity();
			block.setDropItem(false);
			block.remove();
		}

	}

	private void doSlamEffect(Player player, double damage, double range) {
		int amount = (int) Math.round(damage/1.6);
		spawnBlocks(player.getLocation().getBlock().getRelative(BlockFace.DOWN), amount);

		GameSound.play(Sound.ENTITY_GENERIC_EXPLODE, player.getLocation());

		for(Entity e : player.getNearbyEntities(range, range/2, range)) {
			if(e instanceof LivingEntity) {

				Vector out = e.getLocation().subtract(player.getLocation()).toVector().setY(0.5);
				e.setVelocity(out);

				((Damageable)e).damage(damage);
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void spawnBlocks(Block center, int amount) {

		int[][] cyl = com.rpgme.content.skill.mining.PowerTool.cylFlat;

		for(int i = 0; i < amount; i++) {

			int[] mod = cyl[i % cyl.length];
			Block b = center.getRelative(mod[0], mod[1], mod[2]);

			if(!b.getType().isSolid()) {
				continue;
			}
			
			Vector dir = new Vector(mod[0] * 0.2, 0.4, mod[1] * 0.2);

			FallingBlock block = b.getWorld().spawnFallingBlock(b.getLocation().add(0, 1.1, 0), b.getType(), b.getData());
			block.setMetadata("groundSlam", new FixedMetadataValue(plugin, true));
			block.setVelocity(dir);
			block.setDropItem(false);
		}

	}

	private double getArmorValue(Player p) {
		ItemStack[] armor = p.getEquipment().getArmorContents();
		int value = 0;
		for(int i = 0; i < 4; i++) {
			if(ArmorUtil.isHeavy(armor[i])) {
				value = (value > 0 ? value*2 : 2);
			}
		}
		value += 9;
		return value / 25.0;
	}

	private void doParticleEffect(Location loc, int amount) {
		if(amount > 0)
			ParticleEffect.BLOCK_CRACK.display(new ParticleEffect.BlockData(Material.BEDROCK, (byte)0),
					0.2f, 0f, 0.2f, 1, amount, loc, 32);

	}



}
