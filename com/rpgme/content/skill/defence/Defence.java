package com.rpgme.content.skill.defence;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.*;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class Defence extends BaseSkill {

	private final Set<DamageCause> causes = EnumSet.of(DamageCause.ENTITY_ATTACK, DamageCause.ENTITY_EXPLOSION,
			DamageCause.MAGIC, DamageCause.PROJECTILE);

	private final Scaler armorRating = new Scaler(1, 0.7, 100, 1.5);

	private Scaler knockbackReduction;
	private Scaler dodgeChance;

	public static final int ABILITY_SECOND_WIND = Id.newId();
	public static final int ABILITY_GROUND_SLAM = Id.newId();
	public static final int ABILITY_BULK_UP = Id.newId();

	public Defence() {
		super("Defence", SkillType.DEFENCE);
	}

	@Override
	public Material getItemRepresentation() {
		return Material.SHIELD;
	}

    @Override
    public void onEnable() {
        registerAbility(ABILITY_SECOND_WIND, new SecondWind(this));
        registerAbility(ABILITY_BULK_UP, new BulkUp(this));
        registerAbility(ABILITY_GROUND_SLAM, new GroundSlam(this));
    }

    @Override
	public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
		super.createConfig(config, bundle);

		config.addValue("This perk reduces the knockup from incoming attacks.", "Knockback reduction enabled", true);
		config.addValue("Knockback reduction unlocked", false);
		LegacyConfig.injectMessage(bundle, "notification_knockback");

		config.addValue("Gives a chance to dodge any incoming attack.", "Evasion enabled", true);
		config.addValue("Evasion unlocked", 15);
		LegacyConfig.injectMessage(bundle, "notification_evasion");

		LegacyConfig.injectMessage(bundle, "ability_evade");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);

		// in this case it was much more efficient to handle this in here instead a new module
		if(config.getBoolean("Evasion enabled", true)) {
			int unlocked = config.getInt("Evasion unlocked", 15);
			dodgeChance = new Scaler(unlocked, 5, 100, 30);
			addNotification(unlocked, Notification.ICON_PASSIVE, "Evasion", messages.getMessage("notification_evasion"));
		}

		if(config.getBoolean("Knockback reduction enabled")) {
			int unlocked = config.getInt("Knockback reduction unlocked");
			knockbackReduction = new Scaler(unlocked ,0.9, 100, 0.3);
			addNotification(unlocked, Notification.ICON_PASSIVE, "Knockback Reduction", messages.getMessage("notification_knockback"));
		}
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		list.add("Armor Efficiency:" + ((int) (armorRating.scale(forlevel)*100)) + '%');
		if(knockbackReduction != null && forlevel >= knockbackReduction.minlvl) {
			double reduction = (1.0 - knockbackReduction.scale(forlevel) ) * 100;
			list.add("Knockback Reduction:" + StringUtil.readableDecimal(reduction) + '%');
		}
		if(dodgeChance != null && forlevel >= dodgeChance.minlvl) {
			list.add("Dodge chance:"+dodgeChance.readableScale(forlevel) + '%');
		}
	}

	// scales armor values
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onArmorScale(EntityDamageEvent e) {
		if(e.getEntityType() != EntityType.PLAYER)
			return;

		Player p = (Player) e.getEntity();
		if(!isEnabled(p))
			return;

		double armor = e.getDamage(DamageModifier.ARMOR);

		if(armor < 0) {

			double percentage = -armor / e.getDamage();
			percentage *= armorRating.scale(getLevel(p));

			armor = -(e.getDamage() * Math.min(0.8, percentage));
			e.setDamage(DamageModifier.ARMOR, armor);

		}
	}


	// adds exp
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onDamageSecond(EntityDamageEvent e) {
		if(e.getEntityType() != EntityType.PLAYER || !causes.contains(e.getCause()))
			return;

		Player p = (Player) e.getEntity();
		if(!isEnabled(p))
			return;

		// evasion
		if(dodgeChance != null && (e.getCause() == DamageCause.ENTITY_ATTACK || e.getCause() == DamageCause.PROJECTILE)) {
			int level = getLevel(p);
			if(level >= dodgeChance.minlvl && dodgeChance.isRandomChance(level)) {
				e.setCancelled(true);
				addExp(p, 6f);
				doDodge(p);
				return;
			}
		}

		// register exp
		double baseXP = (e.getDamage(DamageModifier.ARMOR) + e.getDamage(DamageModifier.MAGIC) + e.getDamage(DamageModifier.BLOCKING)) * -3.5;

		if(e instanceof EntityDamageByEntityEvent) {
			Entity damager = ((EntityDamageByEntityEvent)e).getDamager();
			if(damager instanceof Projectile) {
				ProjectileSource source = ((Projectile)damager).getShooter();
				if(source instanceof Entity)
					damager = (Entity) source;
			}

			if(damager instanceof LivingEntity)
				baseXP *= Combat.getExpFactor((LivingEntity)damager);
		}

		addExp(p, (float) baseXP);

	}


	//Knockback reduction
	@EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onDamage(PlayerVelocityEvent e) {
		
		if(knockbackReduction == null) {
			PlayerVelocityEvent.getHandlerList().unregister(this);
			return;
		}
		
		Player p = e.getPlayer();
		if(!p.isSneaking() || !isEnabled(p))
			return;

		int level = getLevel(p);

		if(level < knockbackReduction.minlvl)
			return;

		double reduction = knockbackReduction.scale(level);

		if(e.getPlayer().getLocation().getBlock().isLiquid()) {
			reduction = Math.min(reduction, 0.5);
		}
		if(e.getPlayer().isBlocking()) {
			reduction = Math.min(reduction - 0.2, 0.1);
		}
		e.setVelocity(e.getVelocity().multiply(reduction));
	}

	// evasion
	public void doDodge(Player p) {
		Vector dir = p.getLocation().getDirection();
		double rotation = CoreUtil.random.nextBoolean() ? 75 : -75;
		Vector result = CoreUtil.rotate(dir, rotation).setY(0.2).multiply(0.75);
		p.setVelocity(result);

		GameSound.play(Sound.ENTITY_ENDERDRAGON_FLAP, p.getLocation(), 2f, 1f);
		MessageUtil.sendToActionBar(p, messages.getMessage("ability_evade"));
	}


}
