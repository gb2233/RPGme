package com.rpgme.content.skill.archery;

import com.rpgme.content.util.ItemUtil;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.MessageUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.cooldown.VarMaxEnergyCooldown;
import com.rpgme.plugin.util.math.Scaler;
import com.rpgme.plugin.util.math.Vec3D;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.WeakHashMap;

public class Volley extends Ability<Archery> {

	private int unlocked;
	private int upgradeInterval;

	private final VarMaxEnergyCooldown cooldown;
	private Scaler maxEnergy;

	private final Map<Player, Integer> map = new WeakHashMap<Player, Integer>();

	public Volley(Archery skill) {
		super(skill, "Volley", Archery.ABILITY_VOLLEY);
		cooldown = new VarMaxEnergyCooldown(plugin, 1, 20);
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 30);
		config.addValue("amount of levels needed to getEntry an extra arrow", "upgrade interval", 15);

		LegacyConfig.injectNotification(messages, getClass(), 1);
		LegacyConfig.injectNotification(messages, getClass(), "2+");
        LegacyConfig.injectMessage(messages, "ability_volley_loaded");
        LegacyConfig.injectMessage(messages, "ability_volley_unload");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		unlocked = config.getInt("unlocked");
		upgradeInterval = config.getInt("upgrade interval");

		maxEnergy = new Scaler(unlocked, 20, 100, 70);

		addNotification(unlocked, Notification.ICON_UNLOCK, getName(), LegacyConfig.getNotification(messages, getClass(), 1));

		int maxLvl = getPlugin().getSkillManager().getTargetLevel();
		int lvl = unlocked;
		int arrows = 3;

		while(lvl < maxLvl) {
			arrows++;
			lvl += upgradeInterval;
			addNotification(lvl, Notification.ICON_UPGRADE, getName() + " (" + arrows + ")", LegacyConfig.getNotification(messages, getClass(), "2+", arrows));
		}
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlocked) {
			list.add("Volley Energy:"+(int) Math.round(maxEnergy.scale(forlevel)) + "&7 costs 20");
			list.add("Volley Arrows:"+getArrowsToShoot(forlevel));
		}
	}

	private int getArrowsToShoot(int level) {
		int arrows = 1;
		for(int i = 0; i < 5; i++) {
			arrows++;
			if(level < unlocked + upgradeInterval * i) {
				break;
			}
			if(i == 4)
				arrows++;

		}
		return arrows;
	}

	@EventHandler
	public void onLoad(PlayerInteractEvent e) {
		if(!e.hasItem() || e.getItem().getType() != Material.BOW)
			return;

		if(e.getAction() != Action.LEFT_CLICK_AIR && e.getAction() != Action.LEFT_CLICK_BLOCK)
			return;

		Player p = e.getPlayer();
		if(!isEnabled(p))
			return;
		int level = getLevel(e.getPlayer());

		if(level >= unlocked) {

			//cooldown
			if(cooldown.isOnCooldown(p)) {
				sendOnCooldownMessage(p, cooldown.getMillisRemaining(p));
				return;
			}

			cooldown.add(p, (int)Math.round(maxEnergy.scale(level)));


			int arrowAmount = getArrowsToShoot(level);

			if(!p.getInventory().contains(Material.ARROW, arrowAmount)) {
				GameSound.play(Sound.ENTITY_VILLAGER_NO, p);
				return;
			}

			// take arrow
			if(p.getGameMode() != GameMode.CREATIVE && !ItemUtil.take(p.getInventory(), Material.ARROW, arrowAmount)) {

				p.sendMessage(ChatColor.RED + "You need "+arrowAmount+ " arrows to load an volley.");
				return;
			}

			map.put(p, arrowAmount);
			
			MessageUtil.sendToActionBar(p, messages.getMessage("ability_volley_loaded", arrowAmount));
			GameSound.play(Sound.BLOCK_NOTE_PLING, p, 1.5f, 0.8f, 0.2);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onShoot(EntityShootBowEvent e) {

		Integer shots = map.remove(e.getEntity());
		if(shots == null)
			return;
		
		// we know it's a player here as all keys in the map are players
		Player p = (Player) e.getEntity();
		//ItemStack bow = e.getBow();

		Vector velocity = p.getEyeLocation().getDirection().multiply(2.8);
		boolean crit = ((Arrow)e.getProjectile()).isCritical();
		int fireTicks = e.getProjectile().getFireTicks();

		e.setCancelled(true);
		shootVolley(p, velocity, shots, crit, fireTicks);

	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onSlotChange(PlayerItemHeldEvent e) {
		
		if(map.remove(e.getPlayer()) != null)
			MessageUtil.sendToActionBar(e.getPlayer(), messages.getMessage("ability_volley_unload"));
		
	}

	public void shootVolley(final ProjectileSource source, final Vector velocity, final int amount, final boolean crit, final int fireTicks) {

		final Random random = CoreUtil.random;
		final double offset = 0.22;

		new BukkitRunnable() {

			int i = 0;

			public void run() {
				if(++i == amount) {
					cancel();
				}

				Vec3D rand = new Vec3D(random.nextDouble(), random.nextDouble(), random.nextDouble()).substract(0.5, 0.5, 0.5).scale(2*offset);
				Vector dir = new Vector(rand.x, rand.y, rand.z).add(velocity);

				Arrow a = source.launchProjectile(Arrow.class, dir);
				a.setCritical(crit);
				a.setFireTicks(fireTicks);
				a.setBounce(false);
				a.getWorld().playSound(a.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1.5f, 1f + (i * 0.15f));

			}


		}.runTaskTimer(plugin, 0, 1);

	}


}
