package com.rpgme.content.skill.taming;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.rpgme.content.nms.NMS;
import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.effect.ParticleEffect;
import com.rpgme.plugin.util.math.Scaler;
import nl.flamecore.util.Maths;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.spigotmc.event.entity.EntityMountEvent;

import com.google.common.collect.Maps;

public class Taming extends BaseSkill {

    public static final int ABILITY_TWINS = Id.newId();
    public static final int ABILITY_GOLDEN_LASSO = Id.newId();

    private final Map<UUID, Integer> rideMap = Maps.newHashMap();
    private final Scaler horseSpeed = new Scaler(0, 1.0, 100, 4.0);

    public Taming() {
        super("Taming", SkillType.TAMING);
    }

    @Override
    public Material getItemRepresentation() {
        return Material.LEASH;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        registerAbility(ABILITY_GOLDEN_LASSO, new GoldenLasso(this));
        registerAbility(ABILITY_TWINS, new Twins(this));
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        list.add("Mount speed:x"+horseSpeed.readableScale(forlevel));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTame(EntityTameEvent e) {

        if(e.getOwner() instanceof Player) {
            Player p = (Player) e.getOwner();
            if(isEnabled(p))
                addExp((Player)e.getOwner(), ExpTables.getTameExp(e.getEntityType()));
        }

    }

    @SuppressWarnings("incomplete-switch")
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBreed(CreatureSpawnEvent e) {
        switch(e.getSpawnReason()) {
            case BREEDING:
            case CURED:
            case BUILD_IRONGOLEM:
            case BUILD_SNOWMAN:
                Player responsible = NMS.util.getNearestPlayer(e.getLocation(), 8);
                if(responsible == null || !isEnabled(responsible))
                    return;

                int xp = ExpTables.getBreedExp(e.getEntityType());
                addExp(responsible, xp);
                break;
        }

    }

	/* Exp gained for riding mounts */

    private final Map<Player, Float> speedMap = Maps.newHashMap();

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMount(EntityMountEvent e) {
        if(e.getEntityType() == EntityType.PLAYER && e.getMount().getType() == EntityType.HORSE) {

            Player p = (Player)e.getEntity();

            if(!isEnabled(p))
                return;

            rideMap.put(p.getUniqueId(), p.getStatistic(Statistic.HORSE_ONE_CM));

            float speed = p.getWalkSpeed();
            speedMap.put(p, speed);

            NMS.util.setMovementSpeed(e.getMount(), Maths.clamp(speed * horseSpeed.scale(getLevel(p)), 0.2, 1));

        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDismount(VehicleExitEvent e) {

        if(e.getExited().getType() == EntityType.PLAYER && e.getVehicle().getType() == EntityType.HORSE) {

            Player p = (Player) e.getExited();

            if(!isEnabled(p))
                return;

            Integer distance = rideMap.remove(p.getUniqueId());

            if(distance != null) {

                long dif = p.getStatistic(Statistic.HORSE_ONE_CM) - distance;
                int xp = (int) (dif/1000.0 * ExpTables.TAMING_EXP_RIDING_HORSE);
                addExp(p, xp);
            }

            Float speed = ((speed = speedMap.remove(p)) != null) ? speed : 0.2f;
            NMS.util.setMovementSpeed(e.getVehicle(), speed);
        }
    }



}
