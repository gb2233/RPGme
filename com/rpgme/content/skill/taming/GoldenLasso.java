package com.rpgme.content.skill.taming;

import java.util.List;

import com.rpgme.content.util.ItemUtil;
import com.rpgme.plugin.integration.PlayerRelation;
import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.EntityTypes;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.effect.EnchantmentGlow;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.DoubleTag;
import com.rpgme.plugin.util.nbtlib.ListTag;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class GoldenLasso extends Ability<Taming> {

	private int unlockedHorse, unlockedPassive, unlockedHostile;
	private boolean checkPlacement;

	public GoldenLasso(Taming skill) {
		super(skill, "Golden Lasso", Taming.ABILITY_GOLDEN_LASSO);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked horse", 15)
                .addValue("unlocked passive", 40)
                .addValue("unlocked hostile", 80)
                .addValue("Is the unlock level also required to place entities", "unlock placement", true);

        LegacyConfig.injectNotification(messages, getClass(), 1);
        LegacyConfig.injectNotification(messages, getClass(), 2);
        LegacyConfig.injectNotification(messages, getClass(), 3);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlockedHorse = config.getInt("unlocked horse");
        unlockedPassive = config.getInt("unlocked passive");
        unlockedHostile = config.getInt("unlocked hostile");
        checkPlacement = config.getBoolean("unlock placement");

        addNotification(unlockedHorse, Notification.upgradableIcon(1, 3), "Golden Lasso", LegacyConfig.getNotification(messages, getClass(), 1));
        addNotification(unlockedPassive, Notification.upgradableIcon(2, 3), "Golden Lasso", LegacyConfig.getNotification(messages, getClass(), 2));
        addNotification(unlockedHostile, Notification.upgradableIcon(3, 3), "Golden Lasso", LegacyConfig.getNotification(messages, getClass(), 3));
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		StringBuilder sb = new StringBuilder();
		if(forlevel >= unlockedPassive) {
			sb.append("Animals");
		} else if(forlevel >= unlockedHorse) {
			sb.append("Horses");
		}

		if(forlevel >= unlockedHostile) {
			if(sb.length() > 0)
				sb.append(" and ");
			sb.append("Monsters");
		}

		if(sb.length() > 0) {
			sb.insert(0, "Lasso:");
			list.add(sb.toString());
		}
	}

	private boolean isEntityUnlocked(int level, EntityType ent) {
		if(ent == null)
			return false;
		if(ent == EntityType.HORSE)
			return unlockedHorse <= level;
		if(EntityTypes.passiveEntities.contains(ent))
			return unlockedPassive <= level;
		return unlockedHostile <= level;
	}
	
	private boolean canPickup(Player p, Entity entity) {
		if(entity.getType() == EntityType.PLAYER)
			return false;
		
		// pets
		if(entity instanceof Tameable) {
			AnimalTamer tamer = ((Tameable)entity).getOwner();
			if((tamer instanceof Player && PluginIntegration.getInstance().getRelation(p, (Player)tamer) == PlayerRelation.TEAM))
				return true;
			else 
				return false;
		}
		
		// protection
		return PluginIntegration.getInstance().canChange(p, entity.getLocation().getBlock());
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onPickup(PlayerInteractEntityEvent e) {
		Player player = e.getPlayer();
		
		if(!isEnabled(player))
			return;

		PlayerInventory inv = player.getInventory();
		ItemStack item = inv.getItemInMainHand();
		if(item != null && item.getType() == Material.LEASH && !player.isSneaking() && canPickup(player, e.getRightClicked()) ) {

			int level = getLevel(player);
			if(!isEntityUnlocked(level, e.getRightClicked().getType()))
				return;

			CompoundTag entityTag = NBTFactory.copyOf(e.getRightClicked());
			entityTag.putString("EntityType", e.getRightClicked().getType().name());

			item = NBTFactory.setCompoundTag(item, new CompoundTag().putTag("LassooEntity", entityTag));
			EnchantmentGlow.addGlow(item);

			String customName = e.getRightClicked().getCustomName();
			StringBuilder name = new StringBuilder();
			if(customName == null)
				name.append("&f&l").append(StringUtil.reverseEnum(e.getRightClicked().getType().name()));
			else
				name.append("&b&l").append(customName);

			ItemUtil.setName(item, name.toString());

			if(item.getAmount() == 1) {
				inv.setItemInMainHand(null);
			} else {
				ItemStack held = inv.getItemInMainHand();
				held.setAmount(item.getAmount() - 1);
				inv.setItemInMainHand(held);
				item.setAmount(1);
			}
			ItemUtil.give(player, item);

			e.setCancelled(true);
			e.getRightClicked().remove();
		}

	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	private void onPlace(PlayerInteractEvent e) {

		if(e.getAction() != Action.RIGHT_CLICK_BLOCK || !e.hasItem() || e.getItem().getType() != Material.LEASH )
			return;

		ItemStack item = e.getItem();
		CompoundTag itemTag = NBTFactory.getFrom(item);
		if(itemTag == null)
			return;

		CompoundTag entityTag = (CompoundTag) itemTag.getTag("LassooEntity");
		if(entityTag == null)
			return;

		String type = entityTag.getString("EntityType");

		if(checkPlacement) {
			int level = getLevel(e.getPlayer());
			if(!isEntityUnlocked(level, EntityType.valueOf(type))) {
				GameSound.play(Sound.ENTITY_VILLAGER_NO, e.getPlayer());
				return;
			}
		}

		e.setCancelled(true);
		Location loc = e.getClickedBlock().getRelative(e.getBlockFace()).getLocation().add(0.5, 0, 0.5);

		entityTag.putTag("Pos", getDoublesList("Pos", loc.getX(), loc.getY(), loc.getZ()));
		entityTag.putTag("Motion", getDoublesList("Motion", 0,0,0));

		Entity entity = e.getClickedBlock().getWorld().spawnEntity(loc, EntityType.valueOf(type));

		NBTFactory.setEntityAttributes(entity, entityTag);

		item = NBTFactory.setCompoundTag(item, itemTag.remove("LassooEntity"));
		EnchantmentGlow.removeGlow(item);
		ItemUtil.setName(item, null);

		e.getPlayer().getInventory().setItemInMainHand(item);

	}

	private ListTag getDoublesList(String name, double... doubles) {
		ListTag nbttaglist = new ListTag(name);
		for (int i = 0; i < doubles.length; i++)
		{
			nbttaglist.add(new DoubleTag("", doubles[i]));
		}
		return nbttaglist;
	}


}
