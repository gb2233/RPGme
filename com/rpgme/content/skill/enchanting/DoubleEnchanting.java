package com.rpgme.content.skill.enchanting;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.rpgme.content.util.ItemUtil;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.RomanNumber;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.Map.Entry;

class DoubleEnchanting extends Ability<Enchanting> {

    public static final String DOUBLE_ENCHANT_KEY = "DoubleEnchanted";

	private int unlocked;
    private Map<Player, Map<Enchantment, Integer>> storage = Maps.newHashMap();


    public DoubleEnchanting(Enchanting skill) {
		super(skill, "Double Enchants", Enchanting.ABILITY_DOUBLE_ENCHANT);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 50);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlocked = config.getInt("unlocked");
        addNotification(unlocked, Notification.ICON_UNLOCK, getName(), LegacyConfig.getNotification(messages, getClass(), ""));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onTableEnchant(EnchantItemEvent e) {
		if(!isEnabled(e.getEnchanter()))
			return;

		Map<Enchantment, Integer> stored = storage.get(e.getEnchanter());
		if(stored != null) {

			Set<Enchantment> toRemove = Sets.newHashSet();

			for(Entry<Enchantment, Integer> entry : e.getEnchantsToAdd().entrySet() ) {

				for(Enchantment ench : stored.keySet()) {
					if(entry.getKey().conflictsWith(ench)) {
						toRemove.add(entry.getKey());
						continue;
					}
				}

				if(stored.containsKey(entry.getKey())) {

					int toadd = entry.getValue();
					int old = stored.get(entry.getKey());
					toRemove.add(entry.getKey());

					if(toadd < old) {
						continue;
					}

					int newi = (toadd == old) ? old++ : toadd;
					stored.put(entry.getKey(), newi);

				} 
			}

			for(Enchantment ench : toRemove) {
				e.getEnchantsToAdd().remove(ench);
			}

			ItemStack item = e.getItem();
			CompoundTag tag = NBTFactory.getFrom(item);
			if(tag == null) tag = new CompoundTag();

			item = NBTFactory.setCompoundTag(item, tag.putBoolean(DOUBLE_ENCHANT_KEY, true));

			reapplyEnchantments(e.getEnchanter(), item);
			item.addEnchantments(e.getEnchantsToAdd());

			e.getInventory().setItem(0, item);

		}

	}

	@EventHandler
	public void onEnchantPrepare(PrepareItemEnchantEvent e) {

		if(e.getItem().getEnchantments().isEmpty())
			return;

		Player p = e.getEnchanter();
		if(!isEnabled(p))
			return;
		int level = getLevel(p);
		if(level < unlocked)
			return;

		CompoundTag tag = NBTFactory.getFrom(e.getItem());

		if(tag == null || !tag.containsKey(DOUBLE_ENCHANT_KEY))
			saveAndRemoveEnchantments(p, e.getItem());

	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getInventory().getType() != InventoryType.ENCHANTING)
			return;

		InventoryAction i = e.getAction();
		ItemStack item = e.getCurrentItem();

		if(e.getSlot() == 0 && item != null && 
				(i == InventoryAction.PICKUP_ALL || i == InventoryAction.SWAP_WITH_CURSOR || i == InventoryAction.MOVE_TO_OTHER_INVENTORY)) {


			Player p = (Player) e.getViewers().get(0);
			reapplyEnchantments(p, item);
		}

	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryClose(InventoryCloseEvent e) {
		if(e.getInventory().getType() != InventoryType.ENCHANTING)
			return;

		ItemStack item = e.getInventory().getItem(0);
		if(item == null)
			return;

		Player p = (Player) e.getViewers().get(0);

		reapplyEnchantments(p, item);
	}


	private void saveAndRemoveEnchantments(Player p, ItemStack item) {
		storage.put(p, new HashMap<Enchantment,Integer>(item.getEnchantments()));
		Iterator<Entry<Enchantment, Integer>> it = item.getEnchantments().entrySet().iterator();
		for(Entry<Enchantment, Integer> e : item.getEnchantments().entrySet()) {

			ItemUtil.addToLore(item, "&b" + StringUtil.reverseEnum(e.getKey().getName()) + " " + RomanNumber.toRoman(e.getValue()) );
			item.removeEnchantment(e.getKey());


		}
	}

	private void reapplyEnchantments(Player p, ItemStack item) {
		Map<Enchantment, Integer> enchs = storage.remove(p);
		if(enchs != null) {
			item.addEnchantments(enchs);
			ItemUtil.removeLastLore(item, enchs.size());
		}

	}

}