package com.rpgme.content.skill.enchanting;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.enchantment.EnchantItemEvent;

import java.util.List;

public class CustomEnchantmentsHook extends Ability<Enchanting> {

	private static final String CONFIG_PATH = "Global.Enchantments.CEnchantingProbability";

	private int originalProbability;

	private int unlocked;
	private Scaler chanceIncrease;

	public CustomEnchantmentsHook(Enchanting skill) throws NoClassDefFoundError {
		super(skill, "CustomEnchantments Hook", Enchanting.ABILITY_CUSTOM_ENCHANTS_HOOK);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);

        config.addValue("unlocked", 15)
                .addValue("max increase", 2.5);

        LegacyConfig.injectMessage(messages, "notification_customenchantments_hook", "notification");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        // using this weird string-integer parsing because thats what the CustomEnchantment manager does.
        // just for assurance we do the same
        originalProbability = Integer.parseInt(com.taiter.ce.Main.config.getString(CONFIG_PATH));

        unlocked = getConfig().getInt("Custom Enchantments.unlocked", 15);
        double maxMultiplier = getConfig().getDouble("Custom Enchantments.max increase", 2.5);
        chanceIncrease = new Scaler(unlocked, 1.15, 100, maxMultiplier);

        addNotification(unlocked, Notification.ICON_UNLOCK, "Custom Enchantments",messages.getMessage("notification"));
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlocked) {
			list.add("Custom Enchant chance:x"+chanceIncrease.readableScale(forlevel));
		}
	}

	public void setProbability(int percent) {
		com.taiter.ce.Main.config.set(CONFIG_PATH, String.valueOf(percent));
	}

	public void resetProbability() {
		setProbability(originalProbability);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onEnchant(EnchantItemEvent event) {
		
		Player player = event.getEnchanter();
		if(!isEnabled(player)) {
			resetProbability();
			return;
		}
		
		int level = getLevel(player);
		
		if(level < unlocked) {
			resetProbability();
		} else {
			
			double multiplier = chanceIncrease.scale(level);
			int chance = (int) Math.round(originalProbability * multiplier);
			setProbability(chance);
		}
	}
}
