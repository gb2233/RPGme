package com.rpgme.content.skill.enchanting;

import com.rpgme.content.event.AnvilCraftEvent;
import com.rpgme.content.event.AnvilRecipeListener;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Enchanting extends BaseSkill {

    public static final int ABILITY_DOUBLE_ENCHANT = Id.newId();
    public static final int ABILITY_CUSTOM_ENCHANTS_HOOK = Id.newId();

	private int tableCostReduction1, tableCostReduction2;
	
	public Enchanting() {
		super("Enchanting", SkillType.ENCHANTING);
        // this enables our listener that calls AnvilCraftEvents
        AnvilRecipeListener.registerRecipe(null);
	}

    @Override
    public Material getItemRepresentation() {
        return Material.ENCHANTMENT_TABLE;
    }

    @Override
    public void onEnable() {
        registerAbility(ABILITY_DOUBLE_ENCHANT, new DoubleEnchanting(this));
        if(plugin.getServer().getPluginManager().getPlugin("CustomEnchantments") != null) {
            registerAbility(ABILITY_CUSTOM_ENCHANTS_HOOK, new CustomEnchantmentsHook(this));
        }
        // TODO re-introduce SkillEnchantment module. In new form; not an enchantment, just temporally boost exp-gain when book is used.
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= tableCostReduction2) {
			list.add("Max Enchant Cost:2");
		} else if(forlevel >= tableCostReduction1) {
			list.add("Max Enchant Cost:1");
		}
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);

        config.addValue("level at which enchants cost at most 2 levels", "max cost 2", 30)
                .addValue("level at which enchants cost at most 1 level", "max cost 1", 90);

        LegacyConfig.injectNotification(bundle, getClass(), 1);
        LegacyConfig.injectNotification(bundle, getClass(), 2);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        tableCostReduction1 = config.getInt("max cost 2", 30);
        tableCostReduction2 = config.getInt("max cost 1", 90);

        addNotification(tableCostReduction1, Notification.upgradableIcon(1,2), "Cost Reduction", LegacyConfig.getNotification(messages, getClass(), 1));
        addNotification(tableCostReduction2, Notification.upgradableIcon(2,2), "Cost Reduction", LegacyConfig.getNotification(messages, getClass(), 2));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onTableEnchant(EnchantItemEvent e) {
		if(!isEnabled(e.getEnchanter()))
			return;

		int xp = getExpForEnchants(e.getEnchantsToAdd());

		GameSound.play(Sound.ENTITY_EXPERIENCE_BOTTLE_THROW, e.getEnchanter(), 0.5f, 1.2f, 0.3);

		addExp(e.getEnchanter(), xp);

		int level = getLevel(e.getEnchanter());

		if(level >= tableCostReduction2) {
			e.setExpLevelCost(Math.min(1, e.getExpLevelCost()));
		} else if(level >= tableCostReduction1) {
			e.setExpLevelCost(Math.min(2, e.getExpLevelCost()));
		}
	}


	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onAnvilEnchant(AnvilCraftEvent e) {
		if(e.isEnchant() && isEnabled(e.getPlayer())) {

			EnchantmentStorageMeta meta = (EnchantmentStorageMeta) e.getCraftMaterial().getItemMeta();

			int xp = (int) (getExpForEnchants(meta.getStoredEnchants()) + getExpForEnchants(e.getCraftOrigional().getEnchantments()) * 0.5);

			GameSound.play(Sound.ENTITY_EXPERIENCE_BOTTLE_THROW, e.getPlayer(), 0.5f, 1.2f, 0.3);

			addExp(e.getPlayer(), xp);
		}
	}

	private int getExpForEnchants(Map<Enchantment, Integer> map) {
		int xp = 0;
		for(Entry<Enchantment, Integer> e : map.entrySet()) {
			xp += getExpForEnchant(e.getKey(), e.getValue());
		}
		return xp;
	}

	private int getExpForEnchant(Enchantment ench, int level) {
		return (int) (50 * (double) level / ench.getMaxLevel());
	}

}
