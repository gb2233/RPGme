package com.rpgme.content.skill.forging;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import net.flamedek.rpgme.Messages;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class FieldRepair extends Ability<Forging> {

	private int unlocked;

	public FieldRepair(Forging skill) {
		super(skill, "Field Repair", Forging.ABILITY_FIELD_REPAIR);
		unlocked = getConfig().getInt("Field Repair.unlocked", 80);
		addNotification(unlocked, Notification.ICON_UNLOCK, "Field Repair", Messages.getNotification(getClass(), 1));
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {

	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 70);
        LegacyConfig.injectNotification(messages, getClass(), 1);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlocked = config.getInt("unlocked");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
	public void onRepairClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if(e.getAction() != InventoryAction.SWAP_WITH_CURSOR || !isEnabled(p))
			return;

		int level = getLevel(p);
		if(level < unlocked)
			return;

		ItemStack item = e.getCurrentItem();
		if(item.getDurability() == (short) 0)
			return;

		Material mat = item.getType();

		int seperator = mat.name().indexOf('_');
		if(seperator < 0)
			return;

		ItemStack cursor = e.getCursor();
		Material cost = Forging.getRepairMaterial(mat.name().substring(0, seperator).toLowerCase());
		if(cost == null || cursor.getType() != cost)
			return;

		int ingots = Forging.getIngotsUsed(mat.name().substring(seperator+1).toLowerCase()) + 1;
		if(ingots <= 1)
			return;

		e.setCancelled(true);

		short durRequired = item.getDurability();
		short durPerIngot = (short) Math.round((double)mat.getMaxDurability() / ingots);

		int materialsUseing = Math.min(cursor.getAmount(), durRequired / durPerIngot + 1);
		short durRepairing = (short) Math.min(durPerIngot * materialsUseing, durRequired);

		// take material
		cursor.setAmount(cursor.getAmount() - materialsUseing);
		if(cursor.getAmount() == 0)
			cursor = null;
		e.getView().setCursor(cursor);	

		// repair item
		item.setDurability((short) (item.getDurability() - durRepairing));
		e.setCurrentItem(item);

		GameSound.play(Sound.BLOCK_ANVIL_USE, p.getLocation());
		
		addExp(p, skill.getRepairExp(mat, durRepairing));
	}



}
