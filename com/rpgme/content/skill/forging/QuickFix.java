package com.rpgme.content.skill.forging;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import net.flamedek.rpgme.Messages;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class QuickFix extends Ability<Forging> {
	
	private int unlockLevel, keepEnchantLevel;
	
	public QuickFix(Forging skill) {
		super(skill, "Quick Fix", Forging.ABILITY_QUICK_FIX);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlockLevel", 20)
                .addValue("When upgraded, items will not lose their enchantments when using quick fix", "upgrade", 40);

        LegacyConfig.injectNotification(messages, getClass(), 1);
        LegacyConfig.injectNotification(messages, getClass(), 2);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        unlockLevel = config.getInt("Quick Fix.unlockLevel");
        keepEnchantLevel = config.getInt("Quick Fix.upgraded");

        addNotification(unlockLevel, Notification.ICON_UNLOCK, "Quick Fix", LegacyConfig.getNotification(messages, getClass(), 1));
        addNotification(keepEnchantLevel, Notification.ICON_UPGRADE, "Quick Fix", LegacyConfig.getNotification(messages, getClass(), 2));
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {

	}


	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onRepair(PlayerInteractEvent e) {
		
		Player p = e.getPlayer();
		
		// check item and condition
		if(!e.hasItem() || !e.hasBlock() || e.getClickedBlock().getType() != Material.ANVIL || !p.isSneaking() || !isEnabled(p) || e.getItem().getDurability() == 0)
			return;
		
		int level = getLevel(p);
		if(level < unlockLevel)
			return;
		
		ItemStack result = e.getItem();
		Material item = result.getType();
		int seperator = item.name().indexOf('_');
		
		if(seperator < 0)
			return;
		
		Material cost = Forging.getRepairMaterial(item.name().substring(0, seperator).toLowerCase());
		
		if(cost == null)
			return;
		
		int ingots = Forging.getIngotsUsed(item.name().substring(seperator+1).toLowerCase()) + 1;
		
		if(ingots <= 1)
			return;
			
		e.setCancelled(true);
		
		// take material
		if(!p.getInventory().removeItem(new ItemStack(cost)).isEmpty()) {
			Messages.sendToActionBar(p, Messages.getMessage("ability_quickfix_nomaterials"));
			return;
		}
		
		// repair
		if(level < keepEnchantLevel) {
			for(Enchantment ench : result.getEnchantments().keySet()) {
				result.removeEnchantment(ench);
			}
		}

		short repair = (short) Math.round((double)item.getMaxDurability() / ingots);
		repair = (short) Math.min(repair, result.getDurability());
		
		addExp(p, skill.getRepairExp(item, repair));
		
		result.setDurability((short) (result.getDurability() - repair));
		
		p.getInventory().setItemInMainHand(result);
		GameSound.play(Sound.BLOCK_ANVIL_USE, e.getClickedBlock().getLocation());
		}
	

}
