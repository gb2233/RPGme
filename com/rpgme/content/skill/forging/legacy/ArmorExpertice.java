package com.rpgme.content.skill.forging.legacy;

import com.rpgme.content.skill.forging.Forging;
import com.rpgme.content.util.ItemCategory;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * This class tracks the amount of armor made in categories. It can p
 * @author Robin
 *
 */
public class ArmorExpertice extends Ability<Forging> {
	
	public static final String LEATHER = "leather", IRON = "iron", DIAMOND = "diamond";

	public ArmorExpertice(Forging skill) {
		super(skill, "Armor Expertice", Forging.ABILITY_ARMOR_EXPERICE);
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		for(String type : new String[]{LEATHER, IRON, DIAMOND}) {
			
		}
	}
	
	public int getLevel(RPGPlayer player, String type) {
		return getLevelAtCount(getCount(player, type));
	}
	
	private int getCount(RPGPlayer player, String type) {
		String value = player.getSetting("forge_"+type);
		try { 
			return value != null ? Integer.parseInt(value) : 0; 
		} catch(NumberFormatException e) {
			return 0;
		}
	}
	
	private int getLevelAtCount(int count) {
		if(count < 30)
			return 0;
		else if(count < 70)
			return 1;
		else if(count < 120)
			return 2;
		else
			return 3;
	}
	
	public void increaseCount(RPGPlayer player, String type, int amount) {
		int count = getCount(player, type);
		int level = getLevelAtCount(count);
		
		player.setSetting("forge_"+type, String.valueOf(count += amount));
		
		int newlevel = getLevelAtCount(count);
		if(newlevel > level) {
			// TODO: show notification
			Notification notification; // = new Notification(getLevel(player), Notification.ICON_PASSIVE);
		}
	}
	
	public void onCraftItem(Player p, Material material, int amount) {
		if(!ItemCategory.ARMOR.isMaterial(material))
			return;
		
		String type = material.name().substring(0, material.name().indexOf('_')).toLowerCase();
		
		if(type.equals("chainmail"))
			type = "leather";
		
		increaseCount(getPlayer(p), type, amount);
	}

}
