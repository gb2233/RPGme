package com.rpgme.content.skill.stamina;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.VarEnergyPerSecCooldown;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerStatisticIncrementEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.spigotmc.event.entity.EntityMountEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Stamina extends BaseSkill {

    public static final int ABILITY_MAX_HEALTH = Id.newId();
    public static final int ABILITY_FOOD_REDUCTION = Id.newId();
    public static final int ABILITY_FALL_DAMAGE = Id.newId();

	private final Map<UUID, Long> sprintMap = new HashMap<>();
	private Cooldown jumpExpCooldown;

    /* The constructor should supply a name and id to use.
    This id used here MUST be the same as the one used to register this to the plugin.
    id's can be declared as final int somewhere in your plugin. See SkillType class for an example.
     */
	public Stamina() {
		super("Stamina", SkillType.STAMINA);
	}

    /*
    onEnable is a place to register additional (sub)modules or abilities.
    You, again, need an id to register to.
     */
    @Override
    public void onEnable() {
        jumpExpCooldown = new VarEnergyPerSecCooldown(manager.getPlugin(), 1, 15);

        registerAbility(ABILITY_MAX_HEALTH, new MaxHealth(this));

        registerAbility(ABILITY_FOOD_REDUCTION, new FoodReduction(this));

        registerAbility(ABILITY_FALL_DAMAGE, new FallDamage(this));
    }

    /* An abstract method from BaseSkill. Return a Material that can represent your skill in for example a menu. */
    @Override
    public Material getItemRepresentation() {
        return Material.IRON_BOOTS;
    }

    /*
     * These two methods are overridden from {@link Module}.
     * Read the documentation there for more information.
     * You should make sure to always call the super methods from BaseSkill.
     */
    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
    }

    /*
         * Then have your implementation of the skill. BaseSkill already implements Listener and is automatically registered.
         * You should use this space to at least:
         * - award exp to players for in-game events
         *
         * Additionally you could apply other effects that are tied directly to your skill.
         * Generally though it is recommended to make these perks as separate Abilities so that they can be separately configured and disabled.
         */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onSprint(PlayerToggleSprintEvent e) {
		if(!isEnabled(e.getPlayer()))
			return;
		if(e.isSprinting()) {
			sprintMap.put(e.getPlayer().getUniqueId(), System.currentTimeMillis());
		} else {
			Player p = e.getPlayer();
			Long time = sprintMap.remove(p.getUniqueId());
			if(time != null) {

				long dif = System.currentTimeMillis() - time;
				float xp = (dif/1000f * ExpTables.STAMINA_PER_SEC);
				addExp(p, xp);

			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onSprint(PlayerToggleFlightEvent e) {
		if(e.isFlying())
			sprintMap.remove(e.getPlayer().getUniqueId());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onMount(EntityMountEvent e) {
		if(e.getEntityType() == EntityType.PLAYER)
			sprintMap.remove(e.getEntity().getUniqueId());
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onJump(PlayerStatisticIncrementEvent e) {
		if(e.getStatistic() == Statistic.JUMP) {
			
			if(jumpExpCooldown.isOnCooldown(e.getPlayer()))
				return;
			jumpExpCooldown.add(e.getPlayer(), 750);
			addExp(e.getPlayer(), ExpTables.STAMINA_JUMP_EXP);
		}
	}


}
