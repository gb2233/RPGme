package com.rpgme.content.skill.attack;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.SimpleCooldown;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class Disarm extends Ability<Attack> implements Attack.AttackListener {

    private int unlocked;
    private boolean mobs, canDropItem;

    private Scaler disarmChance, ironGripChance;
    private final Cooldown cooldown = new SimpleCooldown(plugin, 90000L); // 1.5min

    public Disarm(Attack skill) {
        super(skill, "Disarm", Attack.ABILITY_DISARM);


        new BukkitRunnable() {
            public void run() {
                //Skill.DEFENCE.getSkill().addNotification(ironGripUnlock, Notification.ICON_PASSIVE, "Iron Grip", Messages.getMessage("notification_irongrip"));
            }
        }.runTask(plugin);
    }



    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if(forlevel >= unlocked)
            list.add("Disarm Success:"+disarmChance.readableScale(forlevel)+"%");
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 30);
        config.addValue("true if an equipped weapon can be dropped if the holders inventory is full.", "can drop item", true);
        config.addValue("true if players are also able to disarm non-human entities", "mobs", true);

        LegacyConfig.injectNotification(messages, getClass(), "");
        LegacyConfig.injectMessage(messages, "notification_irongrip");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlocked = skill.getConfig().getInt("unlocked", 30);
        disarmChance = new Scaler(unlocked, 35, 100, 80);

        mobs = config.getBoolean("mobs");
        canDropItem = config.getBoolean("can drop item");

        addNotification(unlocked, Notification.ICON_UNLOCK, getName(), LegacyConfig.getNotification(messages, getClass(), ""));
        getSkill().getManager().getById(SkillType.DEFENCE)
                .addNotification(Notification.builder().icon(Notification.ICON_UNLOCK)
                        .title(getName())
                        .text(messages.getMessage("notification_irongrip")).build());

        final int ironGripUnlock = unlocked + 20;
        ironGripChance = new Scaler(ironGripUnlock, 30, 100, 100);
    }

    @Override
    public boolean onInteractEntity(Player player, LivingEntity entity, int level) {
        ItemStack item = player.getInventory().getItemInMainHand();

        if(item != null && item.getType() != Material.AIR)
            return false;

        if(level < unlocked) {
            return true;
        }

        if(!mobs && entity.getType() != EntityType.PLAYER) {
            return true;
        }


        EntityEquipment equipment = entity.getEquipment();

        if(equipment.getItemInMainHand().getType() == Material.AIR) {
            return true;
        }

        if(cooldown.isOnCooldown(player)) {
            sendOnCooldownMessage(player, cooldown.getMillisRemaining(player));
            return true;
        }

        if(!disarmChance.isRandomChance(level) || isIronGripChance(entity)) {
            entity.getWorld().playSound(entity.getLocation(), Sound.ENTITY_SKELETON_HURT, 1.3f, 1f);
            return true;
        }

        if(entity instanceof InventoryHolder) {

            Inventory inv = ((InventoryHolder)entity).getInventory();
            int firstslot = inv.firstEmpty();

            if(firstslot >= 0) {
                inv.setItem(firstslot, equipment.getItemInMainHand());
                equipment.setItemInMainHand(new ItemStack(Material.AIR));

            } else if(canDropItem) {
                entity.getWorld().dropItemNaturally(entity.getLocation().add(0, 1.5, 0), equipment.getItemInMainHand());
                equipment.setItemInMainHand(new ItemStack(Material.AIR));
            } else {
                GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
            }

        } else if(canDropItem) {
            entity.getWorld().dropItemNaturally(entity.getLocation().add(0, 1.5, 0), equipment.getItemInMainHand());
            equipment.setItemInMainHand(new ItemStack(Material.AIR));
        } else {
            GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
        }
        return true;
    }

    private boolean isIronGripChance(Entity target) {
        if(target.getType() != EntityType.PLAYER)
            return false;

        int defence = getSkill().getManager().getById(SkillType.DEFENCE).getLevel((Player)target);
        return defence >= ironGripChance.minlvl && ironGripChance.isRandomChance(defence);

    }


}
