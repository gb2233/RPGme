package com.rpgme.content.skill.attack;

import com.rpgme.content.nms.NMS;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.Combat;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.VarEnergyPerSecCooldown;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class PowerStrike extends Ability<Attack> implements Attack.AttackListener {

	private int doublestrikeUnlock;
	private Scaler doublestrikeEnergy, doublestrikeDamage;

	private Cooldown doublestrikeCooldown;

	public PowerStrike(Attack skill) {
		super(skill, "Power Strike", Attack.ABILITY_POWERSTRIKE);
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= doublestrikeUnlock)
			list.add(getName() + " Max cooldown:"+ StringUtil.readableDecimal(20/doublestrikeEnergy.scale(forlevel))+"s");
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 50);
		LegacyConfig.injectNotification(messages, getClass(), "");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		doublestrikeUnlock = skill.getConfig().getInt("unlocked");

		doublestrikeCooldown = new VarEnergyPerSecCooldown(plugin, 20, 50);
		doublestrikeEnergy = new Scaler(doublestrikeUnlock, 1, 100, 5);
		doublestrikeDamage = new Scaler(doublestrikeUnlock, 1.5, 100, 2.5);

		addNotification(doublestrikeUnlock, Notification.ICON_UNLOCK, "Power Strike", LegacyConfig.getNotification(messages, getClass(), ""));

	}

	@Override
	public boolean onInteractEntity(Player player, LivingEntity entity, int level) {

		ItemStack item = player.getInventory().getItemInMainHand();

		if(!item.getType().name().endsWith("SWORD"))
			return false;

		if(level >= doublestrikeUnlock) {

			if(doublestrikeCooldown.isOnCooldown(player)) {
				sendOnCooldownMessage(player, doublestrikeCooldown.getMillisRemaining(player), false);
			} else {

				long energy = Math.round(doublestrikeEnergy.scale(level)*1000);
				doublestrikeCooldown.add(player, energy);

				NMS.packets.doArmSwing(player);

				double damage = Combat.getDamageAttribute(item.getType()) * doublestrikeDamage.scale(level);
				entity.damage(damage, player);
			}
		}

		return true;
	}

}
