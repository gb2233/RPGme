package com.rpgme.content.skill.attack;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.Combat;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.TreasureBag;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class Attack extends BaseSkill {
	
	public interface AttackListener {

        /** A chance to listen to interact entity event without having to register an other listener for it.
         * return true if the event should be consumed. if false it will also pass on to other submodules.
         */
		boolean onInteractEntity(Player player, LivingEntity entity, int level);
		
	}

	public static final int ABILITY_BLOODLUST = Id.newId();
	public static final int ABILITY_AXE = Id.newId();
	public static final int ABILITY_DISARM = Id.newId();
	public static final int ABILITY_POWERSTRIKE = Id.newId();

	private final Scaler treasureChance = new Scaler(25,0.1,100,1);

	public Attack() {
		super("Attack", SkillType.ATTACK);
	}

	@Override
	public void onEnable() {
		registerAbility(ABILITY_BLOODLUST, new Bloodlust(this));
		registerAbility(ABILITY_AXE, new CleavingAxe(this));
		registerAbility(ABILITY_DISARM, new Disarm(this));
		registerAbility(ABILITY_POWERSTRIKE, new PowerStrike(this));
	}

	@Override
	public Material getItemRepresentation() {
		return Material.STONE_SWORD;
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= treasureChance.minlvl)
			list.add("Treasure Chance:"+String.format("%.1f", treasureChance.scale(forlevel)*10)+'�');
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
		super.createConfig(config, bundle);
		bundle.addValue("notification_treasure", "From now on, killing entities with a melee attack has a chance to drop a Treasure. The chance increases as you level up.");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
		addNotification(treasureChance.minlvl, Notification.ICON_PASSIVE, "Treasure", messages.getMessage("notification_treasure"));
	}


	/**
	 * Attack exp for hits
 	 */
	@EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onHit(EntityDamageByEntityEvent e) {
		if(e.getCause() == DamageCause.ENTITY_ATTACK && e.getDamager().getType() == EntityType.PLAYER && e.getEntityType() != EntityType.ARMOR_STAND && e.getEntityType().isAlive()) {
			Player p = (Player) e.getDamager();
			if(!isEnabled(p))
				return;

			addExp(p, Combat.getAttackDamageExp((LivingEntity) e.getEntity(), e.getFinalDamage()));
		}
	}

	/**
	 * Attack exp for kills
 	 */
	@EventHandler
	public void onKillingBlow(EntityDeathEvent e) {

		Player killer = e.getEntity().getKiller();

		if(killer != null && e.getEntity().getLastDamageCause().getCause() == DamageCause.ENTITY_ATTACK && isEnabled(killer)) {

			float exp = Combat.getKillExp(e.getEntity());
			if(e.getEntity().getMaxHealth() < 20)
				exp *= (e.getEntity().getMaxHealth()/20);

			addExp(killer, exp);

			int level = getLevel(killer);
			if(level >= treasureChance.minlvl && treasureChance.isRandomChance(level)) {

				TreasureBag.getInstance().spawnTreasure(e.getEntity().getEyeLocation(), level);

			}

		}
	}
	
	private final Set<EntityType> interactableLivingEntities = EnumSet.of(EntityType.ARMOR_STAND, EntityType.HORSE, EntityType.OCELOT, EntityType.WOLF, EntityType.VILLAGER);

	// pass on interactevent to abilities
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onInteractEntity(PlayerInteractEntityEvent e) {
				
		// ignore interactables and non living
		EntityType clickedType = e.getRightClicked().getType();
		if(!clickedType.isAlive() || interactableLivingEntities.contains(clickedType))
			return;
		
		Player p = e.getPlayer();
		if(!isEnabled(p))
			return;
		
		int level = getLevel(p);
		LivingEntity clicked = (LivingEntity) e.getRightClicked();

        for(Ability ability : getAbilities()) {
            if(ability instanceof AttackListener) {

                boolean consumed = ((AttackListener)ability).onInteractEntity(p, clicked, level);
                if(consumed)
                    return;
            }
        }
	}


}
