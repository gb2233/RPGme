package com.rpgme.content.skill.landscaping;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.content.skill.mining.PowerTool;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.TreasureBag;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.effect.PotionEffectUtil;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.EnumSet;
import java.util.List;

public class Landscaping extends BaseSkill implements PowerTool.BlockBreaker {

    public static final int ABILITY_POWER_TOOL = Id.newId();

    private final Scaler treasureChance = new Scaler(5, 0.02, 100, 0.25);
	private int hasteUnlock1, hasteUnlock2;

	public Landscaping() {
		super("Landscaping", SkillType.LANDSCAPING);
	}

    @Override
    public Material getItemRepresentation() {
        return Material.STONE_SPADE;
    }

    @Override
    public void onEnable() {
        super.onEnable();

        EnumSet<Material> GROUND_BLOCKS = EnumSet.of(Material.GRASS, Material.DIRT, Material.SAND, Material.GRAVEL, Material.CLAY, Material.LEAVES, Material.LEAVES_2);
        registerAbility(ABILITY_POWER_TOOL, new PowerTool<>(this, "SPADE", GROUND_BLOCKS, new int[] {120, 45}, new int[] {8, 30}));
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);
        config.addValue("haste 1", 50)
                .addValue("haste 2", 90);
        LegacyConfig.injectMessage(bundle, "notification_haste1");
        LegacyConfig.injectMessage(bundle, "notification_haste2");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        hasteUnlock1 = config.getInt("haste 1");
        hasteUnlock2 = config.getInt("haste 2");
        if(hasteUnlock1 > -1) {
            addNotification(hasteUnlock1, Notification.ICON_PASSIVE, "Hasteful Gathering", messages.getMessage("notification_haste1"));
            if(hasteUnlock2 > -1) {
                addNotification(hasteUnlock2, Notification.ICON_PASSIVE, "Hasteful Gathering", messages.getMessage("notification_haste2"));
            }
        }
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= treasureChance.minlvl)
			list.add("Treasure Chance:"+ StringUtil.readableDecimal(treasureChance.scale(forlevel)/10) + '�');
	}

	@Override
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent e) {

		Player p = e.getPlayer();

		if(!isEnabled(p))
			return;
		
		ItemStack item = p.getInventory().getItemInMainHand();

		if(isShovel(item)) {

			int xp = ExpTables.getLandscapingExp(e.getBlock().getType());
			if(xp > 0) {
				addExp(p, xp);

				int level = getLevel(p);
				if(treasureChance.isRandomChance(level)) {
                    TreasureBag.getInstance().spawnTreasure(e.getBlock(), level);
				}
				// hastefull gathering
				if(hasteUnlock1 >= 0) {
					int tier = level >= hasteUnlock2 ? 2 : level >= hasteUnlock1 ? 1 : 0;
					if(tier > 0)
						PotionEffectUtil.addPotionEffect(p, PotionEffectType.FAST_DIGGING, tier, 3, false, true);
				}
			}

		} else if(isShears(item) && isBush(e.getBlock().getType())) {

			addExp(p, 1);

		}
	}


	public static boolean isShovel(ItemStack item) {
		return item != null && item.getType().name().endsWith("SPADE");
	}

    public static boolean isShears(ItemStack item) {
		return item != null && item.getType() == Material.SHEARS;
	}

    public static boolean isBush(Material mat) {
		return mat == Material.LEAVES || mat == Material.LEAVES_2;
	}

}
