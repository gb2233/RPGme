package com.rpgme.content.skill.magic.spell;

import com.avaje.ebean.validation.NotNull;
import com.rpgme.content.skill.magic.essence.EssenceCost;
import com.rpgme.content.skill.magic.staff.Staff;
import com.rpgme.plugin.player.RPGPlayer;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Robin on 14-5-2016.
 */
public interface Spell extends Listener {

    @NotNull String getName();

    @NotNull String getDisplayName();

    @NotNull String getDescription();

    int getTier();

    @NotNull Type getType();

    int getCastExp(RPGPlayer player);

    double getManaCost();

    @NotNull EssenceCost getEssenceCost();

    boolean isUnlocked(RPGPlayer player, int magicLevel);

    /**
     * Cast the spell for this player.
     * @param player the player
     * @param staff the type of staff used to cast this spell
     * @param item the actual staff itemstack used to cast this spell
     * @return true if the cast is successful. if true, mana and items will be consumed.
     */
    boolean castSpell(RPGPlayer player, Staff staff, ItemStack item);

    final class Type {

        public static final Type WIND = new Type("Wind", '@', ChatColor.GRAY, true);
        public static final Type WATER = new Type("Water", '~', ChatColor.BLUE, true);
        public static final Type EARTH = new Type("Earth", '=', ChatColor.GREEN, true);
        public static final Type FIRE = new Type("Fire", '~', ChatColor.RED, true);
        public static final Type TELEPORT = new Type("Teleport", '#', ChatColor.DARK_PURPLE, false);

        private final String name;
        private final char symbol;
        private final ChatColor color;
        private final boolean combat;

        public Type(String name, char symbol, ChatColor color, boolean combat) {
            this.name = name;
            this.symbol = symbol;
            this.color = color;
            this.combat = combat;
        }

        public String getName() {
            return name;
        }

        public char getSymbol() {
            return symbol;
        }

        public ChatColor getColor() {
            return color;
        }

        public boolean isCombat() {
            return combat;
        }
    }

}
