package com.rpgme.content.skill.magic.spell;

import com.rpgme.plugin.RPGme;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
public class SpellManager {

    //private IntMap<Spell> spells = new IntMap<>(32);
    private Map<String, Spell> spells = new HashMap<>(32);

    public void registerSpell(Spell spell) {
        Spell previous = spells.get(spell.getName());
        if(previous != null) {
            throw new IllegalStateException("Spell with name " + spell.getName() + " already registered to class "+ previous.getClass());
        }
        RPGme.getInstance().getServer().getPluginManager().registerEvents(spell, RPGme.getInstance());
        spells.put(spell.getName(), spell);
    }

    public Collection<Spell> getSpells() {
        return spells.values();
    }

    public Spell getSpell(String name) {
        if(name == null)
            return null;
        return spells.get(name);
    }

    public List<Spell> getSpells(SpellFilter filter) {
        return spells.values().stream().filter(filter::matches).collect(Collectors.toList());
    }
}
