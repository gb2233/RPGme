package com.rpgme.content.skill.magic.spell.misc;

import com.rpgme.content.skill.magic.essence.EssenceCost;
import com.rpgme.content.skill.magic.Magic;
import com.rpgme.content.skill.magic.spell.Spell;
import com.rpgme.content.skill.magic.spell.Spells;
import com.rpgme.content.skill.magic.staff.Staff;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.GameSound;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Robin on 07/08/2016.
 */
public class TeleportHome implements Spell {

    private static final int CHARGE_DURATION = 5; // seconds

    private final Magic skill;

    private Map<Player, TeleportEffectTask> teleportingPlayers = new HashMap<>();

    public TeleportHome(Magic skill) {
        this.skill = skill;
    }

    public void startTeleport(Player player) {
        TeleportEffectTask task = new TeleportEffectTask(player);
        teleportingPlayers.put(player, task);
        task.start();
    }

    public void stopTeleport(Player player) {
        TeleportEffectTask task = teleportingPlayers.remove(player);
        if(task != null) {
            task.cancel();
            GameSound.play(Sound.ENTITY_PLAYER_ATTACK_WEAK, player, 1f, 0.5f);
        }
    }

    public void doTeleport(Player player) {
        player.getWorld().spawnParticle(Particle.DRAGON_BREATH, player.getEyeLocation(), 10);
        GameSound.play(Sound.ENTITY_ENDERMEN_TELEPORT, player.getLocation(), 2f, 1f);

        Location location = player.getBedSpawnLocation();
        if(location == null)
            location = player.getWorld().getSpawnLocation();
        player.teleport(location);

        player.getWorld().spawnParticle(Particle.DRAGON_BREATH, player.getEyeLocation(), 10);
        GameSound.play(Sound.ENTITY_ENDERMEN_TELEPORT, player.getLocation(), 2f, 1f);
    }

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
        stopTeleport(event.getPlayer());
    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        stopTeleport(event.getPlayer());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if(event.getFrom().distanceSquared(event.getTo()) > 0.015) {
            stopTeleport(event.getPlayer());
        }
    }

    @Override
    public int getTier() {
        return 1;
    }

    @Override
    public Type getType() {
        return Type.TELEPORT;
    }

    @Override
    public String getName() {
        return Spells.TELEPORT_HOME;
    }

    @Override
    public String getDescription() {
        return "A way to get back home, which is either your bed location or the servers spawn.\n" +
                "It takes " + CHARGE_DURATION + " seconds to charge. Moving during this time will cancel the spell.";
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public boolean castSpell(RPGPlayer player, Staff staff, ItemStack item) {
        startTeleport(player.getPlayer());
        return true;
    }

    @Override
    public int getCastExp(RPGPlayer player) {
        return 10;
    }

    @Override
    public EssenceCost getEssenceCost() {
        return new EssenceCost();
    }

    @Override
    public boolean isUnlocked(RPGPlayer player, int magicLevel) {
        return true;
    }

    @Override
    public double getManaCost() {
        return 50;
    }

    private class TeleportEffectTask extends BukkitRunnable {

        static final float RADIUS = 0.85f;
        static final int POINTS = 9;
        static final int TIMEOUT = (int) (CHARGE_DURATION * 20f / 4f);

        private final Player player;
        private final Location location = new Location(null, 0, 0, 0);
        int step = 0;

        public TeleportEffectTask(Player player) {
            this.player = player;
        }

        public void start() {
            runTaskTimer(skill.getPlugin(), 0l, 4l);
        }

        @Override
        public void run() {
            if(step++ >= TIMEOUT) {
                cancel();
                doTeleport(player);
            }

            float y = (step * 0.35f) % 2.5f;

            player.getLocation(location);
            location.add(0, y, 0);
            for(Location loc : CoreUtil.circle(location, RADIUS, POINTS)) {

                loc.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, loc, 1, 0, 0, 0, 0.01);

            }
            float pitch = (1.6f / TIMEOUT) * step + 0.4f;
            GameSound.play(Sound.BLOCK_NOTE_BASS, location, 1f, pitch);
        }
    }
}
