package com.rpgme.content.skill.magic.essence;

import com.rpgme.plugin.util.effect.EnchantmentGlow;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

import static org.bukkit.Material.*;

/**
 * Created by Robin on 12/08/2016.
 */
public class Essence {

    protected static final Material ESSENCE_MATERIAL = Material.WATCH;

    public static final Essence DUST = new Essence(1, "&bMagic Dust") {
        @Override
        public String getDisplayName() {
            return getName();
        }
    };

    public static final Essence AIR = new Essence(2, "Air");
    public static final Essence WATER = new Essence(3, "Water");
    public static final Essence EARTH = new Essence(4, "Earth");
    public static final Essence FIRE = new Essence(5, "Fire");
    public static final Essence ENDER = new Essence(6, "Ender");
    public static final Essence NETHER = new Essence(7, "Nether");

    private final short durability;
    private final String name;
    private String displayName;
    private int unlocked;

    private boolean glowing;

    public Essence(int id, String name) {
        this.durability = (short) (id * 4);
        this.name = name;
    }

    public Essence(short id, String name, boolean glowing) {
        this.durability = id;
        this.name = name;
        this.glowing = glowing;
    }

    public ItemStack createItem(int amount) {
        ItemStack item = new ItemStack(ESSENCE_MATERIAL, amount, durability);
        if(glowing)
            item = EnchantmentGlow.addGlow(item);
        //item = ItemUtil.setMaxStackSize(item, 128);
        ItemMeta meta = item.getItemMeta();
        meta.spigot().setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_DESTROYS);
        meta.setDisplayName(getDisplayName());
        item.setItemMeta(meta);
        return item;
    }

    public void addRecipes(List<Recipe> list) {
        switch(durability) {
            case 2:
                add(list, 3, FEATHER, 1, 1);
                add(list, 3, STRING, 1, 1);
                break;
            case 3:
                add(list, 16, WATER_BUCKET, 1, 2);
                add(list, 1, GLASS_BOTTLE, 1, 0);
                add(list, 24, PRISMARINE_CRYSTALS, 1, 1);
                add(list, 24, PRISMARINE_SHARD, 1, 1);
                add(list, 8, SNOW_BALL, 8, 1);
                add(list, 32, PACKED_ICE, 1, 3);
                break;
            case 4:
                add(list, 6, SLIME_BALL, 1, 1);
                add(list, 4, BONE, 1, 1);
                add(list, 6, SAPLING, 1, 1);
                add(list, 32, MAGMA_CREAM, 1, 3);
                add(list, 4, DIRT, 8, 1);
                break;
            case 5:
                add(list, 4, FLINT, 1, 1);
                add(list, 24, LAVA_BUCKET, 1, 2);
                add(list, 32, TNT, 1, 3);
                add(list, 15, BLAZE_ROD, 1, 3);
                add(list, 5, BLAZE_POWDER, 1, 1);
        }
    }

    protected void add(List<Recipe> recipeList, int resultAmount, Material extra, int extraAmount, int dustAmount) {
        recipeList.add(new RecipeBuilder(this, resultAmount).with(extra, extraAmount).withDust(dustAmount).create());
    }

    public short getDurability() {
        return durability;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Essence setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public int getUnlocked() {
        return unlocked;
    }

    public Essence setUnlocked(int unlocked) {
        this.unlocked = unlocked;
        return this;
    }

    public static class RecipeBuilder {

        private static final ItemStack MAGIC_DUST = Essence.DUST.createItem(0);

        private final Essence essence;
        private final ShapelessRecipe recipe;

        public RecipeBuilder(Essence result, int resultAmount) {
            this.recipe = new ShapelessRecipe(result.createItem(resultAmount));
            this.essence = result;
        }

        public RecipeBuilder withDust(int amount) {
            recipe.addIngredient(MAGIC_DUST.getType());
            return this;
        }

        public RecipeBuilder with(Material material, int amount) {
            recipe.addIngredient(amount, material);
            return this;
        }

        public Recipe create() {
            return recipe;
        }
    }
}
