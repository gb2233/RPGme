package com.rpgme.content.skill.magic.staff;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class StaffManager {

    private final List<Staff> staffs = new ArrayList<>();

    public void registerStaff(Staff staff) {
        if(!staffs.contains(staff)) {
            staffs.add(staff);
        }
    }

    public Staff getStaff(ItemStack item) {
        for(Staff staff : staffs) {
            if(staff.isItem(item)) {
                return staff;
            }
        }
        return null;
    }



}
