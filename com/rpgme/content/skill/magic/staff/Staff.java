package com.rpgme.content.skill.magic.staff;

import com.rpgme.content.skill.magic.spell.SpellFilter;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.List;

/**
 * Created by Robin on 14-5-2016.
 */
public interface Staff {

    SpellFilter getSpellFilter();

    void addRecipes(List<Recipe> recipeList);

    ItemStack createItem();

    boolean isItem(ItemStack item);

    ItemStack setSelectedSpell(ItemStack item, String selectedSpell);

    String getSelectedSpell(ItemStack item);

}
