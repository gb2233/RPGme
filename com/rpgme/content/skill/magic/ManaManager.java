package com.rpgme.content.skill.magic;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by Robin on 07/08/2016.
 */
public class ManaManager implements Module {

    private final Map<Player, ManaEntry> map = new HashMap<>(16);
    private final Map<Player, BarUpdateTask> runnablesMap = new HashMap<>(8);

    private final Set<Player> infinite = new HashSet<Player>(6);


    public ManaEntry getEntry(Player player) {
        return map.get(player);
    }

    /**
     * use the specified amount of mana.
     * @return true if the operation was a success, that means there was enough mana and it has been redacted.
     * if false, there was not enough mana, and nothing has been changed.
     */
    public boolean useMana(Player player, double used) {
        if(hasInfinite(player))
            return true;

        ManaEntry entry = getEntry(player);
        boolean manaHasBeenUsed = entry.useMana(used);

        if(manaHasBeenUsed && !runnablesMap.containsKey(player)) {
            BarUpdateTask task = new BarUpdateTask(entry).start();
            runnablesMap.put(player, task);
        }
        return manaHasBeenUsed;
    }

    /** @return true if this player has at least amount mana. */
    public boolean hasMana(Player player, double amount) {
        if(hasInfinite(player)) {
            return true;
        }

        final ManaEntry entry = getEntry(player);
        boolean hasMana = entry.getCurrentMana() >= amount;
        if(!hasMana) {
            entry.manaBar.setColor(BarColor.RED);
            new BukkitRunnable() {
                @Override
                public void run() {
                    entry.manaBar.setColor(BarColor.BLUE);
                }
            }.runTaskLater(RPGme.getInstance(), 10L);
        }
        return hasMana;
    }

    public boolean hasInfinite(Player player) {
        return infinite.contains(player);
    }

    public void setInfinite(Player player, boolean inf) {
        if(inf) {
            infinite.add(player);
            ManaEntry entry = getEntry(player);
            entry.setMana(entry.getMaxMana());
        } else {
            infinite.remove(player);
        }
    }

    void addPlayer(Player player, int max, double perSec) {
        map.put(player, new ManaEntry(player, max, perSec));
    }

    void removePlayer(Player player) {
        ManaEntry entry = map.remove(player);
        if(entry != null) {
            entry.manaBar.removeAll();
        }
        BarUpdateTask task = runnablesMap.remove(player);
        if(task != null) {
            task.cancel();
        }
        infinite.remove(player);
    }

    public static class ManaEntry {

        // timestamp of last change
        private long lastUsed;
        // mana at above timestamp
        private double mana;
        // max mana
        private int maxMana;
        // mana regen per sec
        private double manaRegen;

        private boolean full = true;

        private final BossBar manaBar;

        public ManaEntry(Player player, int maxMana, double manaRegen) {
            this.lastUsed = System.currentTimeMillis();
            this.manaRegen = manaRegen;
            this.mana = this.maxMana = maxMana;

            manaBar = player.getServer().createBossBar("", BarColor.BLUE, BarStyle.SOLID);
            manaBar.setVisible(false);
            manaBar.addPlayer(player);
        }

        private void updateMana() {
            mana = getCurrentMana();
            lastUsed = System.currentTimeMillis();
        }

        /** calculates and returns the current mana */
        public double getCurrentMana() {
            if(full)
                return maxMana;
            else {
                double calculated = Math.min(maxMana, (mana + ((System.currentTimeMillis() - lastUsed) / 1000.0 * manaRegen)));
                full = (calculated == maxMana);
                return calculated;
            }
        }

        public boolean useMana(double used) {
            double current = getCurrentMana();
            if(used <= current) {
                mana = current - used;
                lastUsed = System.currentTimeMillis();
                full = false;
                updateManaBar();
                return true;
            } else {
                return false;
            }
        }

        public void setMana(double newMana) {
            mana = Math.min(maxMana, newMana);
            lastUsed = System.currentTimeMillis();
            full = (mana == maxMana);
            updateManaBar();
        }

        public int getMaxMana() {
            return maxMana;
        }

        public void setMaxMana(int maxMana) {
            this.maxMana = maxMana;
        }

        public double getManaRegen() {
            return manaRegen;
        }

        public void setManaRegen(double manaRegen) {
            this.manaRegen = manaRegen;
        }

        public boolean isFull() {
            return full;
        }

        public void updateManaBar() {
            updateMana();
            manaBar.setVisible(!isFull());
            double progress = mana / getMaxMana();
            manaBar.setProgress(progress);
        }
    }

    private class BarUpdateTask extends BukkitRunnable {

        static final int PERIOD = 5;

        private final ManaEntry entry;

        public BarUpdateTask(ManaEntry entry) {
            this.entry = entry;
        }

        public BarUpdateTask start() {
            runTaskTimer(RPGme.getInstance(), 0L, PERIOD);
            return this;
        }

        @Override
        public void run() {
            entry.updateManaBar();

            if(entry.isFull()) {
                cancel();
            }
        }

        @Override
        public synchronized void cancel() throws IllegalStateException {
            super.cancel();
            Iterator<BarUpdateTask> valueIterator = runnablesMap.values().iterator();
            while(valueIterator.hasNext()) {
                if(valueIterator.next() == this) {
                    valueIterator.remove();
                    break;
                }
            }
        }
    }
}
