package com.rpgme.content.skill.fishing;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.TreasureBag;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;

import java.util.EnumSet;
import java.util.List;

public class Fishing extends BaseSkill {
	
	private static final EnumSet<Material> fishingTreasure = EnumSet.of(Material.BOW, Material.ENCHANTED_BOOK, Material.FISHING_ROD, Material.NAME_TAG, Material.SADDLE);
    private static final int EXP_TREASURE = 80;
    private static final int EXP_FISH = 25;
    private static final int EXP_JUNK = 10;

	private Scaler treasureChance = new Scaler(0,5, 100,20);
	
	public Fishing() {
		super("Fishing", SkillType.FISHING);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		//list.add("Bite Chance:x" + StringUtil.readableDecimal(((double)forlevel/ biteChance +1)));
		list.add("Treasure chance:"+treasureChance.readableScale(forlevel) + "%");
	}

    @Override
    public Material getItemRepresentation() {
        return Material.FISHING_ROD;
    }

    public int getFishingExpReward(ItemStack item) {
		if(item != null) {
			if(item.getType() == Material.RAW_FISH) {
				return (item.getDurability()+1) * EXP_FISH;
			}
			else if(isTresure(item)) {
				return EXP_TREASURE;
			}
		}
		return EXP_JUNK;
	}
	
	private boolean isTresure(ItemStack item) {
		return fishingTreasure.contains(item.getType());
	}
	
	@EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onFish(PlayerFishEvent e) {
		
		if(!isEnabled(e.getPlayer()))
			return;
		
		State state = e.getState();
		int level = getLevel(e.getPlayer());

		if(state == State.CAUGHT_FISH) {
			
			Item item = (Item) e.getCaught();
			if(treasureChance.isRandomChance(level)) {

			    TreasureBag treasureBag = plugin.getModule(RPGme.MODULE_TREASURES);
				item.setItemStack(treasureBag.rollTreasure(level));
				GameSound.play(Sound.ENTITY_PLAYER_LEVELUP, e.getPlayer(), 0.8f, 1.4f);
			} else {
				
				ItemStack caught = item.getItemStack();
				
				// set fish type based on level
				if(caught.getType() == Material.RAW_FISH) {
					short type = (short) Math.min(((CoreUtil.random.nextDouble()+0.05)*((double)level/100)*3), 3.0);
					caught.setDurability(type);

				} else if(!isTresure(caught)) {

                    if (CoreUtil.random.nextDouble() < level / 60.0) { // at level 60, never recieve junk again
                        TreasureBag treasureBag = plugin.getModule(RPGme.MODULE_TREASURES);
                        item.setItemStack(treasureBag.rollTreasure(level)); // instead recieve one of our treasures
                    }
                }
			}

			int exp = getFishingExpReward(item.getItemStack());

			addExp(e.getPlayer(), exp);
			e.setExpToDrop((int) (e.getExpToDrop()*((double)level/plugin.getSkillManager().getTargetLevel()*2 + 1)));
			
			
		}
	}

}
