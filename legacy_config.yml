## Auto-update version. Do not change!
# version=

# ===================================  =================================== #
# ==                          RPGme Configuration                       == #
# ===================================  =================================== #

# Time in minutes between saving player data. Set to 0 to disable
# This only has an effect when your server crashes to reduce the data roll-back
Auto save: 45

# List the name of worlds this manager should ignore. example:
# Disabled in worlds:
# - CreativeWorld
# - MinigameWorld
Disabled in worlds: {}

# Check permissions for every skill in the format 'rpgme.skill.<skillname>'
# Permissions are not given by default so they should be given with a permissions manager
Per skill permissions: false

# Also show an artificial 'combat level' in commands
Show Combat Level: true

# Enable placeholders in chat for use with a chat manager (like EssentialsChat)
# {TOTAL_LEVEL} = combined level of all skills
# {COMBAT_LEVEL} = average of combat related skills
# {AVERAGE_LEVEL} = average level of all skills
# Disable when not used to not waste performance
Chat formatting: true

# Length of the /rpg top <skill> command
Highscore length: 10

# Simple party implementation with party chat and exp sharing
Parties:
  enabled: true
  # Amount of blocks players can be apart to share exp
  # Set to -1 to never share exp, or set to 0 for unlimited range.
  share exp range: 25

# This module can scale the difficulty of monsters based on nearby players, and display healthbars
Mob Spawning:
  # Naturally spawned monsters getById a combat level based on the nearest player.
  # These levels influences it's health and movement speed.  
  assign levels: true
  # If levels are assigned, should they also be equipped with tiered armor sets?
  equip armor: true
  # If equipping armor, what is their equipments drop chance from 0 to 1?
  drop armor: 0.1
  # At which level do they receive their best equipment? 
  # Stats are also scaled accordingly.
  # Lowering this will increase the difficulty, while setting it higher will make them weaker. default: 200
  target level: 200
  # Choose 'hearts', 'bar', 'stats' or leave it empty to disable
  healthbar: hearts
  # Should the level (if any) also be displayed in the healthbar?
  show level: false

# Option to disable scoreboards server-wide in case it is not compatible with other plugins
# Note that if this is true, players can still disable them for themselves
Scoreboard enabled: true

# =================================  ================================== #
# ==                                               Skill Configuration == #
# =================================  ================================== #

# =============================== Attack =============================== #
Attack:
  enabled: true
  exp gain: 1.0
  # Hitting something within 1s of a previous hit builds a stack.
  Bloodlust:
    enabled: true
    # Stacks will increase damage
    unlocked: 25
    # Stacks will grant lifesteal
    upgrade: 70
    # Maximum stacks you can build. Changing this makes it easier or more difficult to receive the max effect from this ability.
    max stacks: 6
  
  # Axe ability: right click to do area of effect damage
  Cleaving Axe:
    enabled: true
    unlocked: 60
    
  # Unarmed ability: right click to knock the weapon out of someones hand
  Disarm:
    enabled: true
    unlocked: 40
    # When the owners inventory is full should the weapon drop (true) or fail (false).
    # Because mobs don't have an inventory, it is always considered full.
    can drop item: false
    
  # Sword ability: right-click to do high damage
  Power Strike:
    enabled: true
    unlocked: 50

# =============================== Archery =============================== #
Archery:
  enabled: true
  exp gain: 1.0
  # passive: recover arrows from successful hits
  arrow-recovery unlocked: 5
  # passive: extra damage on head shots
  headshots unlocked: 15
  
  # throw arrows out of your hand for fast critical shots
  Darts:
    enabled: true
    unlocked: 20
    # level at which darts become poisonous
    poison: 50
    # arrows you can throw in a row at level 100
    max arrows: 5
    
  # Load arrows into your bow by left clicking. The next shot will shoot multiple arrows
  Volley:
    enabled: true
    unlocked: 40
    # amount of arrows you shoot at level 100
    max arrows: 8
    

# ============================== Defence ============================== #
Defence:
  enabled: true
  exp gain: 1.0
  # chance to dodge incoming attacks
  Evasion:
    enabled: true
    unlocked: 10
    
  # Reduce knockback force while sneaking
  Knockback reduction:
    enabled: true
    unlocked: 5
    
  # shift in mid air to charge. Slam when you reach the ground.
  Ground Slam:
    enabled: true
    unlocked: 20
    
  # chance to ignore the disarm attack ability. Enabled with Attack.Disarm
  Iron Grip:
    unlocked: 40
    
  # sneak to charge up absorbsian hearts
  Bulk Up: 
    enabled: true
    unlocked: 30
    # at level 100, how many hearts can you charge up?
    max bonus hearts: 10
    # ticks it takes to start charging (20 ticks = 1s)
    charge delay: 70
    
  # respawn at the location where you died
  Second Wind:
    enabled: true
    # should items be dropped? and how much of the users exp should be dropped? 0 to 1
    keep inventory: true
    exp drop: 0.5
    
    # unlock level
    unlocked: 50
    # level at which it receives an upgrade
    upgraded: 65
    # cooldown in seconds
    cooldown: 180

# =============================== Mining =============================== #
Mining:
  enabled: true
  exp gain: 1.0
  # Hasteful gathering unlocks. Set to each -1 to disable.
  haste 1: 40
  haste 2: 90
  
  Power Tool:
    enabled: true
    # uplock: dig adjacent
    unlocked: 25
    # upgrade 1: dig 3x3
    upgrade1: 50
    # upgrade 2: no limit or cooldown
    upgrade2: 100
    
# ============================= Landscaping ============================= #
Landscaping:
  enabled: true
  exp gain: 1.0
  # Hasteful gathering unlocks. Set to each -1 to disable.
  haste 1: 50
  haste 2: 100
  # See Mining.Power Tool
  Power Tool:
    enabled: true
    unlocked: 15
    upgrade1: 30
    upgrade2: 70
    
# ============================= Woodcutting ============================= #
Woodcutting:   
  enabled: true
  exp gain: 1.0
  # Hasteful gathering unlocks. Set to each -1 to disable.
  haste 1: 50
  haste 2: 100
  
  Timber:
    enabled: true
    unlocked: 15
    # The max amount of blocks Timer can break (including leaves). This prevents a freeze when someone chops a connecting forest. default: 300
    timeout: 300
    
# =============================== Fishing =============================== #
Fishing:  
  enabled: true
  exp gain: 1.0
#  Double fishing bite chance for every x levels.
#  Setting this lower makes fishing easier.
  bite chance: 30
  
# =============================== Farming =============================== #
Farming:  
  enabled: true
  exp gain: 1.0
  # Chance to cause dropped apples to become golden
  Gapple Harvest:
    enabled: true
    unlocked: 25
    # Upgrade: if golden, also have a chance for it to be enchanted
    upgraded: 60
  # When holding seeds in your offhand automatically place them when breaking crops.
  # Unlocks in stages and can be set here
  Replant:
    enabled: true
    crops: 15
    canes: 30
    crops2: 50
    nether: 75

# ==============================================================
#                                                      Forging
Forging:  
  enabled: true
  exp gain: 1.0
  # Weapon upgrades
  Damage:
    unlocked: 5
    # material to use
    type: emerald
    # amount of materials per upgrade
    amount: 1
    # special displayname required or null for no name
    name: null
    # maximum amount of upgrades
    max: 15
  # Speed gives a chance to double-strike
  Speed:
    unlocked: 60
    type: flint
    amount: 3
    max: 20
  # Support greatly increases blocking efficiency and gives minor damage reduction
  Support:
    unlocked: 20
    type: iron_block
    max: 20
  
  # Allows you to smelt new tools and armor into materials.
  Deconstruct:
    enabled: true
    # Level required to reconstruct materials. 
    # in order: stone, iron, gold, chainmail, diamond
    unlock at: '0, 10, 25, 40, 50'
    # An item is reconstructing by combining it with a item in an anvil
    # Specify the material here, and optionally a display name to only accept a custom item
    repair item type: flint and steel
    repair item name: null
    
  # Sneak right click an anvil to quickly repair items
  QuickFix:
    enabled: true
    unlocked: 20
     # Level at which you keep enchants
    upgraded: 40
    
  # Click an item with the material on your curser to repair it from your inventory
  Field Repair:
    enabled: true
    unlocked: 90

# =============================== Alchemy =============================== #
Alchemy:  
  enabled: true
  exp gain: 1.0
  # With Potion Cloud every splash potion you throw has a chance to spread as a cloud
  # This makes instant potions effect multiple times and other potions last longer.
  Potion Cloud:
    enabled: true
    unlocked: 30
  # Increases the range of splash potions
  Splash:
    enabled: true
    unlocked: 15
  # No longer getById effected by your own splash potion's negative effects
  Safe Shot:
    unlocked: 40

# ============================== Enchanting ============================== #
Enchanting:  
  enabled: true
  exp gain: 1.0
  Double Enchanting: # Allows you to use the Enchantment Table twice per item
    enabled: true
    unlocked: 50
    
  # At these levels, enchanting will cost at most 2 or 1 levels (where 3 is default)
  # This applies to the Enchantment Table only
  max cost 2: 30
  max cost 1: 90
  
  # If the manager CustomEnchantments is installed, this can increase the chance to receive those custom enchants.
  Custom Enchantments:
    enabled: true
    unlocked: 15
    # multiplier for the chance. default: 3.0 (3x higher chance)
    max increase: 3.0

# This introduces enchantments that boost exp gains and can be added to all gear.
# Enchantment books are a rare find from treasures
Skill Enchants:
  enabled: true
  # True if it requires a specific enchanting level to apply to gear
  # The required level will be that of the boost in percentages
  need level: true

# =============================== Stamina =============================== #
Stamina:  
  enabled: true
  exp gain: 1.0
  # reduce hunger bar usage
  food reduction: true
  # reduced fall damage
  delicate landing: true
  # Adjust how health scales, counted in hearts. 
  # Set both to 10 to disable.
  Health:
    start at: 8
    maximum: 20
    # level when you reach the maximum
    at level: 100

# ================================ Taming ================================ #
Taming:  
  enabled: true
  exp gain: 1.0
  # gives a chance of twins when animals breed
  Twins:
    enabled: true
    # chance at which twins will occur at lvl 100, from 0 to 1
    chance: 0.4
  
  # with 'golden lassoo' you can pick up and place down entities, or sneak to just leach them  
  Lassoo:
    enabled: true
    horses unlocked: 15
    passives unlocked: 40
    hostiles unlocked: 80
    need level to place: false
    
# ============================== Summoning ============================== #
Summoning:  
  enabled: true
  exp gain: 1.0  
  # at which level are the specified entity types unlocked to harvest
  unlocked:
    chicken: 1
    slime: 5
    skeleton: 15
    enderman: 1
    blaze: 30
    magmacube: 40
    ghast: 50
  # is the level also required to use a gem ability?
  unlock to use: true
  # Should lava-bombs do block damage?
  Magmacube blockdamage: false

# ============================= End of file ============================= #
